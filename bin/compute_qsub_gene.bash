#!/bin/bash

### -------------------- fusion ---------------------------------
###PBS -P lmt
###PBS -N asterXXestla
###PBS -l select=1:ncpus=4:mpiprocs=4
###PBS -l walltime=01:00:00
### -------------------------------------------------------------

### -------------------- lmt ------------------------------------
###PBS -N asterXXestla
###PBS -l nodes=4:ppn=16
###PBS -q bigmem
###PBS -l vmem=120gb
###PBS -l walltime=500:00:00
### -------------------------------------------------------------

### -----------------------------------------------------------------
### qsub -v "aster_dir=..., cluster=..., data_file=..., N_job=..."
### -----------------------------------------------------------------


source ~/.bashrc

if [ -f ~/.asterxxrc ]; then
    source ~/.asterxxrc
fi

if [ ! -z $aster_dir ];
then
    echo "Sources of aster : $aster_dir"
else
	echo "No aster source given"
	exit 2
fi

if [ ! -z $data_file ];
then
    echo "Data file        : $data_file"
else
	echo "No data file given"
	exit 2
fi

if [ ! -z $cluster ];
then
    echo "Cluster          : $cluster"
else
	echo "No cluster given"
	exit 2
fi

if [ ! -z $sources ]; then
    echo "Sources of latin : $sources"
else
    echo "No path to the sources of latindd4py ! Define it in the .asterxxrc or give it with the option -s (--sources)" ;
    exit 2;
fi

if [ ! -z $N_job ];
then
    echo "Number of job    : $N_job"
else
	echo "No number of job given"
	exit 2
fi

if [ $cluster = fusion ];
then
    home=/gpfs/workdir/$USER
    export workdir_architecture=/gpfs/workdir
    module load openmpi/2.1.1
elif [ $cluster = lmt ];
then
    home=$HOME
    export workdir_architecture=/usrtmp/$USER
elif [ $cluster = utalca ];
then
    home=$HOME
    module load openmpi/3.1.0
    #module load python-2.7.15
    module load anaconda3
else
	echo "I don't know the memory architecture"
	exit 2
fi



dir_sources=$sources
dir_launch=$home/launch_qsub_asterxx_1
i=1
while [ -d "$dir_launch" ];
    do
    i=$((i+1))
    dir_launch=$home/launch_qsub_asterxx_$i
done

mkdir -p $dir_launch
cd $dir_launch

cat $PBS_NODEFILE > myHostfile
if [ $cluster = utalca ];
then
    /opt/openmpi-3.1.0/bin/mpirun -map-by node:OVERSUBSCRIBE --bind-to none -display-allocation --machinefile myHostfile --output-filename OUTPUT/output -np $N_job  $sources/latindd4py/bin/launch_job.bash --data $data_file --aster $aster_dir --sources $dir_sources
else
    mpirun -map-by node:OVERSUBSCRIBE --bind-to none -display-allocation --hostfile myHostfile --output-filename OUTPUT/output -np $N_job  $sources/latindd4py/bin/launch_job.bash --data $data_file --aster $aster_dir --sources $dir_sources
fi

#cat $PBS_NODEFILE > myHostfile

#mpirun -map-by node:OVERSUBSCRIBE --bind-to none -display-allocation --hostfile myHostfile --output-filename OUTPUT/output -np $N_job  $sources/latindd4py/bin/launch_job.bash --data $data_file --aster $aster_dir --sources $dir_sources

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 11:21:01 2017

@author: oumaziz
"""

#==============================================================================
# Import salome
#==============================================================================

try:
    import salome
    salome.salome_init()
    theStudy = salome.myStudy
    from salome.smesh import smeshBuilder
    smesh = smeshBuilder.New(theStudy)
except:
    print('No module salome')

#==============================================================================
# Import python modules
#==============================================================================
from os import system as osym
from os import environ as env
import sys
from sys import stdout as st
import xml.etree.ElementTree as ET
import shelve
import time
from tqdm import tqdm

def separeMesh(dataFile):
#==============================================================================
#   Load MPI environnement variable because mpi4py is not supported by salome
#==============================================================================
    sizeMPI = int(env["OMPI_COMM_WORLD_SIZE"])
    rank = int(env["OMPI_COMM_WORLD_RANK"])

    print("Number MPI process : {}".format(sizeMPI))
    print("Rank MPI : {}".format(rank))

#==============================================================================
#   Import data
#==============================================================================
    tree = ET.parse(dataFile)
    root = tree.getroot()
    prepare = root.find("method/prepare").text
    numberSubDomain = int(root.find("method/substructure").text)
    dir_med = root.find("method/resDir").text+"/MED"
    g = dict()
    neighbourSD = dict()
    connecInterf = dict()
    connecSDInterf = dict()

    nbSD_Node = numberSubDomain / sizeMPI
    if rank < numberSubDomain % sizeMPI:                                       # Test if rank is the last MPI instance
        nbSD_on_node = nbSD_Node + 1
    else:
        nbSD_on_node = nbSD_Node



    ### Create mesh builder
    ################################################################################

    smesh = smeshBuilder.New(theStudy)


    ### Import mesh
    ################################################################################
    time0 = time.time()
    timeInit = time.time()

    mesh = dict()
#    meshS = dict()
    nodes = dict()
    transferMatrix = dict()
    test = dict()

#==============================================================================
#   Separation of meshes
#==============================================================================


    st.write("Separation of meshes \n")
    st.flush()
    ### Find the subdomains to separate
    deb = rank * nbSD_Node + numberSubDomain % sizeMPI - max(numberSubDomain % sizeMPI - rank,0)
    fin = deb + nbSD_on_node

    st.write("Sub-domains to separate : {}--{} \n".format(deb,fin))
    osym("mkdir -p "+dir_med)
    for kk in range(deb,fin):
        osym("cp "+prepare+"prepare.med "+root.find("method/resDir").text+"/prepare_{}.med".format(kk))
        groupeShelve = shelve.open(prepare+"groupeShelve/groupeShelve_{}/groupeShelve".format(kk))
        g[kk] = groupeShelve['groupe'][kk]
#        neighbourSD[kk] = groupeShelve['neighbourSD'][kk]
#        connecInterf[kk] = groupeShelve['connecInterfSD'][kk]
#        connecSDInterf[kk] = groupeShelve['connecSDInterf'][kk]
        groupeShelve.close()
        time0 = time.time()
        st.write("Separation of {} th meshes \n".format(kk))
        st.flush()
        nodeIDs = []
        nodes[kk] = dict()
        transferMatrix[kk] = dict()
        time1 = time.time()
        st.write("Import mesh ")
        st.flush()
        ([mesh],zob) = smesh.CreateMeshesFromMED(root.find("method/resDir").text+"/prepare_{}.med".format(kk))
        NODE_IDs = set(mesh.GetNodesId())
        st.write("{0:.2f}".format(time.time() - time1)+" s \n")
        st.flush()
        ### Remove nodes
        st.write("Separation of the {} th meshes : \n".format(kk))
        st.flush()
        #groupNames = mesh.GetGroupNames()
        for toto in tqdm(g[kk],desc='Get nodeIDs from groups ',file=sys.stdout,ascii=True,unit=' group',dynamic_ncols=True):
            group = mesh.GetGroupByName(toto)[0]
            if str(group.GetType()) in ('NODE'):
                nodeIDs += group.GetIDs()
        nodeIDs = set(nodeIDs)
        st.write("Time to remove nodes : ".format(kk))
        st.flush()
        time0 = time.time()
        mesh.RemoveNodes(sorted(list(NODE_IDs-nodeIDs)))
        st.write("{0:.2f}".format(time.time() - time0)+" s \n")
        st.flush()
        ### Remove groups
        test[kk] = list(set(mesh.GetGroupNames()) - set([toto for toto in g[kk]]))
        for groupToRemove in tqdm(test[kk],desc='Remove groups ',file=sys.stdout,ascii=True,unit=' group',dynamic_ncols=True):
            mesh.RemoveGroup(mesh.GetGroupByName(groupToRemove)[0])
        st.write("Time to export mesh : ".format(kk))
        st.flush()
        time0 = time.time()
        mesh.ExportMED(dir_med+'/S{}.med'.format(kk))
        mesh.__del__()
        st.write("{0:.2f}".format(time.time() - time0)+" s \n")
        st.flush()
    st.write("Total time {0:.2f}".format(time.time() - timeInit)+" s \n")
    st.flush()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 11:29:15 2017

@author: oumaziz
"""

from setuptools import setup

setup(name='salomedd4aster',
      version='0.1dev',
      description='Substructuring of mesh with salome',
      url='',
      author='Paul Oumaziz',
      author_email='paul.oumaziz@gmail.com',
      license='MIT',
      long_description=open('README.rst').read(),
      )
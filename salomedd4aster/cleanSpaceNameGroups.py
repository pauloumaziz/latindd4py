# -*- coding: utf-8 -*-

###
### This file is generated automatically by SALOME v8.3.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
theStudy = salome.myStudy




###
### SMESH component
###

import argparse
parser = argparse.ArgumentParser(description="Preparation of the meshes of subdomains")
parser.add_argument('-mesh',help='Location of mesh to prepare',type=str,required=True)
args = parser.parse_args()
meshPath = args.mesh

from salome.smesh import smeshBuilder
smesh = smeshBuilder.New(theStudy)

([mesh],titi) = smesh.CreateMeshesFromMED(meshPath)

for group in mesh.GetGroups():
    group.SetName(group.GetName().strip())

mesh.ExportMED(meshPath)

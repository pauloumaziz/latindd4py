#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 15:00:56 2017

@author: oumaziz
"""



###############################################################################
### To launch
### salome shell -- python script.py dataFile.xml potential subdomains to prepare
###############################################################################
import time
timeBegin = time.time()
#import xml.etree.ElementTree as et
from lxml import etree as et
import numpy as np
import shelve
from os import environ as env
from os import system as osym
from sys import stdout as st
import sys
import pdb
import operator
from tqdm import tqdm

class SubStructuredMesh():
    """
    Class to define a substructured mesh with salome
    """
    def __init__(self,dataFile,mesh):
        """
        Build and return the substructured mesh
        Save 2 python shelves:
            - subStructuredMesh to save all information about the subdomains
            such as groups of nodes/elements, soles...
        Save the global med file for the substructured mesh with soles
        """
#==============================================================================
#       Import data
#==============================================================================
        print("Import parameters")
        tree = et.parse(dataFile)
        root = tree.getroot()
        if root.find("method").attrib:
            self.method = root.find("method").attrib['choice']
        else:
            self.method = 'latin'
        self.nameStudy = root.find("nameOfStudy").text
        self.initialMesh = mesh
        self.numberSubDomain = int(root.find("method/substructure").text)
        self.dim = int(root.find("method/dimension").text)
        self.numberNeumann = int(root.find("structure/neumann").attrib['N'])
        self.numberDirichlet = int(root.find("structure/dirichlet").attrib['N'])
        self.rootResDir = root.find("method/resDir").text+"/"
        self.directory = root.find("method/resDir").text+"/"+root.find("nameOfStudy").text+"/Prepare/"
        self.resPrepare = root.find("method/prepare").text
        self.material = [int(root.xpath("structure/material[not(@id='default')]")[jj].attrib['id'])
                         for jj in range(len(root.xpath("structure/material[not(@id='default')]")))]
        self.group = dict()

    def build(self,subdomains2prepare):
        """
        Build the subStructured domain
        Input:
            -subdomains2prepare: list of subdomains to prepare
        """

        nameStudy = self.nameStudy
        numberSubDomain = self.numberSubDomain
        dim = self.dim
        numberNeumann = self.numberNeumann
        numberDirichlet = self.numberDirichlet
        rootResDir = self.rootResDir
        group = self.group
#==============================================================================
#       Creation of directories
#==============================================================================
        osym("mkdir -p "+rootResDir+nameStudy)
        osym("mkdir -p "+rootResDir+nameStudy+"/Prepare/MED")
        osym("mkdir -p "+rootResDir+nameStudy+"/Prepare/Parameters")


        sizeMPI = int(env["OMPI_COMM_WORLD_SIZE"])
        rank = int(env["OMPI_COMM_WORLD_RANK"])
        print("Number MPI process : {}".format(sizeMPI))
        print("Rank MPI : {}".format(rank))

        ### Test if other arguments are given to choose the subdomains to prepare
        if subdomains2prepare != 'all':
            subDomains2prepare = list(map(int,subdomains2prepare))
        else:
            subDomains2prepare = list(range(numberSubDomain))

        nbSD_Node = len(subDomains2prepare) // sizeMPI
        if rank < len(subDomains2prepare) % sizeMPI:                                       # Test rank to split the rest of subdomains
            nbSD_on_node = nbSD_Node + 1
        else:
            nbSD_on_node = nbSD_Node
        deb = rank * nbSD_Node + len(subDomains2prepare) % sizeMPI - max(len(subDomains2prepare) % sizeMPI - rank,0)
        fin = deb + nbSD_on_node
        subDomains2prepare_on_node = np.array(subDomains2prepare)[list(range(deb,fin))].tolist()
        st.write("Sub-domains to prepare : {}\n".format(subDomains2prepare_on_node))

        import salome
        salome.salome_init()
        theStudy = salome.myStudy
        import  SMESH
        from salome.smesh import smeshBuilder
        smesh = smeshBuilder.New()
        elemType = [SMESH.NODE,SMESH.EDGE,SMESH.FACE,SMESH.VOLUME]

        if self.method == 'latin':
            initialMesh = self.initialMesh
    #==============================================================================
    #       Import mesh
    #==============================================================================
            time0 = time.time()
            st.write("Import mesh ")
            st.flush()
            for kk in tqdm(subDomains2prepare_on_node,desc='SD ',file=sys.stdout,ascii=True,unit=' SD',dynamic_ncols=True):
                ([mesh],zob) = smesh.CreateMeshesFromMED(initialMesh)

                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()


    #==============================================================================
    #       Count the number of nodes in each sub-domain eventually
    #       to re-substrutrure
    #==============================================================================
                numberNodes = []
                numberNodes.append(mesh.GetGroupByName("S{}".format(kk))[0].GetNumberOfNodes())

    #==============================================================================
    #       Creation of the bounding box of sub-domains
    #==============================================================================
                st.write("Creation of bounding box of sub-domains ")
                time0 = time.time()
                boundingBox = dict()
                centerBB = dict()
                for subdomain in range(numberSubDomain):
                    BB = smesh.BoundingBox(mesh.GetGroupByName('S{}'.format(subdomain))[0])
                    boundingBox[subdomain] = [BB[jj+3] - BB[jj] for jj in range(3)]
                    centerBB[subdomain] = [(BB[jj+3] + BB[jj])/2. for jj in range(3)]
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

    #==============================================================================
    #       Creation of the node groups of boundary conditions
    #==============================================================================
                st.write("Creation of node groups of BC ")
                time0 = time.time()
                ## Neumann conditions
                for jj in range(numberNeumann):
                    mesh.CreateDimGroup(mesh.GetGroupByName('Fd{}'.format(jj)),
                                        SMESH.NODE,
                                        'FdN{}'.format(jj),
                                        nbCommonNodes = SMESH.ALL_NODES,
                                        underlyingOnly = False)

                ## Dirichlet conditions
                for jj in range(numberDirichlet):
                    mesh.CreateDimGroup(mesh.GetGroupByName('Wd{}'.format(jj)),
                                        SMESH.NODE,
                                        'WdN{}'.format(jj),
                                        nbCommonNodes = SMESH.ALL_NODES,
                                        underlyingOnly = False)
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()
                mesh.UnionListOfGroups([mesh.GetGroupByName('WdN{}'.format(pp))[0] for pp in range(numberDirichlet)],'allDiri')
                ## Computation of possible connectivity with bounding boxes
                potentialConnec = dict()
#                for subdomain in range(numberSubDomain):
                potentialConnec[kk] = np.where([((centerBB[kk][0]-centerBB[jj][0])**2 + (centerBB[kk][1]-centerBB[jj][1])**2 + (centerBB[kk][2]-centerBB[jj][2])**2 <=
                                    1./4 * ((boundingBox[kk][0]+boundingBox[jj][0])**2 + (boundingBox[kk][1]+boundingBox[jj][1])**2 + (boundingBox[kk][2]+boundingBox[jj][2])**2)) for jj in range(numberSubDomain)])[0].tolist()

    #==============================================================================
    #       Creation on the boundary elements for the sub-domains
    #==============================================================================
                st.write("Creation of boundary elements of sub-domains \n")
                st.flush()
                time0 = time.time()
                for subdomain in tqdm(potentialConnec[kk],leave=False,desc="  Other SD",ascii=True,unit=' Interface',dynamic_ncols=True):
                    if dim == 3:
                        mesh.MakeBoundaryElements( SMESH.BND_2DFROM3D,
                                                    'S{}b'.format(subdomain),
                                                    '',
                                                    0,
                                                    mesh.GetGroupByName('S{}'.format(subdomain))
                                                    )
                    elif dim == 2 :
                        mesh.MakeBoundaryElements( SMESH.BND_1DFROM2D,
                                                    'S{}b'.format(subdomain),
                                                    '',
                                                    0,
                                                    mesh.GetGroupByName('S{}'.format(subdomain))
                                                    )
                    ### Node group of the boundary of the subdomain
                    mesh.CreateDimGroup(mesh.GetGroupByName('S{}b'.format(subdomain)),
                                                            SMESH.NODE,
                                                            'S{}bN'.format(subdomain),
                                                            nbCommonNodes = SMESH.ALL_NODES,
                                                            underlyingOnly = False)

                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()
                potentialConnec[kk].remove(kk)

    #==============================================================================
    #       Creation of nodes groups related to sub-domains
    #==============================================================================
                st.write("Creation of node groups of sub-domains \n")
                st.flush()
                time0 = time.time()
                ### Node group of the subdomain
                mesh.CreateDimGroup(mesh.GetGroupByName('S{}'.format(kk)),
                                                        SMESH.NODE,
                                                        'GN{}'.format(kk),
                                                        nbCommonNodes = SMESH.ALL_NODES,
                                                        underlyingOnly = False)
                mesh.CreateDimGroup(mesh.GetGroupByName('S{}b'.format(kk)),
                                                        SMESH.NODE,
                                                        'S{}bN'.format(kk),
                                                        nbCommonNodes = SMESH.ALL_NODES,
                                                        underlyingOnly = False)
                group[kk] = ['S{}'.format(kk)]
                group[kk].append('GN{}'.format(kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

    #==============================================================================
    #       Computation of the connectivity between sub-domains
    #==============================================================================
                st.write("Computation of connectivity of sub-domains \n")
                st.flush()
                time0 = time.time()
                connecSDinterf = dict()                                                         # Table of connectivity between sub-domains / interfaces
                neighbourSD = dict()
                corner = dict()
                for subdomain in range(0,numberSubDomain):
                    connecSDinterf[subdomain] = []
                    neighbourSD[subdomain] = []
                    corner[subdomain] = dict()
                for jj in tqdm(potentialConnec[kk],leave=False,desc="  Other SD",ascii=True,unit=' Interface',dynamic_ncols=True):
                    GM= mesh.IntersectListOfGroups([mesh.GetGroupByName('S{}b'.format(kk))[0],mesh.GetGroupByName('S{}b'.format(jj))[0]],
                                                   'GM{}_{}'.format(kk,jj))
                    if GM.IsEmpty():
                        mesh.RemoveGroup(GM)
                        GN= mesh.IntersectListOfGroups([mesh.GetGroupByName('S{}bN'.format(kk))[0],mesh.GetGroupByName('S{}bN'.format(jj))[0]],
                                                   'GN{}_{}'.format(kk,jj))
                        if GN.IsEmpty():
                            mesh.RemoveGroup(GN)
                        else:
                            neighbourSD[kk].append(jj)
                            neighbourSD[jj].append(kk)
                            corner[kk][jj] = True
                            # Increment of the number of interfaces
                            mesh.IntersectListOfGroups([mesh.GetGroupByName('S{}bN'.format(kk))[0],mesh.GetGroupByName('S{}bN'.format(jj))[0]],
                                                       'GN{}_{}'.format(jj,kk))
                    else:
                        neighbourSD[kk].append(jj)
                        neighbourSD[jj].append(kk)
                        corner[kk][jj] = False
                        # Increment of the number of interfaces
                        mesh.IntersectListOfGroups([mesh.GetGroupByName('S{}b'.format(kk))[0],mesh.GetGroupByName('S{}b'.format(jj))[0]],
                                                   'GM{}_{}'.format(jj,kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()



    #==============================================================================
    #       Creation of node groups of interfaces and interior edge of sub-domains
    #==============================================================================
                st.write("Creation of node groups of interfaces \n")
                st.flush()
                time0 = time.time()
                for jj in tqdm(neighbourSD[kk],desc='SD ',file=sys.stdout,ascii=True,unit=' SD',dynamic_ncols=True):
                    ## Convert GMA into GNO
                    if not(corner[kk][jj]):
                        mesh.CreateDimGroup(mesh.GetGroupByName('GM{}_{}'.format(kk,jj))[0],
                                            SMESH.NODE,
                                            'GN{}_{}'.format(kk,jj),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
                    ## Find nodes in common with Dirichlet boundary
                    mesh.IntersectListOfGroups([mesh.GetGroupByName('WdN{}'.format(pp))[0] for pp in range(numberDirichlet)]+mesh.GetGroupByName('GN{}_{}'.format(kk,jj)),
                                               'commonWd{}_{}'.format(kk,jj))
                    ## Suppress nodes in common with Dirichlet boundary
                    mesh.CutListOfGroups(mesh.GetGroupByName('GN{}_{}'.format(kk,jj)),
                                         [mesh.GetGroupByName('WdN{}'.format(pp))[0] for pp in range(numberDirichlet)],
                                         'GN{}_{}temp'.format(kk,jj))
                    if not(corner[kk][jj]):
                        mesh.RemoveGroup(mesh.GetGroupByName('GM{}_{}'.format(kk,jj))[0])
                        mesh.RemoveGroup(mesh.GetGroupByName('GM{}_{}'.format(jj,kk))[0])
                    mesh.RemoveGroup(mesh.GetGroupByName('GN{}_{}'.format(kk,jj))[0])
                    mesh.UnionListOfGroups(mesh.GetGroupByName('GN{}_{}temp'.format(kk,jj)),
                                            'GN{}_{}'.format(kk,jj))
                    mesh.RemoveGroup(mesh.GetGroupByName('GN{}_{}temp'.format(kk,jj))[0])
                    ## Update the new group of elements of the interface
                    if not(corner[kk][jj]):
                        mesh.CreateDimGroup(mesh.GetGroupByName('GN{}_{}'.format(kk,jj)),
                                            elemType[dim-1],
                                            'GM{}_{}'.format(kk,jj),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
                        mesh.RemoveGroup(mesh.GetGroupByName('GN{}_{}'.format(kk,jj))[0])
                        mesh.CreateDimGroup(mesh.GetGroupByName('GM{}_{}'.format(kk,jj)),
                                            elemType[0],
                                            'GN{}_{}'.format(kk,jj),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
                        mesh.UnionListOfGroups(mesh.GetGroupByName('GM{}_{}'.format(kk,jj)),
                                            'GM{}_{}'.format(jj,kk))
                        group[kk].append('GM{}_{}'.format(jj,kk))
                        group[kk].append('GM{}_{}'.format(kk,jj))
                    mesh.UnionListOfGroups(mesh.GetGroupByName('GN{}_{}'.format(kk,jj)),
                                            'GN{}_{}'.format(jj,kk))
                    group[kk].append('GN{}_{}'.format(kk,jj))
                    group[kk].append('GN{}_{}'.format(jj,kk))

                status_corner = [not(corner[kk][jj]) for jj in neighbourSD[kk]]
                mesh.UnionListOfGroups([mesh.GetGroupByName('GM{}_{}'.format(kk,jj))[0] for jj in np.array(neighbourSD[kk])[status_corner].tolist()],
                                       'b{}'.format(kk))

                mesh.CreateDimGroup(mesh.GetGroupByName('b{}'.format(kk)),
                                    SMESH.NODE,
                                    'bN{}'.format(kk),
                                    nbCommonNodes = SMESH.ALL_NODES,
                                    underlyingOnly = False)
                group[kk].append('b{}'.format(kk))
                group[kk].append('bN{}'.format(kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

    #==============================================================================
    #       Creation of the soles
    #==============================================================================
                st.write("Creation of soles \n")
                st.flush()
                time0 = time.time()
                boundingBoxSole = dict()
                boundingBoxSole[kk] = dict()
                for jj in tqdm(neighbourSD[kk],leave=False,desc="  Neighbours ",ascii=True,unit=' Interface',dynamic_ncols=True):
                    boundingBoxSole[jj] = dict()
                    ## Create the temporary double sole
                    mesh.CreateDimGroup(mesh.GetGroupByName('GN{}_{}'.format(kk,jj)),
                                        elemType[dim],
                                        'DS{}_{}temp'.format(kk,jj),
                                        nbCommonNodes = SMESH.AT_LEAST_ONE,
                                        underlyingOnly = False)
                    ## To avoid a blocked node in common with Dirichlet boundary conditions
                    mesh.CreateDimGroup(mesh.GetGroupByName('DS{}_{}temp'.format(kk,jj)),
                                        elemType[0],
                                        'DS{}_{}tempN'.format(kk,jj),
                                        nbCommonNodes = SMESH.AT_LEAST_ONE,
                                        underlyingOnly = False)
                    mesh.CutListOfGroups(mesh.GetGroupByName('DS{}_{}tempN'.format(kk,jj)),
                                         mesh.GetGroupByName('allDiri'),
                                         'DSN{}_{}'.format(kk,jj))
                    mesh.CreateDimGroup(mesh.GetGroupByName('DSN{}_{}'.format(kk,jj)),
                                        elemType[dim],
                                        'DS{}_{}'.format(kk,jj),
                                        nbCommonNodes = SMESH.ALL_NODES,
                                        underlyingOnly = False)
                    mesh.RemoveGroup(mesh.GetGroupByName('DS{}_{}temp'.format(kk,jj))[0])
                    mesh.RemoveGroup(mesh.GetGroupByName('DS{}_{}tempN'.format(kk,jj))[0])
                    mesh.RemoveGroup(mesh.GetGroupByName('DSN{}_{}'.format(kk,jj))[0])
                    mesh.RemoveGroup(mesh.GetGroupByName('commonWd{}_{}'.format(kk,jj))[0])
                    ## Create the sole by intersection of sub-domain and double sole
                    mesh.IntersectListOfGroups([mesh.GetGroupByName('DS{}_{}'.format(kk,jj))[0],mesh.GetGroupByName('S{}'.format(jj))[0]],
                                                'S{}_{}'.format(kk,jj))
                    mesh.IntersectListOfGroups([mesh.GetGroupByName('DS{}_{}'.format(kk,jj))[0],mesh.GetGroupByName('S{}'.format(kk))[0]],
                                                'S{}_{}'.format(jj,kk))
                    ## Create the bounding box of the sole
                    BB = smesh.BoundingBox(mesh.GetGroupByName('S{}_{}'.format(kk,jj)))
                    boundingBoxSole[kk][jj] = [BB[zz+3] - BB[zz] for zz in range(3)]
                    BB = smesh.BoundingBox(mesh.GetGroupByName('S{}_{}'.format(jj,kk)))
                    boundingBoxSole[jj][kk] = [BB[zz+3] - BB[zz] for zz in range(3)]
                    ## Create node group of sole
                    mesh.CreateDimGroup(mesh.GetGroupByName('S{}_{}'.format(kk,jj)),
                                        SMESH.NODE,
                                        'BNt{}_{}'.format(kk,jj),
                                        nbCommonNodes = SMESH.ALL_NODES,
                                        underlyingOnly = False)
                    ## Create node group for the blocked side of the sole
                    mesh.CutGroups(mesh.GetGroupByName('BNt{}_{}'.format(kk,jj))[0],
                                    mesh.GetGroupByName('GN{}_{}'.format(kk,jj))[0],
                                    'BN{}_{}'.format(kk,jj))

                    mesh.CreateDimGroup(mesh.GetGroupByName('S{}_{}'.format(jj,kk)),
                                        SMESH.NODE,
                                        'BNt{}_{}'.format(jj,kk),
                                        nbCommonNodes = SMESH.ALL_NODES,
                                        underlyingOnly = False)
                    ## Create node group for the blocked side of the sole
                    mesh.CutGroups(mesh.GetGroupByName('BNt{}_{}'.format(jj,kk))[0],
                                    mesh.GetGroupByName('GN{}_{}'.format(jj,kk))[0],
                                    'BN{}_{}'.format(jj,kk))
                    ## Remove the temporary double sole
                    mesh.RemoveGroup(mesh.GetGroupByName('DS{}_{}'.format(kk,jj))[0])
                    ## Reorientation of edge to have normal outside
                    if jj < kk:
                        if dim == 3 and not(corner[kk][jj]):
                            mesh.Reorient2DBy3D(mesh.GetGroupByName('GM{}_{}'.format(kk,jj))[0],
                                                mesh.GetGroupByName('S{}_{}'.format(kk,jj))[0])
                    group[kk].append('S{}_{}'.format(kk,jj))
                    group[kk].append('BN{}_{}'.format(kk,jj))
                    group[kk].append('BNt{}_{}'.format(kk,jj))
                    group[kk].append('S{}_{}'.format(jj,kk))
                    group[kk].append('BN{}_{}'.format(jj,kk))
                    group[kk].append('BNt{}_{}'.format(jj,kk))

                st.write("Creation of double-soles \n")
                st.flush()
                for jj in tqdm(neighbourSD[kk],leave=False,desc="  Neighbours ",ascii=True,unit=' Interface',dynamic_ncols=True):
                    mesh.UnionGroups(mesh.GetGroupByName('S{}_{}'.format(kk,jj))[0],
                                        mesh.GetGroupByName('S{}_{}'.format(jj,kk))[0],
                                        'DS{}_{}'.format(kk,jj))
                    group[kk].append('DS{}_{}'.format(kk,jj))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

    #==============================================================================
    #       Treatment of the boundary conditions
    #==============================================================================
                connecNeum = np.zeros([numberSubDomain,numberNeumann])                          # Connectivity for Neumann boundary conditions
                connecDiri = np.zeros([numberSubDomain,numberDirichlet])                        # Connectivity for Dirichlet boundary conditions

                ## Case of piloting boundary conditions
                st.write("Creation of boundary groups \n")
                st.flush()
                time0 = time.time()

                for jj in range(numberNeumann):
                    mesh.IntersectGroups(mesh.GetGroupByName('Fd{}'.format(jj))[0],
                                         mesh.GetGroupByName('S{}b'.format(kk))[0],
                                         'FdM{}_{}'.format(jj,kk))
                    if mesh.GetGroupByName('FdM{}_{}'.format(jj,kk))[0].IsEmpty():
                        mesh.RemoveGroup(mesh.GetGroupByName('FdM{}_{}'.format(jj,kk))[0])
                    else:
                        mesh.CreateDimGroup(mesh.GetGroupByName('FdM{}_{}'.format(jj,kk)),
                                            elemType[0],
                                            'FdN{}_{}'.format(jj,kk),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
                        connecNeum[kk,jj] = 1
                        group[kk].append('FdM{}_{}'.format(jj,kk))
                        group[kk].append('FdN{}_{}'.format(jj,kk))
                for jj in range(numberDirichlet):
                    mesh.IntersectGroups(mesh.GetGroupByName('Wd{}'.format(jj))[0],
                                         mesh.GetGroupByName('S{}b'.format(kk))[0],
                                         'WdM{}_{}'.format(jj,kk))
                    if mesh.GetGroupByName('WdM{}_{}'.format(jj,kk))[0].IsEmpty():
                        mesh.RemoveGroup(mesh.GetGroupByName('WdM{}_{}'.format(jj,kk))[0])
                    else:
                        mesh.CreateDimGroup(mesh.GetGroupByName('WdM{}_{}'.format(jj,kk)),
                                            elemType[0],
                                            'WdN{}_{}'.format(jj,kk),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
                        connecDiri[kk,jj] = 1
                        ## Suppress the nodes of the edges of soles in common with boundary conditions
                        for ii in neighbourSD[kk]:
                            mesh.CutGroups(mesh.GetGroupByName('BN{}_{}'.format(kk,ii))[0],
                                           mesh.GetGroupByName('WdN{}_{}'.format(jj,kk))[0],
                                           'BN{}_{}temp'.format(kk,ii))
                            if not(mesh.GetGroupByName('BN{}_{}temp'.format(kk,ii))[0].IsEmpty()):
                                mesh.RemoveGroup(mesh.GetGroupByName('BN{}_{}'.format(kk,ii))[0])
                                mesh.UnionListOfGroups(mesh.GetGroupByName('BN{}_{}temp'.format(kk,ii)),
                                            'BN{}_{}'.format(kk,ii))
                            mesh.RemoveGroup(mesh.GetGroupByName('BN{}_{}temp'.format(kk,ii))[0])

                        group[kk].append('WdM{}_{}'.format(jj,kk))
                        group[kk].append('WdN{}_{}'.format(jj,kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

    # =============================================================================
    #       Find the intersection with groups of materials
    # =============================================================================
                st.write("Intersection with material groups \n")
                st.flush()
                time0 = time.time()
                connecMaterial = dict()
                connecMaterial['SD'] = list()
                for jj in neighbourSD[kk]:
                    connecMaterial['neighbour_{}'.format(jj)] = {0:list(),1:list()}
                for ii in self.material:
                    ## Intersection with the subdomain
                    M = mesh.IntersectGroups(mesh.GetGroupByName('M{}'.format(ii))[0],
                                         mesh.GetGroupByName('S{}'.format(kk))[0],
                                         'M{}_{}'.format(ii,kk))
                    if not(M.IsEmpty()):
                        connecMaterial['SD'].append(ii)
                        group[kk].append('M{}_{}'.format(ii,kk))
                    else:
                        mesh.RemoveGroup(M)
                    ## Intersection with the soles
                    for jj in neighbourSD[kk]:
                        soleInInterf = 0
                        if kk > jj:
                            soleInInterf = 1
                        M = mesh.IntersectGroups(mesh.GetGroupByName('M{}'.format(ii))[0],
                                                 mesh.GetGroupByName('S{}_{}'.format(kk,jj))[0],
                                                 'M{}_{}_{}'.format(ii,kk,jj))
                        if not(M.IsEmpty()):
                            connecMaterial['neighbour_{}'.format(jj)][soleInInterf].append(ii)
                            group[kk].append('M{}_{}_{}'.format(ii,kk,jj))
                        else:
                            mesh.RemoveGroup(M)
                        M = mesh.IntersectGroups(mesh.GetGroupByName('M{}'.format(ii))[0],
                                                 mesh.GetGroupByName('S{}_{}'.format(jj,kk))[0],
                                                 'M{}_{}_{}'.format(ii,jj,kk))
                        if not(M.IsEmpty()):
                            connecMaterial['neighbour_{}'.format(jj)][1-soleInInterf].append(ii)
                            group[kk].append('M{}_{}_{}'.format(ii,jj,kk))
                        else:
                            mesh.RemoveGroup(M)
                M = mesh.CutListOfGroups(mesh.GetGroupByName('S{}'.format(kk)),
                                         [mesh.GetGroupByName('M{}_{}'.format(ii,kk))[0] for ii in connecMaterial['SD']],
                                         'M0_{}'.format(kk))
                if M.IsEmpty():
                    mesh.RemoveGroup(M)
                else:
                    connecMaterial['SD'] = [0] + connecMaterial['SD']
                    group[kk].append('M0_{}'.format(kk))
                for jj in neighbourSD[kk]:
                    soleInInterf = 0
                    if kk > jj:
                        soleInInterf = 1
                    M = mesh.CutListOfGroups(mesh.GetGroupByName('S{}_{}'.format(kk,jj)),
                                             [mesh.GetGroupByName('M{}_{}_{}'.format(ii,kk,jj))[0] for ii in connecMaterial['neighbour_{}'.format(jj)][soleInInterf]],
                                             'M0_{}_{}'.format(kk,jj))
                    if M.IsEmpty():
                        mesh.RemoveGroup(M)
                    else:
                        connecMaterial['neighbour_{}'.format(jj)][soleInInterf] = [0] + connecMaterial['neighbour_{}'.format(jj)][soleInInterf]
                        group[kk].append('M0_{}_{}'.format(kk,jj))
                    M = mesh.CutListOfGroups(mesh.GetGroupByName('S{}_{}'.format(jj,kk)),
                                             [mesh.GetGroupByName('M{}_{}_{}'.format(ii,jj,kk))[0] for ii in connecMaterial['neighbour_{}'.format(jj)][1-soleInInterf]],
                                             'M0_{}_{}'.format(jj,kk))
                    if M.IsEmpty():
                        mesh.RemoveGroup(M)
                    else:
                        connecMaterial['neighbour_{}'.format(jj)][1-soleInInterf] = [0] + connecMaterial['neighbour_{}'.format(jj)][1-soleInInterf]
                        group[kk].append('M0_{}_{}'.format(jj,kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()
    # =============================================================================
    #       Separate mesh
    # =============================================================================
                st.write("Separation of the {} th meshes : \n".format(kk))
                st.flush()
                nodeIDs = []
                NODE_IDs = set(mesh.GetNodesId())
                for toto in tqdm(group[kk],desc='Get nodeIDs from groups ',file=sys.stdout,ascii=True,unit=' group',dynamic_ncols=True):
                    groupe = mesh.GetGroupByName(toto)[0]
                    if str(groupe.GetType()) in ('NODE'):
                        nodeIDs += groupe.GetIDs()
                nodeIDs = set(nodeIDs)
                mesh.RemoveNodes(sorted(list(NODE_IDs-nodeIDs)))
                test = list(set(mesh.GetGroupNames()) - set([toto for toto in group[kk]]))
                for groupToRemove in tqdm(test,desc='Remove groups ',file=sys.stdout,ascii=True,unit=' group',dynamic_ncols=True):
                    mesh.RemoveGroup(mesh.GetGroupByName(groupToRemove)[0])
                st.write("Time to remove nodes : ".format(kk))
                st.flush()
    #==============================================================================
    #       Export mesh to MED format
    #==============================================================================
                st.write("Export MED ")
                st.flush()
                time0 = time.time()
                mesh.SetName('mesh')
                mesh.ExportMED(rootResDir+nameStudy+"/Prepare/MED/S{}.med".format(kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

                neighbourNeum = dict()
                neighbourNeum[kk] = []
                for jj in range(numberNeumann):
                    if connecNeum[kk,jj] == 1:
                        neighbourNeum[kk].append(jj)
                neighbourDiri = dict()
                neighbourDiri[kk] = []
                for jj in range(numberDirichlet):
                    if connecDiri[kk,jj] == 1:
                        neighbourDiri[kk].append(jj)

    #==============================================================================
    #       Creating shelve
    #==============================================================================
                osym("mkdir -p "+rootResDir+nameStudy+"/Prepare/groupeShelve/groupeShelve_{}".format(kk))

                self.neighbourSD = neighbourSD
                self.numberInterface = None
                self.connecInterfSD = []
                self.connecSDInterface = dict()
                self.connecNeum = connecNeum
                self.connecDiri = connecDiri
                self.connecMaterial = connecMaterial
                self.corner = corner
                self.neighbourNeum = neighbourNeum
                self.neighbourDiri = neighbourDiri
                self.boundingBox = boundingBox
                self.boundingBoxSole = boundingBoxSole
                self.numberNodes = numberNodes

                osym("mkdir -p "+rootResDir+nameStudy+"/Prepare/Parameters/Parameters_{}".format(kk))
                d = shelve.open(rootResDir+nameStudy+"/Prepare/Parameters/Parameters_{}/parameters".format(kk))
                d['subStructuredMesh'] = self
                d.close()

            timeEnd = "{0:.2f}".format(time.time() - timeBegin)+" s"
            print(("Time of execution {}".format(timeEnd)))

        else:
            for kk in subDomains2prepare_on_node:
    #==============================================================================
    #       Import mesh
    #==============================================================================
                neighbourSD = dict()
                neighbourNeum = dict()
                neighbourDiri = dict()
                time0 = time.time()
                st.write("Import mesh ")
                st.flush()
                ([mesh],zob) = smesh.CreateMeshesFromMED(self.resPrepare+'MED/prepare_{}.med'.format(kk))

                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()

                time0 = time.time()
                st.write("Get connectivity ")
                st.flush()
                neighbourSD[kk] = []
                neighbourNeum[kk] = []
                neighbourDiri[kk] = []
                otherSD = list(range(numberSubDomain))
                otherSD.remove(kk)
                for jj in otherSD:
                    if mesh.GetGroupByName('EXT_{}'.format(jj).ljust(80,' ')):
                        neighbourSD[kk].append(jj)
                ### Get element group of the subdomain from the node group
                mesh.CreateDimGroup(mesh.GetGroupByName('EXT_{}'.format(kk).ljust(80,' '))[0],
                                    elemType[dim],
                                    'S{}'.format(kk),
                                    nbCommonNodes = SMESH.ALL_NODES,
                                    underlyingOnly = False)
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()
                st.write("Creation of boundary elements of sub-domains \n")
                st.flush()
                time0 = time.time()
                boundary = [SMESH.BND_1DFROM2D,SMESH.BND_2DFROM3D]
                mesh.MakeBoundaryElements(boundary[dim-2],
                                          'boundary',
                                          '',
                                          0,
                                          mesh.GetGroupByName('S{}'.format(kk)))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()
                ### Define element groups of boundary conditions
                for neum in range(numberNeumann):
                    if mesh.GetGroupByName('Fd{}'.format(neum).ljust(80,' ')):
                        ### temporaire

                        # Temporary until we have the good MEDSplitter
                        mesh.IntersectGroups(mesh.GetGroupByName('Fd{}'.format(neum).ljust(80,' '))[0],
                                             mesh.GetGroupByName('EXT_{}'.format(kk).ljust(80,' '))[0],
                                             'FdN{}_{}'.format(neum,kk))
                        mesh.CreateDimGroup(mesh.GetGroupByName('FdN{}_{}'.format(neum,kk))[0],
                                            elemType[dim-1],
                                            'FdM{}_{}t'.format(neum,kk),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
                        mesh.IntersectGroups(mesh.GetGroupByName('FdM{}_{}t'.format(neum,kk))[0],
                                            mesh.GetGroupByName('boundary')[0],
                                            'FdM{}_{}'.format(neum,kk))


#                        ### Select only elements in the subdomain and not in the overlap
                        # mesh.IntersectGroups(mesh.GetGroupByName('Fd{}'.format(neum).ljust(80,' '))[0],
                        #                      mesh.GetGroupByName('boundary')[0],
                        #                      'FdM{}_{}'.format(neum,kk))
                        ### Convert in group of nodes
                        mesh.CreateDimGroup(mesh.GetGroupByName('FdM{}_{}'.format(neum,kk))[0],
                                            elemType[0],
                                            'FdN{}_{}'.format(neum,kk),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)

                        neighbourNeum[kk].append(neum)
                for diri in range(numberDirichlet):
                    if mesh.GetGroupByName('Wd{}'.format(diri).ljust(80,' ')):
                        # Temporary until we have the good MEDSplitter
#                        pdb.set_trace()
                        mesh.IntersectGroups(mesh.GetGroupByName('Wd{}'.format(diri).ljust(80,' '))[0],
                                             mesh.GetGroupByName('EXT_{}'.format(kk).ljust(80,' '))[0],
                                             'WdN{}_{}'.format(diri,kk))
                        mesh.CreateDimGroup(mesh.GetGroupByName('WdN{}_{}'.format(diri,kk))[0],
                                            elemType[dim-1],
                                            'WdM{}_{}t'.format(diri,kk),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)
#                        ####
                        mesh.IntersectGroups(mesh.GetGroupByName('WdM{}_{}t'.format(diri,kk))[0],
                                             mesh.GetGroupByName('boundary')[0],
                                             'WdM{}_{}'.format(diri,kk))




#                        ### Select only elements in the subdomain and not in the overlap
                        # mesh.IntersectGroups(mesh.GetGroupByName('Wd{}'.format(diri).ljust(80,' '))[0],
                        #                      mesh.GetGroupByName('boundary')[0],
                        #                      'WdM{}_{}'.format(diri,kk))
                        ### Convert in group of nodes
                        mesh.CreateDimGroup(mesh.GetGroupByName('WdM{}_{}'.format(diri,kk))[0],
                                            elemType[0],
                                            'WdN{}_{}'.format(diri,kk),
                                            nbCommonNodes = SMESH.ALL_NODES,
                                            underlyingOnly = False)

                        neighbourDiri[kk].append(diri)


                ### Define the whole node group of the edge of the overlap
                mesh.UnionListOfGroups([mesh.GetGroupByName('EXT_{}'.format(jj).ljust(80,' '))[0] for jj in neighbourSD[kk]],
                                        'S{}bN'.format(kk))
                mesh.CreateDimGroup(mesh.GetGroupByName('S{}bN'.format(kk))[0],
                                        elemType[dim],
                                        'S{}b'.format(kk,jj),
                                        nbCommonNodes = SMESH.AT_LEAST_ONE,
                                        underlyingOnly = False)
                ### Define an element group for the subdomain plus the whole overlap
                mesh.UnionListOfGroups([mesh.GetGroupByName('S{}'.format(kk))[0],mesh.GetGroupByName('S{}b'.format(kk))[0]],
                                        'Soverlap')
                mesh.CreateDimGroup(mesh.GetGroupByName('Soverlap')[0],
                                    elemType[0],
                                    'all_nodes',
                                    nbCommonNodes = SMESH.AT_LEAST_ONE,
                                    underlyingOnly = False)

                ### Define the groups of nodes on the edge of subdomain and overlap
                for jj in neighbourSD[kk]:
                    mesh.CreateDimGroup(mesh.GetGroupByName('EXT_{}'.format(jj).ljust(80,' '))[0],
                                        elemType[dim],
                                        'S{}_{}'.format(kk,jj),
                                        nbCommonNodes = SMESH.AT_LEAST_ONE,
                                        underlyingOnly = False)
                    mesh.CreateDimGroup(mesh.GetGroupByName('S{}_{}'.format(kk,jj))[0],
                                        SMESH.NODE,
                                        'S{}_{}N'.format(kk,jj),
                                        nbCommonNodes = SMESH.ALL_NODES,
                                        underlyingOnly = False)
                    mesh.IntersectGroups(mesh.GetGroupByName('S{}_{}N'.format(kk,jj))[0],mesh.GetGroupByName('EXT_{}'.format(kk).ljust(80,' '))[0],'GN{}_{}'.format(kk,jj))
                    mesh.RemoveGroup(mesh.GetGroupByName('S{}_{}'.format(kk,jj))[0])
                    mesh.RemoveGroup(mesh.GetGroupByName('S{}_{}N'.format(kk,jj))[0])
                mesh.UnionListOfGroups([mesh.GetGroupByName('GN{}_{}'.format(kk,jj))[0] for jj in neighbourSD[kk]],'bN{}'.format(kk))
                st.write("Export MED ")
                st.flush()
                time0 = time.time()
                mesh.SetName('mesh')
                mesh.ExportMED(rootResDir+nameStudy+"/Prepare/MED/S{}.med".format(kk))
                st.write("{0:.2f}".format(time.time() - time0)+" s\n")
                st.flush()
                self.neighbourSD = neighbourSD
                self.neighbourDiri = neighbourDiri
                self.neighbourNeum = neighbourNeum
                osym("mkdir -p "+rootResDir+nameStudy+"/Prepare/Parameters/Parameters_{}".format(kk))
                d = shelve.open(rootResDir+nameStudy+"/Prepare/Parameters/Parameters_{}/parameters".format(kk))
                d['subStructuredMesh'] = self
                d.close()

                timeEnd = "{0:.2f}".format(time.time() - timeBegin)+" s"
                print(("Time of execution {}".format(timeEnd)))

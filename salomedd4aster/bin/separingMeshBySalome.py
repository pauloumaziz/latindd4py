#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 11:31:35 2017
Launch with mpirun -n X salome -t ....
@author: oumaziz
"""

from salomedd4aster import separeMesh
dataFile = "/ul/oumaziz/PostDoc/asterXX/Data/debug_2D.xml"
subStructuredMesh = separeMesh.separeMesh(dataFile)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 15:30:59 2017

@author: oumaziz
"""
import sys
import os
import argparse

parser = argparse.ArgumentParser(description="Preparation of the meshes of subdomains")
parser.add_argument('-data',help='Input data file',type=str,required=True)
parser.add_argument('-mesh',help='Location of mesh to prepare',type=str,required=True)
parser.add_argument('-subdomains',help='Choice the subdomains to prepare',nargs='+',default='all')
parser.add_argument('-sources',help='Path to find salomedd4aster',nargs='+',default=False)

args = parser.parse_args()
dataFile = args.data
mesh = args.mesh
subdomains2prepare = args.subdomains

if args.sources:
    sources = args.sources
else:
    sources = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

print(("Data file       : "+dataFile))
print(("Mesh file       : "+mesh))
print(("Sources of code : "+sources))
sys.path.append(sources)
from salome_instance import SalomeInstance
instance = SalomeInstance.start()
print(("Instance created and now running on port {}".format(instance.get_port())))

from salomedd4aster import prepareSubStructuring

subStructuredMesh = prepareSubStructuring.SubStructuredMesh(dataFile,mesh)
subStructuredMesh.build(subdomains2prepare)

instance.stop()

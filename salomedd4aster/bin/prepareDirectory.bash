#!/bin/bash

#Argument n1 : nombre de sous-domaine
#Argument n2 : dossier de preparation a traiter



cd $2/Prepare
mkdir -p groupeShelve
mkdir -p Parameters
mkdir -p MED
for ii in `seq 0 $(($1-1))`;
do
	mkdir -p groupeShelve/groupeShelve_${ii}
	mkdir -p Parameters/Parameters_${ii}
	cp groupeShelve.* groupeShelve/groupeShelve_${ii}/.
	cp parameters.* Parameters/Parameters_${ii}/.
done

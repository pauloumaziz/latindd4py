#!/usr/bin/python
#  -*- coding: utf-8 -*-
# TODO : Modifier la relecture du maillage pour avoir toutes les mailles (1D, 2D et 3D)

from collections import defaultdict, deque
import sys
#sys.path.append('/utmp/chinon/oumaziz/salome-meca-2017/V2017.0.2/tools/Medcoupling-V8_3_0/bin')
from MEDLoader import *
from MEDLoaderSplitter import MEDLoaderSplitter
import MEDPartitioner
import os
from sys import stdout
from time import *
import numpy as np
import pdb

def versiontuple(v):
    return tuple(map(int, (v.split("."))))

def imprimerTemps():
    """
    Utilitaire d'impression du temps
    """
    tmp = localtime()
    heures = str(tmp[3])
    if len(heures) == 1: heures = "0"+heures
    minutes = str(tmp[4])
    if len(minutes) == 1: minutes = "0"+minutes
    secondes = str(tmp[5])
    if len(secondes) == 1: secondes = "0"+secondes
    txt = heures+" h "+minutes+" min "+secondes+" s"
    print(txt)

a = versiontuple(str(MEDCouplingVersionStr()))
b = versiontuple("6.6.0")
if not a > b:
    print("Mauvaise version de MEDCoupling", MEDCouplingVersionStr())
    assert False

class MyMedSplitter:
    def __ecritureMaillages(self, fichierMED, maillage, listPartitionsMailles, grpsNoeuds):

        imprimerTemps()
        print("Création d'une numérotation globale")
        champNumGlobal = MEDCouplingFieldInt(ON_NODES)
        champNumGlobal.setMesh(maillage)
        champNumGlobal.setName("Numerotation Globale")
        nbNodes = maillage.getNumberOfNodes()
        valGlob = DataArrayInt(nbNodes) ; valGlob.iota()
        champNumGlobal.setArray(valGlob)
        f = MEDFileIntFieldMultiTS()
        f.appendFieldNoProfileSBT(champNumGlobal)
        fichierMED.getFields().pushField(f)

        imprimerTemps()
        print("Écriture des maillages découpés")
        listeNomsGrpNoeuds = fichierMED.getMeshes().getMeshAtPos(0).getGroupsOnSpecifiedLev(1)
        ancienGrps = []
        for nom in listeNomsGrpNoeuds:
            groupe = fichierMED.getMeshes().getMeshAtPos(0).getNodeGroupArr(nom, False)
            ancienGrps.append(groupe)
        fichierMED.getMeshes().getMeshAtPos(0).setGroupsAtLevel(1, ancienGrps + grpsNoeuds, False)
        
        #for typeGroupeMaille in fichierMED.getMeshes().getMeshAtPos(0).getNonEmptyLevels():
            #listeNomsGrpMailles = fichierMED.getMeshes().getMeshAtPos(0).getGroupsOnSpecifiedLev(typeGroupeMaille)
            #ancienGrps = []
            #for nom in listeNomsGrpMailles:
                #groupe = fichierMED.getMeshes().getMeshAtPos(0).getGroupArr(typeGroupeMaille,nom, False)
                #ancienGrps.append(groupe)
            #fichierMED.getMeshes().getMeshAtPos(0).setGroupsAtLevel(typeGroupeMaille, ancienGrps, False)
        
        
        loaderAndSplitter = MEDLoaderSplitter(fichierMED, listPartitionsMailles)
        fichierDecoupes = loaderAndSplitter.getSplittedInstances()
        noeudsFrontiere = []
        maillages = []
        for numero, mfdp in enumerate(fichierDecoupes):
            # MEDCouplingUMesh
            maillageCourant = mfdp.getMeshes().getMeshAtPos(0)[0]
            # MEDFileUmesh
            maillageCourant2 = mfdp.getMeshes().getMeshAtPos(0)
            maillages.append(maillageCourant)

            noeudsInt = maillageCourant2.getGroupArr(1, "EXT_" + str(numero))
            nbNoeudsTot = maillageCourant.getNumberOfNodes()

            frontiere = noeudsInt.buildComplement(nbNoeudsTot)
            noeudsFrontiere.append(frontiere)
        
        return maillages, noeudsFrontiere, fichierDecoupes

    def __ecritureRaccords(self, maillages, noeudsFrontiere, procIdOnNodes, fichierDecoupes):
        nbPart = len(maillages)

        imprimerTemps()
        print("Création des correspondances numérotations locales/globales")

        nomMaillageLocalEtDistant = maillages[0].getName()

        # nsellenet
        #import cProfile, pstats, StringIO
        #pr = cProfile.Profile()
        #pr.enable()
        # ... do something ...
        listLocGlo = []
        listGloLoc = []
        for proc in range(nbPart):
            print(proc)
            fichierMED = fichierDecoupes[proc]
            f1ts=fichierMED.getFields()[0][(-1,-1)]
            curField = f1ts.field(fichierMED.getMeshes()[f1ts.getMeshName()])
            valeur = curField.getArray()

            maillageCourant = maillages[proc]
            nbNoeuds = maillageCourant.getNumberOfNodes()
            dictGloLoc = valeur.invertArrayN2O2O2NOptimized()
            mm=fichierMED.getMeshes()[0]
            mm.setGlobalNumFieldAtLevel(1,valeur)
            listLocGlo.append(valeur[:])
            listGloLoc.append(dictGloLoc)
        #
        #pr.disable()
        #s = StringIO.StringIO()
        #sortby = 'cumulative'
        #ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        #ps.print_stats()
        #print s.getvalue()
        # nsellenet

        imprimerTemps()
        print("Détermination des raccords")
        correspondances = [DataArrayInt(0,2) for i in range(nbPart*nbPart)]
        
        for proc1 in range(nbPart):
            maillageCourant1 = maillages[proc1]
            listeNoeuds1 = noeudsFrontiere[proc1]
            dictLocGlo = listLocGlo[proc1]
            #
            numGlo = dictLocGlo[listeNoeuds1]
            procs2 = procIdOnNodes[numGlo]
            for proc2 in procs2.getDifferentValues().getValues():
                ids=procs2.findIdsEqual(proc2)
                globLoc=listGloLoc[proc2]
                numLoc=numGlo[ids] ; numLoc.transformWithIndArr(globLoc)
                idNoeud=listeNoeuds1[ids]
                correspondances[nbPart*proc1 + proc2].aggregate(DataArrayInt.Meld(idNoeud+1,numLoc+1))
                pass
            stdout.write("\r\t%d processeurs terminés" % (proc1 + 1))
            stdout.flush()
        stdout.write("\n")

        imprimerTemps()
        print("Écriture des raccords")
        js=[MEDFileJoints() for i in range(nbPart)]
        format = "%-4d"
        for proc1 in range(nbPart):
            for proc2 in range(nbPart):
                raccord = correspondances[nbPart*proc1 + proc2]
                if not raccord.empty():
                    assert proc1 != proc2
                    raccord2 = raccord[:]
                    raccord3 = raccord[:,[1,0]]

                    nomRaccord = format%proc1 + " " + format%proc2
                    nomRaccord = nomRaccord.strip()
                    j1=MEDFileJoint(nomRaccord,nomMaillageLocalEtDistant,nomMaillageLocalEtDistant,proc2)
                    j1_p=MEDFileJointOneStep()
                    j1_p.pushCorrespondence(MEDFileJointCorrespondence(raccord2))
                    j1.pushStep(j1_p)
                    js[proc1].pushJoint(j1)
                    #
                    j2=MEDFileJoint(nomRaccord,nomMaillageLocalEtDistant,nomMaillageLocalEtDistant,proc1)
                    j2_p=MEDFileJointOneStep()
                    j2_p.pushCorrespondence(MEDFileJointCorrespondence(raccord3))
                    j2.pushStep(j2_p)
                    js[proc2].pushJoint(j2)
            stdout.write("\r\t%d processeurs terminés" % (proc1 + 1))
            stdout.flush()
        stdout.write("\n")
        for proc in range(nbPart):
            fichierDecoupes[proc].getMeshes()[nomMaillageLocalEtDistant].setJoints(js[proc])
            pass

    def __partitionnementMaillage(self, maillage, nbPart, nomFichierGraph):
        imprimerTemps()
        print("Création de la connectivité")

        nbNoeudsTot = maillage.getNumberOfNodes()
        
        edgetabArray,verttabArray=maillage.computeEnlargedNeighborsOfNodes()
        #
        sk=MEDCouplingSkyLineArray()
        sk.set(verttabArray,edgetabArray)
        g=MEDPartitioner.MEDPartitioner.Graph(sk,1)
        g.partGraph(nbPart)
        procIdOnNodes=g.getPartition().getValuesArray()
        #
        #pdb.set_trace()
        print("Création des partitions")
        #######
        nbMailles = maillage.getNumberOfCells()
        grpsNoeuds=[procIdOnNodes.findIdsEqual(i) for i in range(nbPart)]
        
        for i,tn in zip(range(nbPart),grpsNoeuds):
            tn.setName("EXT_" + str(i))
        stdout.write("\r\t%d %%" % 0)
        stdout.flush()
        
        listPartitionsMailles=[maillage.getCellIdsLyingOnNodes(gn,False) for gn in grpsNoeuds]
        return listPartitionsMailles, grpsNoeuds, procIdOnNodes

    def decoupageMaillageEnMemoire(self, fichierMED, maillage, nbPartition, nomScotch = None):
        listPartitionsMailles, grpsNoeuds, procIdOnNodes = self.__partitionnementMaillage(maillage, nbPartition, nomScotch)
        maillages, noeudsFrontiere,fichierDecoupes       = self.__ecritureMaillages(fichierMED, maillage,
                                                                                    listPartitionsMailles,
                                                                                    grpsNoeuds)
        self.__ecritureRaccords(maillages, noeudsFrontiere, procIdOnNodes, fichierDecoupes)
        return fichierDecoupes

    @classmethod
    def BuildFileNameOfPart(cls,nomFichier,procId):
        import os
        nomSansExtension = os.path.splitext(nomFichier)[0]
        return "%s_%i.med"%(nomSansExtension, procId)
    
    def decoupageMaillage(self, nomFichier, nomMaillage, nbPartition, nomScotch = None):
        print("Lecture du maillage", nomMaillage, "dans le fichier", nomFichier, "")
        fichierMED = MEDFileData(nomFichier)
        tmpMEDFileMesh = fichierMED.getMeshes()[nomMaillage]
        tmpCouplingMesh = tmpMEDFileMesh[0]
        # nsellenet
        maillage = tmpCouplingMesh.buildUnstructured()
        ######
        imprimerTemps()
        fichierDecoupes=self.decoupageMaillageEnMemoire(fichierMED,maillage,nbPartition,nomScotch)
        ###### Writing
        for proc,mfd in enumerate(fichierDecoupes):
            mfd.write(MyMedSplitter.BuildFileNameOfPart(nomFichier,proc),2)
        imprimerTemps()

if __name__ == '__main__':
    import sys
    mesOpts = None
    if sys.version[:3] < '2.7':
        import optparse
        parser = optparse.OptionParser()
        parser.add_option("-f", "--fichier", help = "Nom du fichier", dest = "fichier")
        parser.add_option("-m", "--maillage", help = "Nom du maillage", dest = "maillage")
        parser.add_option("-p", "--partitions", help = "Nombre de domaines", dest = "partitions", type = "int")
        parser.add_option("-s", "--graph_scotch", help = "Nom du fichier pour l'impression du graph Scotch",
                          dest = "graph_scotch")

        (opts, args) = parser.parse_args()
        mesOpts = opts
    else:
        import argparse
        parser = argparse.ArgumentParser()
        parser.add_argument("--fichier", help = "Nom du fichier")
        parser.add_argument("--maillage", help = "Nom du maillage")
        parser.add_argument("--partitions", help = "Nombre de domaines", type = int)
        parser.add_argument("--graph_scotch", help = "Nom du fichier pour l'impression du graph Scotch")

        args = parser.parse_args()
        mesOpts = args
    nomFichierMED = mesOpts.fichier
    nomMaillage = mesOpts.maillage
    nbPart = mesOpts.partitions
    nomScotch = mesOpts.graph_scotch

    assert nomFichierMED != None and nomFichierMED != ""
    assert nomMaillage != None and nomMaillage != ""
    assert nbPart > 1

    monMedSplitter = MyMedSplitter()
    monMedSplitter.decoupageMaillage(nomFichierMED, nomMaillage, nbPart, nomScotch)
    pass


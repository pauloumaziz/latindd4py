#!/usr/bin/env python

import code_aster

import os,sys
path = os.path.dirname(os.path.abspath(__file__))
code_aster.init()
from code_aster.Commands import *

mesh = code_aster.Mesh()
mesh.readMedFile(path+"/testAsterxx.med")

#model = code_aster.Model.create()
#model.setSupportMesh(mesh)
#model.addModelingOnAllMesh(code_aster.Physics.Mechanics,code_aster.Modelings.PlaneStress,)
#model.build()

model = AFFE_MODELE(MAILLAGE=mesh,
                    AFFE=_F(TOUT='OUI',
                            PHENOMENE='MECANIQUE',
                            MODELISATION='3D',
                            ),
                    DISTRIBUTION=_F(METHODE='CENTRALISE'),
                    )

YOUNG = 200000.0
POISSON = 0.3

#mater = code_aster.ElasMaterialBehaviour()
#mater.setDoubleValue( "E", YOUNG )
#mater.setDoubleValue( "Nu", POISSON )

acier = DEFI_MATERIAU(ELAS=_F(E= YOUNG,NU=POISSON ))

#acier= code_aster.Material()
#acier.addMaterialBehaviour(mater)
#acier.build()

mat = code_aster.MaterialOnMesh(mesh)
#mat.addMaterialOnAllMesh(acier)
mat.addMaterialOnGroupOfElements(acier,['all'])
mat.buildWithoutInputVariables()


imposedDof = code_aster.KinematicsMechanicalLoad()
imposedDof.setModel(model)
imposedDof.addImposedMechanicalDOFOnElements(code_aster.PhysicalQuantityComponent.Dx,0.0,'Wd0')
imposedDof.addImposedMechanicalDOFOnElements(code_aster.PhysicalQuantityComponent.Dy,0.0,'Wd0')
imposedDof.addImposedMechanicalDOFOnElements(code_aster.PhysicalQuantityComponent.Dz,0.0,'Wd0')
imposedDof.build()

imposedForc2 = code_aster.PressureDouble()
imposedForc2.setValue(code_aster.PhysicalQuantityComponent.Pres,100.)

CharMeca2 = code_aster.DistributedPressureDouble(model)
CharMeca2.setValue(imposedForc2,"Fd0")
CharMeca2.build()

study = code_aster.StudyDescription(model, mat)
study.addMechanicalLoad(CharMeca2)

dProblem = code_aster.DiscreteProblem(study)
vectElem = dProblem.buildElementaryMechanicalLoadsVector()

matr_elem = dProblem.computeMechanicalStiffnessMatrix()

monSolver = code_aster.MultFrontSolver()

numeDDL = code_aster.DOFNumbering()
numeDDL.setElementaryMatrix( matr_elem )
numeDDL.computeNumbering()


matrAsse = code_aster.AssemblyMatrixDisplacementDouble()
matrAsse.appendElementaryMatrix( matr_elem )
matrAsse.setDOFNumbering( numeDDL )
matrAsse.addKinematicsLoad(imposedDof)
matrAsse.build()

retour = vectElem.assembleVector( numeDDL )

vcine = dProblem.buildKinematicsLoad(numeDDL, 0.)

monSolver.matrixFactorization(matrAsse)
resu = monSolver.solveDoubleLinearSystemWithKinematicsLoad( matrAsse, vcine, retour)

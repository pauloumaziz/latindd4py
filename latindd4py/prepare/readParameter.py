#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#import sys

try:
    from lxml import etree as et
except:
    print("No module lxml")
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

from os import system as osym
import logging
from logging.handlers import RotatingFileHandler

import shelve
from itertools import chain
from latindd4py.aster_functions.dofs import getDofsFromModelisation
from latindd4py.aster_functions.materials import loadMaterialFromXML
from latindd4py.prepare.buildInterface import loadInterfaceFromXML

class readParameter():
    """
    Class to read the XML file for parameter and to convert it in a python class
    """

    def __init__(self,path):
        """
        Initialization of the class
        Input:
            -path: string, path of the data file written in xml
        """
        ### Data file path
        self.path = path
        tree = et.parse(path)
        root = tree.getroot()
        ### Name of the study
        self.nameStudy = root.find("nameOfStudy").text
        ### To use resumption of computation
        if root.find('resumption'):
            if root.find('resumption').attrib['choice'] == 'YES':
                self.resumption = True
            else:
                self.resumption = False
        else:
            self.resumption = False
        ### Choice of method
        if root.find("method").attrib:
            self.method = root.find("method").attrib['choice']
        else:
            self.method = 'latin'
        ### Parameters of the method
        self.relaxation = float(root.find("method/relaxation").text)            # Import relaxation
        if root.find("method/relaxation").attrib:
            self.choiceRelax = root.find("method/relaxation").attrib['choice']
        else:
            self.choiceRelax = 'CLASSICAL'
        self.maxIteration = int(root.find("method/maxIteration").text)        # Import the maximal number of iterations
        self.error = float(root.find("method/error").text)                      # Import the error criterion
        # Load if the error is computed by timestep
        if root.xpath("method/error[@timestep]"):
            self.errorTimeStep = eval(root.find("method/error").attrib['timestep'])
        else:
            self.errorTimeStep = False

        self.dim = int(root.find("method/dimension").text)                      # Import the dimension of the problem
        self.rootResDir = root.find("method/resDir").text+"/"
        self.resDir = root.find("method/resDir").text+"/"+root.find("nameOfStudy").text+"/"+root.find("nameOfStudy").text+"_"+str(rank)+"/"             # Import the results' directory
        self.resPrepare = root.find("method/prepare").text
        self.decomposer = root.find("method/substructure").attrib['type']       # Import the type of decomposer
        self.saveField = root.find("method/saveField").attrib['choice']         # Import the choice of saving fields in txt format
        self.numberSubDomain = int(root.find("method/substructure").text)       # Import the number of sub-domains
        if root.xpath("method/corners"):
            self.corners = root.find("method/corners").attrib['choice']
        else:
            self.corners = False

        if root.xpath("method/searchDirections"):
            self.searchDirections = eval(root.find("method/searchDirections").attrib['different'])
        else:
            self.searchDirections = True
        if self.decomposer != 'MANUAL':
            d = shelve.open(self.resPrepare+"/Parameters/Parameters_{}/connecSDinterf".format(rank))
            self.numberSubDomain = d['numberSD']
            self.connecMaterial = d['connecMaterial']
            d.close()
        self.multiscale = root.find("method/multiscale").attrib['choice']       # Import the choice of mutliscale 'YES' or 'NO'
        if self.multiscale == 'YES':
            self.macroBasis = root.find("method/multiscale").attrib['case']       # Import the choice of the macro basis 'FORCE' or 'DISPLACEMENT'
        if 'spectrum' in root.find("method/multiscale").attrib:
                self.limitSpectrum = float(root.find("method/multiscale").attrib['spectrum'])
        else:
            self.limitSpectrum  = 1.5
        self.macroModes = int(root.find("method/multiscale").text)              # Import the number of macro modes
        self.quasistatic = root.find("method/quasistatic").attrib['choice']     # Import the choice of quasistatic 'YES' or 'NO'
        if self.quasistatic == 'YES':
            self.dt = float(root.find("method/quasistatic/timestep").text)          # Import the time step
            self.numberTimeStep = int(root.find("method/quasistatic/Ntimestep").text)  # Import the number of time step
        else:
            self.dt = 1
            self.numberTimeStep = 1
        ### Case incremental or not
        if root.xpath("method/incremental"):
            self.incremental = eval(root.find("method/incremental").attrib['choice'])
        else:
            self.incremental = False
        if self.incremental:
            self.listTimeSteps = [0]
            self.iterIncremental = list(range(self.numberTimeStep))
        else:
            self.listTimeSteps = list(range(self.numberTimeStep))
            self.iterIncremental = [0]
        ### Case of multiparametric
        self.multiparam = dict()
        self.multiparam['choice'] = root.find("method/multiparametric").attrib['choice']  # Import the choice of multiparametric 'YES' or 'NO'
        if self.multiparam['choice'] == 'YES':
            num = [int(root.xpath("method/multiparametric/variable")[jj].attrib['id']) for jj in range(len(root.xpath("method/multiparametric/variable")))]
            parameter = [eval(root.xpath("method/multiparametric/variable")[jj].text) for jj in range(len(root.xpath("method/multiparametric/variable")))]
            self.multiparam['parameter'] = dict(list(zip(num,parameter)))
            self.multiparam['numCase'] = 1
            for ii in list(self.multiparam['parameter'].keys()):
                self.multiparam['numCase'] = self.multiparam['numCase'] * len(self.multiparam['parameter'][ii])

        ### Parameters of the structure
        ## Modelisation
        self.phenomenon = root.find("structure/model/phenomenon").text          # Import the phenomenon of the modelisation for code_aster
        self.modelisation = root.find("structure/model/modelisation").text      # Import the modelisation of the structure for code_aster
        self.dofs = getDofsFromModelisation(self.phenomenon,self.modelisation)
        ## Materials
        self.defaultMaterial,self.material = loadMaterialFromXML(path,self.numberSubDomain)


        ## Boundary conditions
        # Dirichlet boundary conditions
        self.numberDirichlet = int(root.find("structure/dirichlet").attrib['N'])    # Import the number of Dirichlet boundary conditions
        self.boundaryDirichlet = dict()
        for ii in range(self.numberDirichlet):
            component = [root.xpath("structure/dirichlet/dirichletCond[@id='"+str(int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id']))+"']/component")[jj].attrib['comp']
                         for jj in range(len(root.xpath("structure/dirichlet/dirichletCond[@id='"+str(int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id']))+"']/component")))]    # Import the degree of freedom
            value = [float(root.xpath("structure/dirichlet/dirichletCond[@id='"+str(int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id']))+"']/component")[jj].text)
                     for jj in range(len(root.xpath("structure/dirichlet/dirichletCond[@id='"+str(int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id']))+"']/component")))]        # Import the value of the conditions
            self.boundaryDirichlet[int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id'])] = dict(list(zip(component, value)))
            if self.quasistatic == 'YES':
                #self.boundaryDirichlet[int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id'])]['evol'] = eval(root.find("structure/dirichlet/dirichletCond[@id='"+str(ii)+"']/evolution").text)
                self.boundaryDirichlet[int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id'])]['evol'] = [float(e) for e in root.find("structure/dirichlet/dirichletCond[@id='"+str(ii)+"']/evolution").text.split(' ')]
            else:
                self.boundaryDirichlet[int(root.xpath("structure/dirichlet/dirichletCond")[ii].attrib['id'])]['evol'] = [1.]
        # Neumann boundary conditions
        self.numberNeumann = int(root.find("structure/neumann").attrib['N'])    # Import the number of Neumann boundary conditions
        self.boundaryNeumann = dict()
        allNeum = list()
        for ii in range(self.numberNeumann):
            if eval(root.xpath("structure/neumann/neumannCond[@id='{}']".format(ii))[0].attrib['all']):
                allNeum.append(ii)
            component = [root.xpath("structure/neumann/neumannCond[@id='"+str(ii)+"']/component")[jj].attrib['comp']
                         for jj in range(len(root.xpath("structure/neumann/neumannCond[@id='"+str(ii)+"']/component")))]    # Import the degree of freedom
            component.append("type")                                                                                        # Import the type of Neumann boundary condition
            value = [float(root.xpath("structure/neumann/neumannCond[@id='"+str(ii)+"']/component")[jj].text)
                     for jj in range(len(root.xpath("structure/neumann/neumannCond[@id='"+str(ii)+"']/component")))]        # Import the value of the conditions
            value.append(root.find("structure/neumann/neumannCond[@id='"+str(ii)+"']/type").text)
            self.boundaryNeumann[ii] = dict(list(zip(component, value)))
            if self.quasistatic == 'YES':
                self.boundaryNeumann[ii]['evol'] = [float(e) for e in root.find("structure/neumann/neumannCond[@id='"+str(ii)+"']/evolution").text.split(' ')]
                #self.boundaryNeumann[ii]['evol'] = eval(root.find("structure/neumann/neumannCond[@id='"+str(ii)+"']/evolution").text)
            else:
                self.boundaryNeumann[ii]['evol'] = [1.]
        self.boundaryNeumann['all'] = allNeum
        ### Interfaces
        self.defaultInterface,self.interface = loadInterfaceFromXML(path)

        osym("mkdir -p "+self.resDir)
        osym("mkdir -p "+self.resDir+"MED")
        osym("mkdir -p "+self.resDir+"Fields/Fields_{}".format(rank))
        osym("mkdir -p "+self.resDir+"Resumption/Resumption_{}".format(rank))


        self.set_logger()

    def set_logger(self):
        """
        Define a logger to print message in log file
        """
        # création de l'objet logger qui va nous servir à écrire dans les logs
        self.logger = logging.getLogger("latin")
        # on met le niveau du logger à DEBUG, comme ça il écrit tout
        self.logger.setLevel(logging.DEBUG)
        # création d'un formateur qui va ajouter le temps, le niveau
        # de chaque message quand on écrira un message dans le log
        self.formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
        # création d'un handler qui va rediriger une écriture du log vers
        # un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
        self.file_handler = RotatingFileHandler(self.resDir+'LOG.log', 'w', 1000000, 1)
        # on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
        # créé précédement et on ajoute ce handler au logger
        self.file_handler.setLevel(logging.DEBUG)
        self.file_handler.setFormatter(self.formatter)
        self.logger.addHandler(self.file_handler)

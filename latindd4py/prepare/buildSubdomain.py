#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 14:18:53 2017
Class of subdomain
@author: oumaziz
"""
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as spl
import shelve
from os.path import isfile
from .mesh_model import struct, Value
from latindd4py.aster_functions import basics
from latindd4py.aster_functions.interface_field import pythonField
from latindd4py.aster_functions.solve_mechanics import imposed_boundary_conditions,problemOnDoubleSolePython,problemOnSolePython
from latindd4py.aster_functions.basics import assign_model,block_gno,compute_matele,computeDirichlet,computeNeumann,compute_dirichlet_right_hand_side,compute_neumann_right_hand_side,compute_nume_ddl,compute_stiffness,extract_stiffness_on_nodes, factorize, mergeArrayOnNodes, schurComplement
from latindd4py.aster_functions.save_data import saveField2MED
from latindd4py.aster_functions.materials import assign_material_dict,createAsterMaterial
from . import globalOperator as GO
import pdb
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# =============================================================================
# Class Subdomain
# =============================================================================

class Subdomain():
    """
    Class to define and build subdomains
    """
    def __init__(self,subStructuredMesh,interface,param):
        """
        Initialisation and construction of the class
        Input:
            -subStructuredMesh: class containing the mesh of the subdomain
            and informations of connectivities
            - interface: dictionnary of instance of interface class
            -param: class of parameters
        Return the object Subdomain
        """
        param.logger.info(" Creation of the sub-domain's class")
        self.Operator = dict()                                                 # Dictionnay of python operators
        self.dim = param.dim                                                   # Set the dimension of the problem
        ### Connectivty with the interfaces and subdomains
        self.connecInterf = subStructuredMesh.connecSDInterface[rank]
        self.neighbourSD = subStructuredMesh.neighbourSD[rank]
        ### Connectivity of materials
        self.connecMaterial = subStructuredMesh.connecMaterial['SD']
        ### Dimension of the problem
        self.dim = param.dim
        ### Relaxation coefficient
        self.relaxation = param.relaxation
        ### Quasistaic assumptions
        self.quasistatic = param.quasistatic
        self.numberTimeStep = param.numberTimeStep
        self.listTimeSteps = param.listTimeSteps
        ### Search directions
        self.searchDirections = param.searchDirections
        ### Connectivity with the boundary conditions
        ## Neumann
        self.neighbourNeum = subStructuredMesh.neighbourNeum[rank]                    # Global Neumann boundary conditions from the pre-processing
        self.neighbourNeum = list((set(self.neighbourNeum) & set(param.boundaryNeumann.keys())) | set(param.boundaryNeumann['all'])) # Selection of the BC defined in the input file
        ## Dirichlet
        self.neighbourDiri = subStructuredMesh.neighbourDiri[rank]                    # Global Dirichlet boundary conditions
        self.neighbourDiri = list(set(self.neighbourDiri) & set(param.boundaryDirichlet.keys())) # Selection of the BC defined in the input file

        ### Load soles instance from the interface class
        param.logger.info("          Import soles from interfaces")
        self.set_soles(interface)
        ### Build the structure for the subdomain with the attached soles
        param.logger.info("          Compute the factorization of the subdomain + soles")
        self.set_soleSD(subStructuredMesh,param)
        ### If different upper and down search direction
        if param.searchDirections:
            for jj in range(len(self.neighbourSD)):
                interface[jj].sole = {0:interface[jj].sole[1], 1:interface[jj].sole[0]}
        ### Initialisation of interface field
        param.logger.info("          Initialize the fields of interface")
        self.init_field(param)
        ### Compute the trace operators for the subdomain
        self.set_trace()
        ### Compute global operators for the multiscale
        if param.multiscale == 'YES':
            self.macroType = param.macroBasis
            if param.macroBasis == 'GENEO':
                soleSDGNO = [self.soleSD.mesh.GNO['BN{}'.format(jj)] for jj in range(len(self.connecInterf))]
                if self.neighbourDiri:
                    soleSDGNO += [self.soleSD.mesh.GNO['Wd{}'.format(jj)] for jj in range(len(self.neighbourDiri))]
                schurSD,schurLayer,subMatrixSD,schurHup = self.prepareGENEO(soleSDGNO,subStructuredMesh.Mail,param)
                [ww,vv] = sp.linalg.eigsh(schurSD,k=30,M=schurLayer+schurHup,sigma=1.,which='LM',maxiter=10000)
                np.savetxt(param.resDir+"eigenValuesGeneo.txt",ww)
                ### Extract the interesting modes
                eigMacro = ww[ww < param.limitSpectrum]
                if eigMacro.shape[0] == 0:
                    modesMacro = vv[:,[0,1,2]]
                    eigMacro = ww[[0,1,2]]
                else:
                    modesMacro = vv[:,list(range(eigMacro.shape[0]))]
                ### Compute the macro modes on the whole subdomain
                W = dict()
                self.Macro = {rank:dict()}
                self.Macro['fieldNumberMacro'] = pythonField(modesMacro.shape[1] * np.ones(self.dim * len(self.soleSD.mesh.NO['N_SD'])),self.soleSD.mesh.NO['N_SD']).convert2aster(param)

                for ii in range(len(self.connecInterf)):
                    self.Macro[rank][ii] = dict()
                    self.Macro[rank]['ConnecMacro'] = list(range(modesMacro.shape[1]))
                for jj in range(len(self.connecInterf)):
                    for pp in range(modesMacro.shape[1]):
                        Nx = pythonField(modesMacro[:,pp].ravel(),self.soleSD.mesh.NO['N']).extract_field_on_nodes(self.sole[jj].mesh.NO['N'])
                        HHNx = problemOnSolePython(self.sole[jj],Nx,param,case='displacement') + problemOnSolePython(self.soleUp[jj],Nx,param,case='displacement')
                        self.Macro[rank][jj][pp] = pythonField(HHNx.valField.dot(eigMacro[pp]),self.sole[jj].mesh.NO['N'])
                    self.Macro[rank][jj]['ConnecMacro'] = list(range(modesMacro.shape[1]))



#                for ii,neighbour in enumerate(self.neighbourSD):
#                    modesII = HmSHx.extract_field_on_nodes(self.sole[ii].mesh.NO['N'])
#                    for jj in range(modesII.valField.shape[1]):
#                        self.Macro[rank][ii][jj] = problemOnSolePython(self.sole[ii],pythonField(modesII.valField[:,jj],self.sole[ii].mesh.NO['N']),param,case='displacement')
#                    self.Macro[rank][ii]['ConnecMacro'] = range(modesMacro.shape[1])


#                    modesForNeighbour = list()
#                    countModes = 0
#                    for jj in range(modesII.valField.shape[1]):
#                        modesForNeighbour.append(problemOnSolePython(self.sole[ii],pythonField(modesII.valField[:,jj],self.sole[ii].mesh.NO['N']),param,case='force'))
#                    modesNeighbour = comm.sendrecv(modesForNeighbour,dest=neighbour,sendtag=rank,recvtag=neighbour)
#                    for mode in modesNeighbour:
#                        mode.nodes = modesII.nodes
#                    for jj,modes in enumerate([pythonField(modesII.valField[:,pp],self.sole[ii].mesh.NO['N']) for pp in range(modesII.valField.shape[1])] + modesNeighbour):
##                        print('{} {} {}'.format(rank,ii,jj))
##                        pdb.set_trace()
#                        countModes += 1
#                        self.Macro[rank][ii][jj] = problemOnDoubleSolePython(interface[ii].doubleSole,interface[ii].sole,modes,param)[0][0]
#                    self.Macro[rank][ii]['ConnecMacro'] = range(countModes)
#                self.Macro[rank]['ConnecMacro'] = range(sum([self.Macro[rank][ii]['ConnecMacro'][-1]+1  for ii in range(len(self.connecInterf))]))
                for jj in range(modesMacro.shape[1]):
##                    HmSHxDict = pythonField(HmSHx[:,jj].ravel(),self.soleSD.mesh.NO['N'])
##                    HmSHxList = list()
##                    for ii in range(len(self.connecInterf)):
##                        HmSHxList.append(HmSHxDict.extract_field_on_nodes(self.sole[ii].mesh.NO['N']))
##                        self.Macro[rank][ii][jj] = problemOnDoubleSolePython(interface[ii].doubleSole,interface[ii].sole,problemOnSolePython(self.sole[ii],HmSHxDictjj,param),param)[0][0]
                    W[jj] = pythonField(modesMacro[:,jj].ravel(),self.soleSD.mesh.NO['N'])
                    listInternalNodes = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(self.soleSD.mesh.NO['N_SD_tot'])-set(W[jj].nodes))])))]
                    ### Compute the macro modes on the whole subdomain
                    uii = pythonField(-subMatrixSD['luKii'].solve(subMatrixSD['Kib'].dot(W[jj].valField)),listInternalNodes)
                    Ujj = uii.mergeField(W[jj]).extract_field_on_nodes(self.soleSD.mesh.NO['N_SD'])
                    UjjAster = Ujj.convert2aster(param)
                    saveField2MED(UjjAster,self.SD.model.MODELE,self.SD.model.assigned_material,param.resDir+"macro{}.med".format(jj),param)

#                for ii in range(len(self.connecInterf)):
#                    self.Macro[rank][ii] = dict()
#                    self.Macro[rank][ii]['ConnecMacro'] = range(modesMacro.shape[1])
#                    for jj in range(modesMacro.shape[1]):
#                        self.Macro[rank][ii][jj] = W[jj].extract_field_on_nodes(self.sole[ii].mesh.NO['N'])
                ### Assembly of the local basis of macro modes
#                self.Operator['W'] = np.concatenate([np.concatenate([np.reshape(self.Macro[rank][jj][ii].valField,(self.Macro[rank][jj][ii].valField.shape[0],1)) for ii in self.Macro[rank][jj]['ConnecMacro']],axis=1) for jj in range(len(self.connecInterf))],axis=0)
                ### Allgather to communicate the number of modes per subdomain
                param.logger.info("          Get number modes per subdomain")
                self.listNumberModes = comm.allgather(len(self.Macro[rank]['ConnecMacro']))
#                numberModesPerInterface = np.zeros(param.numberInterface,dtype=np.int8)
#                numberModesPerInterface[self.connecInterf] = [len(self.Macro[rank][jj]['ConnecMacro']) for jj in range(len(self.connecInterf))]
#                self.listNumberModes = comm.allreduce(numberModesPerInterface,op=MPI.SUM).tolist()
#                self.listNumberModes = [kk / 2 for kk in self.listNumberModes]
                self.asseMacroBaseSD(interface,param)
                self.compute_global_operators(param)
            else:
                param.logger.info("          Import macro basis from interfaces")
                self.get_macro_basis(interface,param)
                ### Allgather to communicate the number of modes per subdomain
                param.logger.info("          Get number modes per interface")
                numberModesPerInterface = np.zeros(param.numberInterface,dtype=np.int8)
                numberModesPerInterface[self.connecInterf] = [len(self.Macro[rank][jj]['ConnecMacro']) for jj in range(len(self.connecInterf))]
                self.listNumberModes = comm.allreduce(numberModesPerInterface,op=MPI.SUM).tolist()
                self.listNumberModes = [kk // 2 for kk in self.listNumberModes]
                param.logger.info("          Assemble the macro basis")
                self.asseMacroBaseSD()
                self.compute_global_operators(param)
        param.logger.info(" End of creation of the sub-domain's class")


    def asseMacroBaseSD(self,interface=None,param=None):
        """
        Assembly of the macro basis over the sub-domain to get AtW as a sparse
        matrix.
        """
        if self.macroType == "GENEO":
            self.Operator['AtW'] = dict()
            for jj,SD in enumerate(self.neighbourSD):
                self.Macro[SD] = comm.sendrecv(self.Macro[rank][jj],
                                               dest=SD,
                                               sendtag=rank,
                                               recvtag=SD)
                ## Update the numerotation of the fields to the one of the current subdomain
                for pp in self.Macro[SD]['ConnecMacro']:
                    self.Macro[SD][pp].nodes = self.Macro[rank][jj][self.Macro[rank][jj]['ConnecMacro'][0]].nodes
                    self.Macro[SD][pp] = problemOnDoubleSolePython(interface[jj].doubleSole,interface[jj].sole,self.Macro[SD][pp],param)[0][0]
                for pp in self.Macro[rank]['ConnecMacro']:
                    self.Macro[rank][jj][pp] = problemOnDoubleSolePython(interface[jj].doubleSole,interface[jj].sole,self.Macro[rank][jj][pp],param)[0][0]

                Wneighbour = np.concatenate([np.reshape(self.Macro[SD][ii].valField,(self.Macro[SD][ii].valField.shape[0],1))
                                        for ii in range(len(self.Macro[SD]['ConnecMacro']))],
                                       axis=1)
                Wrank = np.concatenate([np.reshape(self.Macro[rank][jj][ii].valField,(self.Macro[rank][jj][ii].valField.shape[0],1))
                                        for ii in range(len(self.Macro[rank][jj]['ConnecMacro']))],
                                       axis=1)

                if rank < SD:
                    self.Operator['AtW'][jj] = np.concatenate((Wrank,Wneighbour),axis=1)
                else:
                    self.Operator['AtW'][jj] = np.concatenate((Wneighbour,Wrank),axis=1)
            AtWg = sp.lil_matrix((sum([self.dim*len(self.soleSD.mesh.NO['N{}'.format(jj)]) for jj in range(len(self.neighbourSD))]),sum([self.listNumberModes[jj] for jj in self.neighbourSD+[rank]])))
            posColIni = 0
            listSD = sorted([rank]+self.neighbourSD)
            for jj,SD in enumerate(self.neighbourSD):
                posCol = []
                posLine = []
                for kk in sorted([rank,SD]):
                    posColIni = sum([self.listNumberModes[pp] for pp in listSD[:listSD.index(kk)]])
                    posCol += list(range(posColIni,posColIni+self.listNumberModes[kk]))
                posLineIni = sum([self.dim*len(self.soleSD.mesh.NO['N{}'.format(pp)]) for pp in range(sorted(self.neighbourSD).index(SD))])
                posLine += list(range(posLineIni,posLineIni+self.dim*len(self.soleSD.mesh.NO['N{}'.format(jj)])))
                AtWg[np.ix_(posLine,posCol)] = self.Operator['AtW'][jj]
            self.Operator['AtW']['localSD'] = AtWg
        else:
            self.Operator['AtW'] = sp.block_diag([np.concatenate([np.reshape(self.Macro[rank][jj][self.Macro[rank][jj]['ConnecMacro'][pp]].valField,(self.Macro[rank][jj][self.Macro[rank][jj]['ConnecMacro'][pp]].valField.shape[0],1)).T for pp in range(len(self.Macro[rank][jj]['ConnecMacro']))],axis=0) for jj in range(len(self.connecInterf))],format="csr").T

    def compute_global_operators(self,param):
        """
        Compute the global operators for the multiscale approach. Add elements
        in the Operator dictionnary
        Input:
            -param: class of parameters
        """
        self.global_operators = GO.globalOperator(self,param)

    def createMatDict(self,connecMat,param):
        """
        Create the dictionnary of aster material
        Input:
            -connecMat: python list of connectivity of material
            -param: class of parameters
        Output: return a dictionnary where the keys are the nodes groups and
        the values the aster material
        """
        matDict = dict()
        for ii in connecMat:
            if ii in list(param.material.keys()):
                dict_mat = param.material[ii]
            elif ii == 0:
                dict_mat = param.defaultMaterial[rank]
            asterMat = createAsterMaterial(dict_mat)
            matDict[self.soleSD.mesh.GMA['M{}'.format(ii)]] = asterMat
        return matDict

    def createRHS(self,param):
        """
        Create dictionnaries of the right-hand-sides for the linear stage
        Input:
            -param: class of parameters
        """
        self.soleSD.model.neumann = computeNeumann(self.soleSD.model,self.soleSD.mesh,self.neighbourNeum,param)
        self.soleSD.model.dirichlet = computeDirichlet(self.soleSD.model,param)

    def get_macro_basis(self,interface,param):
        """
        Compute the macro basis for the interface. Add an attribute in the
        class Subdomain.
        Input:
            -param: class of parameters

        En TRAVAUX
        """
        self.Macro = {rank:dict()}
        for jj in range(len(self.connecInterf)):
            self.Macro[rank][jj] = interface[jj].Macro

    def init_field(self,param):
        """
        Make the initialiaztion of the fields of interface. Add an attribute
        Value of the class Subdomaine.
        Input:
            -param: class of parameters
        """
        if param.resumption and isfile(param.resPrepare+'Resumption/Resumption_{}/resumption_{}'.format(rank,rank)):
            resumption = shelve.open(param.resPrepare+'Resumption/Resumption_{}/resumption_{}'.format(rank,rank))
            self.Value = resumption['linear']
            resumption.close()
        else:
            self.Value = Value(param)
            for tt in range(-1,param.numberTimeStep):						# Boucle sur les pas de temps
                self.Value.W[tt] = dict()
                self.Value.V[tt] = dict()
                self.Value.T[tt] = dict()
                self.Value.Tw[tt] = dict()
                self.Value.F[tt] = dict()
                self.Value.Wc[tt] = dict()
                self.Value.Vc[tt] = dict()
                self.Value.Tc[tt] = dict()
                self.Value.Twc[tt] = dict()
                self.Value.Fc[tt] = dict()
                for jj in range(len(self.connecInterf)):
                    size_interface = len(self.soleSD.mesh.NO['N'+str(jj)])
                    no = self.soleSD.mesh.NO['N'+str(jj)]
                    self.Value.W[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.V[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.T[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Tw[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.F[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Wc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Vc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Tc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Twc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Fc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.W[tt]['W'] = pythonField(np.zeros(len(self.soleSD.mesh.NO['N_SD']) * param.dim),self.soleSD.mesh.NO['N_SD'])

    def prepareGENEO(self,listGNOboundary,mesh,param):
        """
        Prepare theSchr complement to solve the generalized eigenvalue problem
        Input:
            -listGNOboundary: list of the nodes groups where to impose the
            boundary blocking
            -mesh: mesh to construct the models
        Output:
            -Dense matrices corresponding to the schur complement of the
            subdomain and the soles
        """
        ### Creation of the whole layer of soles
        modLayer = assign_model([self.soleSD.mesh.GMA['S-{}'.format(ii)] for ii in range(len(self.connecInterf))],mesh,param)
        matDictLayer = dict()
        for ii in range(len(self.neighbourSD)):
            matDictLayer.update(self.sole[ii].model.matDict)
#        matLayer = assign_material_dict(dict(zip([self.soleSD.mesh.GMA['S-{}'.format(ii)] for ii in range(len(self.connecInterf))],[self.sole[ii].model.mat for ii in range(len(self.connecInterf))])),mesh)
        matLayer = assign_material_dict(matDictLayer,mesh)
        ### Dualization of the Dirichlet's boundary conditions
        blockLayerGeneo = block_gno(modLayer,listGNOboundary[:len(self.connecInterf)],param.dim)
        matele = compute_matele(modLayer,matLayer)
        nume_ddl = compute_nume_ddl(matele)
        stiffness_layer = compute_stiffness(matele,nume_ddl,[blockLayerGeneo])
        factorize(stiffness_layer,param.solver)
        K = stiffness_layer.EXTR_MATR(sparse=True)
        K_layer = sp.csr_matrix(sp.coo_matrix((K[0],(K[1],K[2])),shape=(param.dim * len(self.soleSD.mesh.NO['layer']),param.dim * len(self.soleSD.mesh.NO['layer']))))

        schurSD,subMatrixSD = schurComplement(self.soleSD.model.K_sparse,self.soleSD.mesh.NO['N'],self.soleSD.mesh.NO['N_SD_tot'],allSubMatrix='True')
        schurLayer = schurComplement(K_layer,self.soleSD.mesh.NO['N'],self.soleSD.mesh.NO['layer'])

        schurHu = list()
        for jj,SD in enumerate(self.neighbourSD):
            schurHu.append(schurComplement(self.soleUp[jj].model.stiffness_python,self.soleUp[jj].mesh.NO['N'],self.soleUp[jj].mesh.NO['Nt']))
        schurHup = mergeArrayOnNodes(schurHu,[self.sole[jj].mesh.NO['N'] for jj,SD in enumerate(self.neighbourSD)])
        return schurSD,schurLayer,subMatrixSD,schurHup

    def set_soles(self,interface):
        """
        Load the sole structure from the interface instance
        Input:
            - interface: dictionnary of the interface instance of subdomain
        """
        self.sole = dict()
        self.soleUp = dict()
        for jj in range(len(self.neighbourSD)):
            ## Sole for the linear problem on the subdomain
            self.sole[jj] = interface[jj].sole[interface[jj].connecSD.index(rank)]
            ## Sole for the upper search direction
            # If different upper and down search direction
            if self.searchDirections:
                self.soleUp[jj] = interface[jj].sole[1-interface[jj].connecSD.index(rank)]
            else:
                self.soleUp[jj] = interface[jj].sole[interface[jj].connecSD.index(rank)]

    def set_soleSD(self,subStructuredMesh,param):
        """
        Build the class for the subdomain plus the attached soles
        Input:
            -subStructuredMeshtructuredMesh: mesh and mesh information
            -param: class of parameters
        """
        self.soleSD = struct()
        self.SD = struct()
        ### Lists of group names for a further use
        soleSDGMA = []
        soleSDInterfGNO = []
        soleSDGNO = []
        ### Load the groups of nodes and elements
        ### GMA
        gma = dict()
        gno = dict()
        ### Importation of the groups about the sub-domains
        gma['S'] = ('S'+str(rank)).ljust(24,' ')               # Importation of the meshes group of the sub-domain
        gno['b'] = ('bN'+str(rank)).ljust(24,' ')               # Importation of the node groups of the inside edge of the sub-domain
        ### Importation of the groups of elements for the materials
        for jj in self.connecMaterial:
            gma['M{}'.format(jj)] = ('M{}_{}'.format(jj,rank)).ljust(24,' ')
        ### En attente des fonctions dans asterXX
#        self.soleSD.mesh.NO['N'] = ['N'+str(ii) for ii in sorted(map(int, subStructuredMesh.Mail.sdj.GROUPENO.get()[self.soleSD.mesh.GNO['b']]))]            # Liste des NO du bord interieur
#        self.soleSD.mesh.NO['N_SD'] = ['N'+str(ii) for ii in sorted(map(int, subStructuredMesh.Mail.sdj.GROUPENO.get()[('GN'+str(rank)).ljust(24,' ')]))]     # Liste des NO du SD
        no = dict()
        no['N'] = basics.get_nodes_from_gno(gno['b'],subStructuredMesh.Mail,param.dim)
        no['N_SD'] = basics.get_nodes_from_gno(('GN'+str(rank)).ljust(24,' '),subStructuredMesh.Mail,param.dim)

# Importation of the ponderation of search direction
#        self.pondDDR = [interf[jj].pondDDR for jj in range(len(self.connecInterf))]
        list_edges_soles = []
        for jj in range(len(self.connecInterf)):
            gma['S-'+str(jj)] = ('S{}_{}'.format(rank,self.neighbourSD[jj])).ljust(24,' ')
            soleSDGMA.append(gma['S-'+str(jj)])                # List of GMA of the soles related to the sub-domain
            gma['GM'+str(jj)] = ('GM{}_{}'.format(rank,self.neighbourSD[jj])).ljust(24,' ')
#            soleSDGMA.append(gma['GM'+str(jj)])
            ### GNO
            gno['GN'+str(jj)] = ('GN{}_{}'.format(rank,self.neighbourSD[jj])).ljust(24,' ')
            soleSDInterfGNO.append(gno['GN'+str(jj)])						# Liste des GNO d'interface pour le SD+sole
            gno['BN'+str(jj)] = ('BN{}_{}'.format(rank,self.neighbourSD[jj])).ljust(24,' ')
            soleSDGNO.append(gno['BN'+str(jj)])

            no['N'+str(jj)] = basics.get_nodes_from_gno(gno['GN'+str(jj)],subStructuredMesh.Mail,param.dim)
            no['N{}b'.format(jj)] = basics.get_nodes_from_gno(gno['BN'+str(jj)],subStructuredMesh.Mail,param.dim)
            list_edges_soles += no['N{}b'.format(jj)]
            ### En attente des fonctions dans asterXX
            ### NO
    #        self.soleSD.mesh.NO['N'+str(jj)] = ['N'+str(ii) for ii in sorted(map(int, subStructuredMesh.Mail.sdj.GROUPENO.get()[self.soleSD.mesh.GNO['GN'+str(jj)]]))]		# Liste des NO de l'interface jj du SD rank
        no['N_SD_tot'] = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(no['N_SD']+list_edges_soles))])))]
        no['BN'] = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(list_edges_soles))])))]
        no['layer'] = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(no['N']+list_edges_soles))])))]
        ### Importation of groups for boundary conditions
        ## Neumann
#        for jj in range(len(subStructuredMesh.neighbourNeum[rank])):
        for jj in range(len(self.neighbourNeum)):
            if not(jj in param.boundaryNeumann['all']):
                gno['Fd'+str(jj)] = ('FdN{}_{}'.format(subStructuredMesh.neighbourNeum[rank][jj],rank)).ljust(24,' ')	# Importation des NO des CL de Neumann
                gma['FdM'+str(jj)] = ('FdM{}_{}'.format(subStructuredMesh.neighbourNeum[rank][jj],rank)).ljust(24,' ')	# Importation des GMA de bord pour les CL de Neumann
                soleSDGMA.append(gma['FdM'+str(jj)])
        ## Dirichlet
#        for jj in range(len(subStructuredMesh.neighbourDiri[rank])):
        list_dirichlet = list()
        for jj in range(len(self.neighbourDiri)):
            gno['Wd'+str(jj)] = ('WdN{}_{}'.format(subStructuredMesh.neighbourDiri[rank][jj],rank)).ljust(24,' ')	# Importation des NO des CL de Dirichlet
            gma['WdM'+str(jj)] = ('WdM{}_{}'.format(subStructuredMesh.neighbourDiri[rank][jj],rank)).ljust(24,' ')	# Importation des GMA de bord pour les CL de Neumann
#            soleSDGNO.append(gno['Wd{}'.format(jj)])                           # Save the potential nodes groups to block ATTENTION si condition imposée non nulle ça va chier dans la colle
            no['Wd{}'.format(jj)] = basics.get_nodes_from_gno(gno['Wd'+str(jj)],subStructuredMesh.Mail,param.dim)
            list_dirichlet += no['Wd{}'.format(jj)]
        nodesDiri = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in (list_dirichlet+list_edges_soles)])))]
        self.soleSD.mesh.set_gma(gma)
        self.soleSD.mesh.set_gno(gno)
        self.soleSD.mesh.set_no(no)

        ### Assign model on the subdomain and the attached soles
        self.soleSD.model.set_model(soleSDGMA+[self.soleSD.mesh.GMA['S']],subStructuredMesh.Mail,param)
        self.SD.model.set_model(self.soleSD.mesh.GMA['S'],subStructuredMesh.Mail,param)
        ### Assign the marerial
        matDict = dict()
        for jj in  range(len(self.connecInterf)):
            matDict.update(self.sole[jj].model.matDict)
        matSD = self.createMatDict(self.connecMaterial,param)
        matDict.update(matSD)
        self.soleSD.model.assigned_material = assign_material_dict(matDict,subStructuredMesh.Mail)
        self.SD.model.assigned_material = assign_material_dict(matSD,subStructuredMesh.Mail)
        ### Assign blocked Dirichlet boundary conditions on soles
        self.soleSD.model.set_blocked_dirichlet_boundary_conditions(soleSDGNO,self.dim)
        ### Assign potential imposed Dirichlet conditions on the subdomain
        if self.neighbourDiri:
            self.soleSD.model.set_imposed_dof([self.soleSD.mesh.GNO['Wd{}'.format(ii)] for ii in range(len(self.neighbourDiri))],self.neighbourDiri,param)
        ### Compute the assembly stiffness matrix
        self.soleSD.model.set_assemblyStiffnessMatrix()
        ### Save the part of the stiffness for non-zeros dirichlet
        if self.neighbourDiri:
            K = self.soleSD.model.stiffness.EXTR_MATR(sparse=True)
            K_sparse = sp.lil_matrix(sp.coo_matrix((K[0],(K[1],K[2])),shape=(param.dim * len(self.soleSD.mesh.NO['N_SD_tot']),param.dim * len(self.soleSD.mesh.NO['N_SD_tot']))))
            self.soleSD.model.stiffness_extr_diri = extract_stiffness_on_nodes(K_sparse,self.soleSD.mesh.NO['N_SD_tot'],self.neighbourDiri,self.soleSD.mesh.NO,param)
        ### Factorization of the stiffness
        self.soleSD.model.set_factorization(param.solver)
        ### Store the sparse stiffness matrix
        K = self.soleSD.model.stiffness.EXTR_MATR(sparse=True)
        self.soleSD.model.K_sparse = sp.csc_matrix(sp.coo_matrix((K[0],(K[1],K[2])),shape=(param.dim * len(self.soleSD.mesh.NO['N_SD_tot']),param.dim * len(self.soleSD.mesh.NO['N_SD_tot']))))
        self.soleSD.model.K_sparse_lu = sp.linalg.splu(self.soleSD.model.K_sparse)
        self.soleSD.model.set_assemblyStiffnessMatrix(block_overlap = False,imposed_dirichlet = False)
        matPython = self.soleSD.model.stiffness_noOverlap.EXTR_MATR(sparse=True)
        self.soleSD.model.stiffness_noOverlap_python = sp.coo_matrix((matPython[0],(matPython[1],matPython[2])),shape=(max(matPython[1])+1,max(matPython[2])+1)).tocsr()

        ### Create the boundary conditions
        self.createRHS(param)

    def set_trace(self):
        """
        Compute the trace operators over the sub-domain with a python format
        """
        N = dict()
        N_SD = dict()
        Ninterf = dict()
        posInterf = 0
        ### Trace operators with the SD+soles
        dict_nodes = {node:pos for pos,node in enumerate(self.soleSD.mesh.NO['N_SD_tot'])}
        for jj in range(len(self.connecInterf)):
            pos_in_nodes = [dict_nodes[node] for node in self.soleSD.mesh.NO['N{}'.format(jj)]]
            pos_in_valField = [self.dim * ii+kk for ii in pos_in_nodes for kk in range(self.dim)]
            N[jj] = sp.lil_matrix((self.dim*len(self.soleSD.mesh.NO['N{}'.format(jj)]),self.dim*len(self.soleSD.mesh.NO['N_SD_tot'])))													# Création d'une matrice sparse pour l'opérateur de trace
            N[jj][list(range(self.dim*len(self.soleSD.mesh.NO['N{}'.format(jj)]))),pos_in_valField] = sp.lil_matrix(np.ones(N[jj].shape[0]))		# Remplissage de la matrice sparse

            numNodeInterf = len(self.soleSD.mesh.NO['N{}'.format(jj)])
            Ninterf[jj] = sp.lil_matrix((self.dim * numNodeInterf,sum([self.dim*len(self.soleSD.mesh.NO['N{}'.format(ii)]) for ii in range(len(self.connecInterf))])))
            Ninterf[jj][np.ix_(list(range(self.dim * numNodeInterf)),list(range(posInterf,posInterf+self.dim*numNodeInterf)))] = sp.identity(self.dim * numNodeInterf,format="lil")
            posInterf += self.dim * numNodeInterf

        pos_in_nodes = [dict_nodes[node] for node in self.soleSD.mesh.NO['N_SD']]
        pos_in_valField = [self.dim * ii+kk for ii in pos_in_nodes for kk in range(self.dim)]
        N['SD'] = sp.lil_matrix((self.dim*len(self.soleSD.mesh.NO['N_SD']),self.dim*len(self.soleSD.mesh.NO['N_SD_tot'])))             # Création d'une matrice sparse pour l'opérateur de trace
        N['SD'][list(range(self.dim*len(self.soleSD.mesh.NO['N_SD']))),pos_in_valField] = sp.lil_matrix(np.ones(N['SD'].shape[0]))     # Remplissage de la matrice sparse

        pos_in_nodes = [dict_nodes[node] for node in self.soleSD.mesh.NO['layer']]
        pos_in_valField = [self.dim * ii+kk for ii in pos_in_nodes for kk in range(self.dim)]
        N['interf'] = sp.lil_matrix((self.dim*len(self.soleSD.mesh.NO['layer']),self.dim*len(self.soleSD.mesh.NO['N_SD_tot'])))             # Création d'une matrice sparse pour l'opérateur de trace
        N['interf'][list(range(self.dim*len(self.soleSD.mesh.NO['layer']))),pos_in_valField] = sp.lil_matrix(np.ones(N['interf'].shape[0]))     # Remplissage de la matrice sparse

        ### Trace operator only with the SD
        dict_nodes = {node:pos for pos,node in enumerate(self.soleSD.mesh.NO['N_SD'])}
        for jj in range(len(self.connecInterf)):
            pos_in_nodes = [dict_nodes[node] for node in self.soleSD.mesh.NO['N{}'.format(jj)]]
            pos_in_valField = [self.dim * ii+kk for ii in pos_in_nodes for kk in range(self.dim)]
            N_SD[jj] = sp.lil_matrix((self.dim*len(self.soleSD.mesh.NO['N{}'.format(jj)]),self.dim*len(self.soleSD.mesh.NO['N_SD'])))     # Création d'une matrice sparse pour l'opérateur de trace
            N_SD[jj][list(range(self.dim*len(self.soleSD.mesh.NO['N{}'.format(jj)]))),pos_in_valField] = sp.lil_matrix(np.ones(N_SD[jj].shape[0]))		# Remplissage de la matrice sparse

        self.Operator['Ninterf'] = Ninterf
        self.Operator['N'] = N                                                 # Sauvegarde du dictionnaire des opérateurs de trace relatifs à chaque interface du SD
        self.Operator['N_SD'] = N_SD                                                 # Sauvegarde du dictionnaire des opérateurs de trace relatifs à chaque interface du SD
        self.Operator['Ne'] = sp.vstack(tuple(N_SD[jj] for jj in range(0,len(self.connecInterf)))).tocsc()
# =============================================================================
# Class SubdomainOverlap
# =============================================================================

class SubdomainOverLap():
    """
    Class to define and build subdomains
    """
    def __init__(self,subStructuredMesh,param):
        """
        Initialisation and construction of the class
        Input:
            -subStructuredMesh: class containing the mesh of the subdomain
            and informations of connectivities
            - interface: dictionnary of instance of interface class
            -param: class of parameters
        Return the object Subdomain
        """
        param.logger.info(" Creation of the sub-domain's class")
        self.Operator = dict()                                                 # Dictionnay of python operators
        self.dim = param.dim                                                   # Set the dimension of the problem
        ### Connectivty with the interfaces and subdomains
        self.neighbourSD = subStructuredMesh.neighbourSD[rank]
        ### Dimension of the problem
        self.dim = param.dim
        ### Connectivity with the boundary conditions
        ## Neumann
        self.neighbourNeum = subStructuredMesh.neighbourNeum[rank]                    # Global Neumann boundary conditions
        self.neighbourNeum = list(set(self.neighbourNeum) & set(param.boundaryNeumann.keys())) # Selection of the BC defined in the input file
        ## Dirichlet
        self.neighbourDiri = subStructuredMesh.neighbourDiri[rank]                    # Global Dirichlet boundary conditions
        self.neighbourDiri = list(set(self.neighbourDiri) & set(param.boundaryDirichlet.keys())) # Selection of the BC defined in the input file
        ### Build the structure of the subdomain and computation factorizations
        param.logger.info("          Compute the factorization of the subdomain")
        self.set_soleSD(subStructuredMesh,param)
        ### Initialisation of the fields
        self.init_field(param)

    def init_field(self,param):
        """
        Make the initialiaztion of the fields of interface. Add an attribute
        Value of the class Subdomaine.
        Input:
            -param: class of parameters
        """
        self.Value = Value()
        for tt in range(-1,param.numberTimeStep):						# Boucle sur les pas de temps
            self.Value.W[tt] = dict()
            self.Value.V[tt] = dict()
            self.Value.T[tt] = dict()
            self.Value.Tw[tt] = dict()
            self.Value.F[tt] = dict()
            self.Value.Wc[tt] = dict()
            self.Value.Vc[tt] = dict()
            self.Value.Tc[tt] = dict()
            self.Value.Twc[tt] = dict()
            self.Value.Fc[tt] = dict()
            for jj in range(len(self.neighbourSD)):
                size_interface = len(self.soleSD.mesh.NO['N{}b'.format(jj)])
                no = self.soleSD.mesh.NO['N{}b'.format(jj)]
                self.Value.W[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.V[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.T[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.Tw[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.F[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.Wc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.Vc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.Tc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.Twc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                self.Value.Fc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
            self.Value.W[tt]['W'] = pythonField(np.zeros(len(self.soleSD.mesh.NO['N_SD']) * param.dim),self.soleSD.mesh.NO['N_SD'])
            self.Value.W[tt]['all_nodes'] = pythonField(np.zeros(len(self.soleSD.mesh.NO['all_nodes']) * param.dim),self.soleSD.mesh.NO['all_nodes'])

    def set_soleSD(self,subStructuredMesh,param):
        """
        Build the class for the subdomain plus the attached soles
        Input:
            -subStructuredMeshtructuredMesh: mesh and mesh information
            -param: class of parameters
        """
        self.soleSD = struct()
        ### Lists of group names for a further use
        soleSDGMA = []
#        soleSDInterfGNO = []
        soleSDGNO = []
        ### Load the groups of nodes and elements
        gma = dict()
        gno = dict()
        no = dict()
        ### Importation of the groups about the sub-domains
        gma['S'] = ('S'+str(rank)).ljust(24,' ')                               # Importation of the meshes group of the sub-domain
        gma['Sb'] = ('S{}b'.format(rank)).ljust(24,' ')                        # Importation of the whole overlap
        gma['Soverlap'] = 'Soverlap'.ljust(24,' ')                             # Importation of the subdomain + the whole overlap
        gno['b'] = ('bN{}'.format(rank).ljust(24,' '))                         # Importation of the node group of the whole edge of subdomain
        no['N_SD'] =  basics.get_nodes_from_gno('EXT_{}'.format(rank).ljust(24,' '),subStructuredMesh.Mail,param.dim)
        no['N'] =  basics.get_nodes_from_gno(gno['b'],subStructuredMesh.Mail,param.dim)
        no['b_ext'] = basics.get_nodes_from_gno('S{}bN'.format(rank),subStructuredMesh.Mail,param.dim)
        no['all_nodes'] = basics.get_nodes_from_gno('all_nodes',subStructuredMesh.Mail,param.dim)
        for jj in range(len(self.neighbourSD)):
            gno['GN{}'.format(jj)] = ('GN{}_{}'.format(rank,self.neighbourSD[jj]).ljust(24,' '))
            gno['BN{}'.format(jj)] = ('EXT_{}'.format(self.neighbourSD[jj])).ljust(24,' ')
            no['N{}'.format(jj)] = basics.get_nodes_from_gno(gno['GN'+str(jj)],subStructuredMesh.Mail,param.dim)
            no['N{}b'.format(jj)] = basics.get_nodes_from_gno(gno['BN'+str(jj)],subStructuredMesh.Mail,param.dim)
            soleSDGNO.append(gno['BN{}'.format(jj)])
        for jj in range(len(self.neighbourNeum)):
            gno['Fd'+str(jj)] = ('FdN{}_{}'.format(subStructuredMesh.neighbourNeum[rank][jj],rank)).ljust(24,' ')	# Importation des NO des CL de Dirichlet
            gma['FdM'+str(jj)] = ('FdM{}_{}'.format(subStructuredMesh.neighbourNeum[rank][jj],rank)).ljust(24,' ')	# Importation des GMA de bord pour les CL de Dirichlet
            soleSDGMA.append(gma['FdM'+str(jj)])
        ## Dirichlet
        if self.neighbourDiri:
            nodesSD = []
            for jj in range(len(self.neighbourDiri)):
                gno['Wd'+str(jj)] = ('WdN{}_{}'.format(subStructuredMesh.neighbourDiri[rank][jj],rank)).ljust(24,' ')	# Importation des NO des CL de Dirichlet
                gma['WdM'+str(jj)] = ('WdM{}_{}'.format(subStructuredMesh.neighbourDiri[rank][jj],rank)).ljust(24,' ')	# Importation des GMA de bord pour les CL de Neumann
                nodesSD += basics.get_nodes_from_gno(gno['Wd{}'.format(jj)],param.mesh,param.dim)
            no['SD_without_diri'] = list(set(no['N_SD'])-set(nodesSD))
        else:
            no['SD_without_diri'] = no['N_SD']
        self.soleSD.mesh.set_no(no)
        self.soleSD.mesh.set_gma(gma)
        self.soleSD.mesh.set_gno(gno)
        ### Assign model on the subdomain and the overlap
        self.soleSD.model.set_model(soleSDGMA+[self.soleSD.mesh.GMA['S'],self.soleSD.mesh.GMA['Sb']],subStructuredMesh.Mail,param)
        ### Assign the material
        self.soleSD.model.set_material(param.material['E'][rank],param.material['nu'][rank],[self.soleSD.mesh.GMA['S'],self.soleSD.mesh.GMA['Sb']],subStructuredMesh.Mail,param)
        ### Assign blocked Dirichlet boundary conditions on the edge of overlap
        self.soleSD.model.set_blocked_dirichlet_boundary_conditions(soleSDGNO,self.dim)
        ### Assign potential imposed Dirichlet conditions on the subdomain
        if self.neighbourDiri:
            self.soleSD.model.set_imposed_dof([self.soleSD.mesh.GNO['Wd{}'.format(ii)] for ii in range(len(self.neighbourDiri))],self.neighbourDiri,param)
#        else:
#            self.soleSD.model.boundary_conditions['imposed_dirichlet'] = None
#            self.soleSD.model.boundary_conditions['imposed_dirichlet_meca'] = None
        ### Compute the assembly stiffness matrix
        self.soleSD.model.set_assemblyStiffnessMatrix()
        ### Factorization of the stiffness
        self.soleSD.model.set_factorization(param.solver)
        ### Compute the assembly stiffness matrix
        self.soleSD.model.set_assemblyStiffnessMatrix(block_overlap = False)
        ### Factorize to take into account dirichlet boundary conditions
#        factorize(self.soleSD.model.stiffness_noOverlap,param.solver)
        ### Initiate the righ-hand-side vector for the residual
        self.f = pythonField(np.zeros(self.dim*(len(self.soleSD.mesh.NO['all_nodes']))),self.soleSD.mesh.NO['all_nodes'])
        [neumann,dirichlet] = imposed_boundary_conditions(self,param)
        for neum in neumann:
            self.f.valField += compute_neumann_right_hand_side(self.soleSD.model.nume_ddl,self.soleSD.model.assigned_material,neum).EXTR_COMP().valeurs
        for diri in dirichlet:
            self.f.valField += compute_dirichlet_right_hand_side(self.soleSD.model.nume_ddl,diri).EXTR_COMP().valeurs

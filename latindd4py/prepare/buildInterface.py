#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 14:18:00 2017
Class of interfaces
@author: oumaziz
"""
import numpy as np
from os.path import isfile
import shelve
try:
    import code_aster
    from code_aster.Commands import AFFE_CARA_ELEM,AFFE_MODELE,CALC_MATR_ELEM,ASSE_MATRICE,NUME_DDL,_F
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

try:
    from lxml import etree as et
except:
    print("No module lxml")


import scipy.sparse as sp
from .mesh_model import struct, Value
from latindd4py.aster_functions import basics
from latindd4py.aster_functions.interface_field import pythonField
from latindd4py.aster_functions.materials import assign_material_dict, createAsterMaterial
import pdb
from collections import Counter
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

def loadPondDDR(xmlPath):
    """
    Loader for the weighting og search directions
    Input:
        -xmlPath: path in the xml file to select the interface
    """
    weighting = dict()
    if root.xpath(xmlPath+"/pondDDR"):
        if root.find(xmlPath+"/pondDDR").attrib['choice'] == 'YES':
            weighting['choice'] = True
            if root.find(xmlPath+"/pondDDR").attrib['up']:
                weighting['up'] = float(root.find(xmlPath+"/pondDDR").attrib['up'])
            else:
                weighting['up'] = 1.
            if root.find(xmlPath+"/pondDDR").attrib['down']:
                weighting['down'] = float(root.find(xmlPath+"/pondDDR").attrib['down'])
            else:
                weighting['down'] = False
        else:
            weighting['choice'] = False
            weighting['up'] = 1.
            weighting['down'] = False
    else:
        weighting['choice'] = False
        weighting['up'] = 1.
        weighting['down'] = False
    return weighting

def perfectLoader(xmlPath):
    """
    Loader for perfect interfaces
    Input:
        -xmlPath: path in the xml file to select the interface
    """
    ### Type of the interface
    interface = {'type' : 'PERFECT'}
    ### Weighting of search directions
    interface['pondDDR'] = loadPondDDR(xmlPath)

    return interface

def contactLoader(xmlPath):
    """
    Loader for interfaces of frictional contact
    Input:
        -xmlPath: path in the xml file to select the interface
    """
    ### Type of the interface
    interface = {'type' : 'CONTACT'}
    ### Weighting of search directions
    interface['pondDDR'] = loadPondDDR(xmlPath)
    ### Load the parameters of the interface
    interface['frictionCase'] = root.find(xmlPath+"/friction").attrib['choice']        # Import the case of friction 'YES' or 'NO'
    interface['preloadCase'] = root.find(xmlPath+"/gap").attrib['preload']          # Import the case of preload 'YES' or 'NO'
    interface['friction'] = float(root.find(xmlPath+"/friction").text)                 # Import the friction coefficient
    interface['gap'] = float(root.find(xmlPath+"/gap").text)                           # Import the initial gap of preload

    return interface

def cohesiveLoader(xmlPath):
    """
    Loader for cohesive interfaces
    Input:
        -xmlPath: path in the xml file to select the interface
    """
    ### Type of the interface
    interface = {'type' : 'COHESIVE'}
    ### Weighting of search directions
    interface['pondDDR'] = loadPondDDR(xmlPath)
    ### Load the parameters of the interface
    cohesiveLawLoader.get(root.find(xmlPath).attrib['law'])(xmlPath,interface)

    return interface

def allixLoader(xmlPath,interface):
    """
    Loader for the Allix's cohesive law
    Input:
        -xmlPath: path in the xml file to select the interface
        -interface: dictionnary to enrich with the parameters
    """
    ### Name of the cohesive law
    interface['law'] = 'Allix'
    ### Load the parameters of the cohesive law
    interface['initial_stiffness'] = float(root.find(xmlPath+"/initial_stiffness").text)          # Import the initial stiffness of the interface
    interface['fragility'] = float(root.find(xmlPath+"/fragility").text)                          # Import the fragility parameter
    interface['gamma1'] = float(root.find(xmlPath+"/gamma1").text)                                # Import parameter
    interface['gamma2'] = float(root.find(xmlPath+"/gamma2").text)                                # Import parameter
    interface['alpha'] = float(root.find(xmlPath+"/alpha").text)                                  # Import parameter
    interface['Y_critique'] = float(root.find(xmlPath+"/Y_critique").text)
    ### Load the choice of the local stage
    interface['solver'] = root.find(xmlPath).attrib['solver']

def allixBuilder(interface,dictParamInterface):
    """
    Builder for the Allix's cohesive law
    Input:
        -interface: class Interface in construction
        -dictParamInterface : dictionnary with the parameters of the interface,
        come from the class of parameters
    Output:
        Enrich the class interface
    """
    interface.initial_stiffness = dictParamInterface['initial_stiffness']
    interface.fragility = dictParamInterface['fragility']
    interface.gamma1 = dictParamInterface['gamma1']
    interface.gamma2 = dictParamInterface['gamma2']
    interface.alpha = dictParamInterface['alpha']
    interface.Y_critique = dictParamInterface['Y_critique']
    interface.solver = dictParamInterface['solver']
    interface.damage = dict()

def perfectBuilder(interface,dictParamInterface):
    """
    Builder to affect parameters for perfect interfaces
    Input:
        -interface: class Interface in construction
        -dictParamInterface : dictionnary with the parameters of the interface,
        come from the class of parameters
    Output:
        Enrich the class interface
    """
    interface.Type = dictParamInterface['type']
    interface.pondDDR = dictParamInterface['pondDDR']

def contactBuilder(interface,dictParamInterface):
    """
    Builder to affect parameters for contact interfaces
    Input:
        -interface: class Interface in construction
        -dictParamInterface : dictionnary with the parameters of the interface,
        come from the class of parameters
    Output:
        Enrich the class interface
    """
    interface.Type = dictParamInterface['type']
    interface.pondDDR = dictParamInterface['pondDDR']
    interface.gap = dictParamInterface['gap']
    interface.frictionCase = dictParamInterface['frictionCase']
    if interface.frictionCase == 'YES':
        interface.friction = dictParamInterface['friction']
    interface.preloadCase = dictParamInterface['preloadCase']

def cohesiveBuilder(interface,dictParamInterface):
    """
    Builder to affect parameters for cohesive interfaces
    Input:
        -interface: class Interface in construction
        -dictParamInterface : dictionnary with the parameters of the interface,
        come from the class of parameters
    Output:
        Enrich the class interface
    """
    interface.Type = dictParamInterface['type']
    interface.pondDDR = dictParamInterface['pondDDR']
    interface.cohesiveLaw = dictParamInterface['law']
    cohesiveLawBuilder.get(interface.cohesiveLaw)(interface,dictParamInterface)

interfaceLoader = {
        'PERFECT' : perfectLoader,
        'CONTACT' : contactLoader,
        'COHESIVE' : cohesiveLoader
        }

cohesiveLawLoader = {
        'Allix' : allixLoader
        }

interfaceSetType = {
        'PERFECT' : perfectBuilder,
        'CONTACT' : contactBuilder,
        'COHESIVE' : cohesiveBuilder
        }

cohesiveLawBuilder = {
        'Allix' : allixBuilder
        }

def loadInterfaceFromXML(xmlFile):
    """
    Load the information about the interfaces described in the xml file
    Input:
        -xmlFile: path to the xml file
    Output:
        return 2 dictionnary, one for default interfaces, one for the others
    """
    global root
    tree = et.parse(xmlFile)
    root = tree.getroot()
    ### Default interfaces
    defaultInterface = interfaceLoader.get(root.find("structure/interface[@id='default']").attrib['type'])("structure/interface[@id='default']")
    ### Other interfaces
    numInterface = [int(root.xpath("structure/interface[not(@id='default')]")[jj].attrib['id'])
                        for jj in range(len(root.xpath("structure/interface[not(@id='default')]")))]   # Number of the interface
    interface = {inter : interfaceLoader.get(root.find("structure/interface[@id='{}']".format(inter)).attrib['type'])("structure/interface[@id='{}']".format(inter)) for inter in numInterface}

    return defaultInterface,interface

def setTypeInterface(interface,dictParamInterface):
    """
    Affect the parameters and type of the interface to the class Interface
    Input:
        -interface: class Interface in construction
        -dictParamInterface : dictionnary with the parameters of the interface,
        come from the class of parameters
    Output:
        Enrich the class interface
    """
    interfaceSetType.get(dictParamInterface['type'])(interface,dictParamInterface)



#==============================================================================
# Class to prepare the interfaces
#==============================================================================
def prepareInterface(subStructuredMesh,param):
    """
    Initialisation and construction of the class
    Input:
        -subStructuredMesh: class containing the mesh of the subdomain
                            and informations of connectivities
        -param: class of parameters
    Return the object Interface
    """
    interface = dict()
    ### Find the nodes with multiplicity
    no = dict()
    listNodes = []
    for kk,pp in enumerate(subStructuredMesh.connecSDInterface[rank]):
        connecSD = subStructuredMesh.connecInterfSD[pp]
        ## Get the list of nodes in the sole of interface kk
#            no[kk] = basics.get_nodes_from_gno(('BNt'+str(connecSD[1-connecSD.index(rank)])+'_'+str(connecSD[connecSD.index(rank)])).ljust(24,' '),subStructuredMesh.Mail,param.dim)
        no[kk] = basics.get_nodes_from_gno(('GN'+str(connecSD[0])+'_'+str(connecSD[1])).ljust(24,' '),subStructuredMesh.Mail,param.dim)
        listNodes += no[kk]
    counterMultiplicity = Counter(listNodes)
    listMultipleNodes = {node:counterMultiplicity[node] for node in counterMultiplicity if counterMultiplicity[node] > 1}
    for kk,pp in enumerate(subStructuredMesh.connecSDInterface[rank]):
        interface[kk] = Interface(pp,subStructuredMesh,listMultipleNodes,param)
    return interface

#==============================================================================
# Class Interface
#==============================================================================
class Interface():
    """
    Class to define and build interfaces
    """
    def __init__(self,kk,subStructuredMesh,listMultipleNodes,param):
        """
        Initialisation and construction of the class
        Input:
            -kk: nuumber of the interface of the associated subdomain
            -subStructuredMesh: class containing the mesh of the subdomain
                                and informations of connectivities
            -listMultipleNodes: dictionnary of the multiple nodes at the
                                interfaces.
            -param: class of parameters
        Return the object Interface
        """
        param.logger.info("     Creation of the interface {}".format(kk))
        self.Type = None
        self.pondDDR = dict()
        self.Operator = dict()                                                 # Dictionnay of python operators
        self.pond = dict()                                                     # Dictionnary of weighting of stiffness of soles
        self.dim = param.dim                                                   # Set the dimension of the problem
        ### Load the connectivity between interfaces and subdomains
        self.connecSD = subStructuredMesh.connecInterfSD[kk]
        ### Load connectivity with material
        self.connecMaterial = subStructuredMesh.connecMaterial['neighbour_{}'.format(self.connecSD[1-self.connecSD.index(rank)])]
        ### Define the type and parameters of the interface
        self.set_type_interface(kk,subStructuredMesh,param)
        ### Information about potential corners
        self.corner = subStructuredMesh.corner
        ### Test if there are multiple nodes for this interface
        self.multipleNodes = {node:listMultipleNodes[node] for node in list(set(basics.get_nodes_from_gno(('GN'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' '),subStructuredMesh.Mail,param.dim)) & set(listMultipleNodes.keys()))}
        ### Create soles instance of the interface
        self.set_soles(kk,subStructuredMesh,param)
        ### Create the double sole instance of the interface
        self.set_double_sole(kk,subStructuredMesh,param)
        ### Initialisation of interface field
        self.init_field(param,subStructuredMesh.connecSDInterface[rank].index(kk))
        if self.Type == "CONTACT" or self.Type == "COHESIVE":
            ### Compute the transfer operator from canonical to normal / tangent basis
            self.Operator['P'] = self.transferNormalTangential()
            self.Operator['Normal'] = sp.block_diag(self.Operator['Normal']).toarray()
            ### Set the search direction into the normal / tangent basis
            self.set_searchDirection_normal_tangent_basis()
        if self.Type == "COHESIVE" :
            ### Compute the mass matrix of the interface
            self.set_cohesive(param)
        ### Definition of macro modes of the interface
        if param.multiscale == 'YES':
            self.compute_macro_basis(kk,param)

    def compute_macro_basis(self,kk,param):
        """
        Compute the macro basis for the interface. Add an attribute in the
        class interface.
        Input:
            -kk: number of the interface of the associated subdomain
            -param: class of parameters
        """
        numNO = self.doubleSole.mesh.NO['N']
        coor = param.mesh.getCoordinates()
        coor_nodes_interface = []
        for ii in [int(numNO[jj].replace('N','')) for jj in range(len(numNO))]:
            coor_nodes_interface.append(np.array([coor[3 * (ii-1)],coor[3 * (ii-1) + 1],coor[3 * (ii-1) + 2]]).reshape((1,3)))
        coor_interface_numpy = np.concatenate(coor_nodes_interface,axis=0)
        self.Macro = dict()
        if param.dim == 2:
            # Modes rigides
            self.Macro['U1'] = np.concatenate((np.ones((len(numNO),1)), np.zeros([len(numNO),1])),axis = 1) # Translation suivant x
            self.Macro['U2'] = np.concatenate((np.zeros([len(numNO),1]),np.ones((len(numNO),1))),axis = 1) # Translation suivant y
            self.Macro['R3'] = np.cross(np.array([0.,0.,1.]),coor_interface_numpy)[:,[0,1]] # Rotation autour de 0z
            # Allongement
            E1 = np.matrix([[1.,0.],[0.,0.]])				# Tenseur de deformation pour un allongement selon 0x
            self.Macro['u1'] = np.array(coor_interface_numpy[:,[0,1]]*E1)							# Champ de deplacement relatif a l'allongement selon 0x
            #self.Macro['u1'] = self.Macro['u1'] / float(self.Macro['u1'].max())
            E2 = np.matrix([[0.,0.],[0.,1.]])				# Tenseur de deformation pour un allongement selon 0y
            self.Macro['u2'] = np.array(coor_interface_numpy[:,[0,1]]*E2)							# Champ de deplacement relatif a l'allongement selon 0y
            #self.Macro['u2'] = self.Macro['u2'] / float(self.Macro['u2'].max())
            # Cisaillement
            E12 = np.matrix([[0.,1.],[1.,0.]])				# Tenseur de deformation pour un cisallement selon (0x,Oy)
            self.Macro['u12'] = np.array(coor_interface_numpy[:,[0,1]]*E12)							# Champ de deplacement relatif au cisallemen selon (0x,Oy)
            #self.Macro['u12'] = self.Macro['u12'] / float(self.Macro['u12'].max())
            self.Macro['OldConnecMacro'] = ['U1','U2','R3','u1','u2','u12']

        if param.dim == 3:
            # Modes rigides
            self.Macro['U1'] = np.concatenate((np.ones((len(numNO),1)), np.zeros([len(numNO),2])),axis = 1) # Translation suivant x
            self.Macro['U2'] = np.concatenate((np.zeros([len(numNO),1]),np.ones((len(numNO),1)), np.zeros([len(numNO),1])),axis = 1) # Translation suivant y
            self.Macro['U3'] = np.concatenate((np.zeros([len(numNO),2]),np.ones((len(numNO),1))),axis = 1) # Translation suivant z
            self.Macro['R1'] = np.cross(np.array([1.,0.,0.]),coor_interface_numpy)				# Rotation autour de 0x
            self.Macro['R2'] = np.cross(np.array([0.,1.,0.]),coor_interface_numpy)				# Rotation autour de 0y
            self.Macro['R3'] = np.cross(np.array([0.,0.,1.]),coor_interface_numpy)				# Rotation autour de 0z
            # Allongement
            E1 = np.matrix([[1.,0.,0.],[0.,0.,0.],[0.,0.,0.]])				# Tenseur de deformation pour un allongement selon 0x
            self.Macro['u1'] = np.array(coor_interface_numpy*E1)							# Champ de deplacement relatif a l'allongement selon 0x
            E2 = np.matrix([[0.,0.,0.],[0.,1.,0.],[0.,0.,0.]])				# Tenseur de deformation pour un allongement selon 0y
            self.Macro['u2'] = np.array(coor_interface_numpy*E2)							# Champ de deplacement relatif a l'allongement selon 0y
            E3 = np.matrix([[0.,0.,0.],[0.,0.,0.],[0.,0.,1.]])				# Tenseur de deformation pour un allongement selon 0z
            self.Macro['u3'] = np.array(coor_interface_numpy*E3)							# Champ de deplacement relatif a l'allongement selon 0z
            # Cisaillement
            E12 = np.matrix([[0.,1.,0.],[1.,0.,0.],[0.,0.,0.]])				# Tenseur de deformation pour un cisallement selon (0x,Oy)
            self.Macro['u12'] = np.array(coor_interface_numpy*E12)							# Champ de deplacement relatif au cisallemen selon (0x,Oy)
            E23 = np.matrix([[0.,0.,0.],[0.,0.,1.],[0.,1.,0.]])				# Tenseur de deformation pour un cisallemen selon (0y,Oz)
            self.Macro['u23'] = np.array(coor_interface_numpy*E23)							# Champ de deplacement relatif au cisallemen selon (0y,Oz)
            E13 = np.matrix([[0.,0.,1.],[0.,0.,0.],[1.,0.,0.]])				# Tenseur de deformation pour un cisallemen selon (0x,Oz)
            self.Macro['u13'] = np.array(coor_interface_numpy*E13)							# Champ de deplacement relatif au cisallement selon (0x,Oz)
            self.Macro['OldConnecMacro'] = ['U1','U2','U3','R1','R2','R3','u1','u2','u3','u12','u23','u13']

        if self.corner[rank][self.connecSD[1-self.connecSD.index(rank)]]:
            self.Macro['OldConnecMacro'] = self.Macro['OldConnecMacro'][:param.dim]
        for key in self.Macro['OldConnecMacro']:
            if max(abs(self.Macro[key].ravel()))!=0.:
                self.Macro[key] = pythonField(self.Macro[key].ravel() / max(abs(self.Macro[key].ravel())),
                                              self.sole[0].mesh.NO['N'])
            else:
                self.Macro[key] = pythonField(self.Macro[key].ravel(),
                                              self.sole[0].mesh.NO['N'])
        ### Filter the potential collinear modes
        ## Concatenate all the macro modes

        macroModesMatrix = np.hstack([np.reshape(self.Macro[key].valField,(self.Macro[key].valField.shape[0],1)) for key in self.Macro['OldConnecMacro'][:param.macroModes]])
        qr = np.linalg.qr(macroModesMatrix)
        diagR = np.diag(qr[1])
        index_to_eliminate = np.arange(diagR.shape[0])[np.where(abs(diagR) < 1e-5)].tolist()
        newModesMatrix = macroModesMatrix[:,list(set(range(macroModesMatrix.shape[1])) - set(index_to_eliminate))]

#        corr = np.corrcoef(macroModesMatrix,rowvar=0)
#        www,vvv = np.linalg.eig(corr)
#        newModesMatrix = macroModesMatrix.dot(vvv[:,np.where(abs(www)>1e-3)[0]])

        self.Macro['ConnecMacro'] = list(range(newModesMatrix.shape[1]))
        for jj in self.Macro['ConnecMacro']:
            self.Macro[jj] = pythonField(newModesMatrix[:,jj],self.sole[0].mesh.NO['N'])


    def compute_weighted_material(self,param):
        """
        Return the Young modulus and Poisson ratio weighted by the attribute of
        the class
        Input:
            -param: class of parameters where the material characteristics are
            stored
        """
        E = dict()
        nu = dict()
        for jj in range(2):
            ### Mean of the Young modulus of the 2 neighboured subdomains
            E[jj] = self.pond['mean'] * param.dt * (param.material['E'][self.connecSD[1-jj]]+param.material['E'][self.connecSD[jj]])*0.5
            ### No weighting on the Poison ratio
            nu[jj] = param.material['nu'][self.connecSD[jj]]
        return E,nu

    def createMatDictSole(self,connecMat,pp,param):
        """
        Create the dictionnary of aster material
        Input:
            -connecMat: python list of connectivity of material
            -pp: number of the sole in the interface
            -param: class of parameters
        Output: return a dictionnary where the keys are the nodes groups and
        the values the aster material
        """
        matDict = dict()
        for ii in connecMat:
            if ii in list(param.material.keys()):
                dict_mat = param.material[ii]
            elif ii == 0:
                dict_mat = param.defaultMaterial[self.connecSD[pp]]
            asterMat = createAsterMaterial(dict_mat,weight=self.pond['mean'])
            matDict[self.sole[pp].mesh.GMA['M{}'.format(ii)]] = asterMat
        return matDict

    def init_field(self,param,kk):
        """
        Make the initialiaztion of the fields of interface. Add an attribute
        Value of the class Interface.
        Input:
            -param: class of parameters
            -kk: number of the interface of the associated subdomain
        """
        if param.resumption and isfile(param.resPrepare+'Resumption/Resumption_{}/resumption_{}'.format(rank,rank)):
            resumption = shelve.open(param.resPrepare+'Resumption/Resumption_{}/resumption_{}'.format(rank,rank))
            self.Value = resumption['local'][kk]
            resumption.close()
        else:
            self.Value = Value(param)
            for tt in range(-1,param.numberTimeStep):						# Boucle sur les pas de temps
                self.Value.W[tt] = dict()
                self.Value.V[tt] = dict()
                self.Value.T[tt] = dict()
                self.Value.Tw[tt] = dict()
                self.Value.F[tt] = dict()
                self.Value.Wc[tt] = dict()
                self.Value.Vc[tt] = dict()
                self.Value.Tc[tt] = dict()
                self.Value.Twc[tt] = dict()
                self.Value.Fc[tt] = dict()
                for jj in range(0,2):
                    size_interface = len(self.sole[jj].mesh.NO['N'])
                    no = self.sole[jj].mesh.NO['N']
                    self.Value.W[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.V[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.T[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Tw[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.F[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Wc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Vc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Tc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Twc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)
                    self.Value.Fc[tt][jj] = pythonField(np.zeros(size_interface * param.dim),no)

    def set_double_sole(self,kk,subStructuredMesh,param):
        """
        Modify the class Interface to build the double sole instance of the
        interface.
        Input: -kk: number of the interface of the associated subdomain
               -subStructuredMesh: class containing the mesh of the subdomain
                                  and informations of connectivities
               -param: class of parameters
        """
        self.doubleSole = struct()
        self.doubleSole.model.dim = param.dim
        ### Load the groups of nodes and elements
        ## GMA
        gma = dict()
        gma['S'] = ('DS'+str(self.connecSD[self.connecSD.index(rank)])+'_'+str(self.connecSD[1-self.connecSD.index(rank)])).ljust(24,' ')
        gma['S0'] = ('S'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')      # Group of elements related to the 1st sole
        gma['S1'] = ('S'+str(self.connecSD[1])+'_'+str(self.connecSD[0])).ljust(24,' ')      # Group of elements related to the 2nd sole
        self.doubleSole.mesh.set_gma(gma)
        ## GNO
        gno = dict()
        gno['GN'] = ('GN'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')
        gno['GNt'] = ('DS'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')
        gno['Bord1'] = ('BN'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')
        gno['Bord2'] = ('BN'+str(self.connecSD[1])+'_'+str(self.connecSD[0])).ljust(24,' ')
        self.doubleSole.mesh.set_gno(gno)
        ## NO Il faut attendre une nouvelle implémentation dans asterXX pour obtenir la liste des noeuds appartenant à un groupe
#        self.doubleSole.mesh.NO['N'] = ['N'+str(ii) for ii in sorted(map(int, subStructuredMesh.Mail.sdj.GROUPENO.get()[self.doubleSole.mesh.GNO['GN']]))]	# Importation des numeros des noeuds d'interface
        no = dict()
        no['N'] = basics.get_nodes_from_gno(self.doubleSole.mesh.GNO['GN'],subStructuredMesh.Mail,param.dim)
        no['N1'] = basics.get_nodes_from_gno(self.doubleSole.mesh.GNO['Bord1'],subStructuredMesh.Mail,param.dim)
        no['N2'] = basics.get_nodes_from_gno(self.doubleSole.mesh.GNO['Bord2'],subStructuredMesh.Mail,param.dim)
        no['Nt'] = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(no['N']+no['N1']+no['N2']))])))]
        self.doubleSole.mesh.set_no(no)
        ### Assign the model of the double sole
        self.doubleSole.model.set_model((self.doubleSole.mesh.GMA['S0'],self.doubleSole.mesh.GMA['S1']),subStructuredMesh.Mail,param)
        ### Assign the material
        matDict = dict()
        matDict.update(self.sole[0].model.matDict)
        matDict.update(self.sole[1].model.matDict)
        self.doubleSole.model.assigned_material = assign_material_dict(matDict,subStructuredMesh.Mail)
        ### Assign Dirichlet boundary conditions
        self.doubleSole.model.set_blocked_dirichlet_boundary_conditions([self.doubleSole.mesh.GNO['Bord1'],self.doubleSole.mesh.GNO['Bord2']],self.dim)
        ### Compute the assembly stiffness matrix
        self.doubleSole.model.set_assemblyStiffnessMatrix()
        self.doubleSole.model.set_assemblyStiffnessMatrix(block_overlap=False)
        matPython = self.doubleSole.model.stiffness_noOverlap.EXTR_MATR(sparse=True)
        self.doubleSole.model.stiffness_noOverlap_python = sp.coo_matrix((matPython[0],(matPython[1],matPython[2])),shape=(max(matPython[1])+1,max(matPython[2])+1)).tocsr()

        ### Factorization of the stiffness matrix
        self.doubleSole.model.set_factorization(param.solver)
        # Extract the sparse matrix with the Dirichlet neumann_conditions
        matPython = self.doubleSole.model.stiffness.EXTR_MATR(sparse=True)
        spcoo = sp.coo_matrix((matPython[0],(matPython[1],matPython[2])),shape=(param.dim * len(self.doubleSole.mesh.NO['Nt']),param.dim * len(self.doubleSole.mesh.NO['Nt'])))
#        if self.multipleNodes:
#            npsole1 = self.sole[0].model.stiffness_python.toarray()
#            npsole2 = self.sole[1].model.stiffness_python.toarray()
#            nparray = spcoo.toarray()
#            dict_nodes = {node:pos for pos,node in enumerate(self.sole[0].mesh.NO['N'])}
#            pos_in_nodes = [dict_nodes[node] for node in self.multipleNodes]
#            pos_in_dofs = [param.dim * ii + pp for ii in pos_in_nodes for pp in range(param.dim)]
#            nparray[pos_in_dofs,pos_in_dofs] = npsole1[pos_in_dofs,pos_in_dofs] + npsole2[pos_in_dofs,pos_in_dofs]
#            spcoo = sp.coo_matrix(nparray)

        self.doubleSole.model.stiffness_python = spcoo.tocsr()
        self.doubleSole.model.stiffness_python_factorized = sp.linalg.splu(self.doubleSole.model.stiffness_python)
        ### Compute stiffness matrix for full Dirichlet boundary condition on the interface
        ## Dirichlet boundary conditions on the two side of the soles
        self.doubleSole.model.boundary_conditions['bloc_full'] = basics.block_gno(self.doubleSole.model.MODELE,[self.doubleSole.mesh.GNO['Bord1'],self.doubleSole.mesh.GNO['Bord2']],self.dim)
        ## Assembly stiffness matrix
        matele = basics.compute_matele(self.doubleSole.model.MODELE,self.doubleSole.model.assigned_material)
        nume_ddl = basics.compute_nume_ddl(matele)
        self.doubleSole.model.stiffness_dirichlet_full = basics.compute_stiffness(matele,nume_ddl,[self.doubleSole.model.boundary_conditions['bloc_full']])
        param.solver.matrixFactorization(self.doubleSole.model.stiffness_dirichlet_full)
        self.doubleSole.model.dirichlet_full_right_hand_side = basics.compute_dirichlet_right_hand_side(nume_ddl,[self.doubleSole.model.boundary_conditions['bloc_full']])

    def set_searchDirection_normal_tangent_basis(self):
        """
        Compute the nodal search direction into the normal / tangent basis
        """

        # Search direction in the normal / tang basis
        for jj in range(2):
            self.sole[jj].model.dirRecherche = pythonField((self.Operator['P'].T.dot(np.diag(self.sole[jj].model.dirRecherche).dot(self.Operator['P']))).diagonal(),self.sole[jj].mesh.NO['N'])
        if self.dim == 3:
            n = [self.dim * ii for ii in range(len(self.sole[0].mesh.NO['N']))]
            t = np.sort(np.concatenate([np.array(n)+dd for dd in range(1,self.dim)])).tolist()
            t1 = np.array(t)[list(range(0,2*len(self.sole[0].mesh.NO['N']),2))].tolist()
            t2 = np.array(t)[[ii+1 for ii in range(0,2*len(self.sole[0].mesh.NO['N']),2)]].tolist()
            a = (self.sole[0].model.dirRecherche.valField[t1] + self.sole[0].model.dirRecherche.valField[t2]) / 2
            an = (self.sole[0].model.dirRecherche.valField[n] + self.sole[0].model.dirRecherche.valField[n])/2
            self.sole[0].model.dirRecherche = pythonField(np.column_stack((an,a,a)).ravel(),self.sole[jj].mesh.NO['N'])
            self.sole[0].model.dirRecherche.valField[t1] = a
            self.sole[0].model.dirRecherche.valField[t2] = a
            b = (self.sole[1].model.dirRecherche.valField[t1] + self.sole[1].model.dirRecherche.valField[t2]) / 2
            bn = (self.sole[1].model.dirRecherche.valField[n] + self.sole[1].model.dirRecherche.valField[n])/2
            self.sole[1].model.dirRecherche = pythonField(np.column_stack((bn,b,b)).ravel(),self.sole[jj].mesh.NO['N'])
            self.sole[1].model.dirRecherche.valField[t1] = b
            self.sole[1].model.dirRecherche.valField[t2] = b

    def set_soles(self,kk,subStructuredMesh,param):
        """
        Modify the class Interface to build the soles instances of the
        interface.
        Input: -kk: number of the interface of the associated subdomain
               -subStructuredMesh: class containing the mesh of the subdomain
                                  and informations of connectivities
               -param: class of parameters
        """
        self.sole = dict()
        for jj in range(2):
            self.sole[jj] = struct()
            self.sole[jj].model.dim = param.dim
            ### Load the groups of nodes and elements
            ## GMA
            gma = dict()
            gma['S'] = ('S'+str(self.connecSD[jj])+'_'+str(self.connecSD[1-jj])).ljust(24,' ')
            gma['GM'] = ('GM'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')		# Importation des MA d'interface pour la Semelle globale
            for mm in self.connecMaterial[jj]:
                gma['M{}'.format(mm)] = ('M{}_{}_{}'.format(mm,self.connecSD[jj],self.connecSD[1-jj])).ljust(24,' ')
            self.sole[jj].mesh.set_gma(gma)
            ## GNO
            gno = dict()
            gno['BNt'] = ('BNt'+str(self.connecSD[jj])+'_'+str(self.connecSD[1-jj])).ljust(24,' ')
            gno['GN'] = ('GN'+str(self.connecSD[jj])+'_'+str(self.connecSD[1-jj])).ljust(24,' ')
            gno['BN'] = ('BN'+str(self.connecSD[jj])+'_'+str(self.connecSD[1-jj])).ljust(24,' ')
            self.sole[jj].mesh.set_gno(gno)
            ## NO Il faut attendre une nouvelle implémentation dans asterXX pour obtenir la liste des noeuds appartenant à un groupe
            no = dict()
            no['Nt'] = basics.get_nodes_from_gno(self.sole[jj].mesh.GNO['BNt'],subStructuredMesh.Mail,param.dim)
            no['N'] = basics.get_nodes_from_gno(self.sole[jj].mesh.GNO['GN'],subStructuredMesh.Mail,param.dim)
            self.sole[jj].mesh.set_no(no)
            ## Importation of the boundingBox
            self.sole[jj].model.boundingBox['sole'] = np.array(subStructuredMesh.boundingBoxSole[self.connecSD[jj]][self.connecSD[jj-1]])
            self.sole[jj].model.boundingBox['SD'] = np.array(subStructuredMesh.boundingBox[self.connecSD[jj]])
            ### Assign the model of the sole
            self.sole[jj].model.set_model(self.sole[jj].mesh.GMA['S'],subStructuredMesh.Mail,param)
            ### Implémenter le calcul de la normale aux interfaces. Cependant besoin d'un équivalent à EXTR_COMP
            if not(self.corner[rank][self.connecSD[1-self.connecSD.index(rank)]]):
                self.Operator['Normal'] = basics.compute_normal_gma(self.sole[jj].model.MODELE,self.sole[jj].mesh.GMA['GM'])
        self.set_weighting_stiffness()
#        [E,nu] = self.compute_weighted_material(param)
        for jj in range(2):
            ### Assign the material
            self.sole[jj].model.matDict = self.createMatDictSole(self.connecMaterial[jj],jj,param)
            self.sole[jj].model.assigned_material = assign_material_dict(self.sole[jj].model.matDict,subStructuredMesh.Mail)
            ### Assign Dirichlet boundary conditions
            self.sole[jj].model.set_blocked_dirichlet_boundary_conditions(self.sole[jj].mesh.GNO['BN'],self.dim)
            ### Compute the assembly stiffness matrix
            self.sole[jj].model.set_assemblyStiffnessMatrix()
            # Extract the sparse matrix with the Dirichlet neumann_conditions
            matPython = self.sole[jj].model.stiffness.EXTR_MATR(sparse=True)
            spcoo = sp.coo_matrix((matPython[0],(matPython[1],matPython[2])),shape=(param.dim * len(self.sole[jj].mesh.NO['Nt']),param.dim * len(self.sole[jj].mesh.NO['Nt'])))
#            if self.multipleNodes:
#                nparray = spcoo.toarray()
#                dict_nodes = {node:pos for pos,node in enumerate(self.sole[jj].mesh.NO['N'])}
#                pos_in_nodes = [dict_nodes[node] for node in self.multipleNodes.keys()]
#                pos_in_dofs = [param.dim * ii + pp for ii in pos_in_nodes for pp in range(param.dim)]
#                nparray[pos_in_dofs,pos_in_dofs] = nparray[pos_in_dofs,pos_in_dofs] / np.array(self.multipleNodes.values())
#                spcoo = sp.coo_matrix(nparray)
            self.sole[jj].model.stiffness_python = spcoo.tocsr()
            self.sole[jj].model.set_assemblyStiffnessMatrix(block_overlap=False)
            # Extract the sparse matrix without Dirichlet conditions
            matPython = self.sole[jj].model.stiffness_noOverlap.EXTR_MATR(sparse=True)
            spcoo = sp.coo_matrix((matPython[0],(matPython[1],matPython[2])),shape=(param.dim * len(self.sole[jj].mesh.NO['Nt']),param.dim * len(self.sole[jj].mesh.NO['Nt'])))
#            if self.multipleNodes:
#                nparray = spcoo.toarray()
#                dict_nodes = {node:pos for pos,node in enumerate(self.sole[jj].mesh.NO['N'])}
#                pos_in_nodes = [dict_nodes[node] for node in self.multipleNodes]
#                pos_in_dofs = [param.dim * ii + pp for ii in pos_in_nodes for pp in range(param.dim)]
#                nparray[pos_in_dofs,pos_in_dofs] = nparray[pos_in_dofs,pos_in_dofs] / np.array(self.multipleNodes.values())
#                spcoo = sp.coo_matrix(nparray)
            self.sole[jj].model.stiffness_noOverlap_python = spcoo.tocsr()
            ### Extraction of the diagonal if contact case
            if self.Type == 'CONTACT' or self.Type == 'COHESIVE':
                matAsse = self.sole[jj].model.stiffness.EXTR_MATR().diagonal()		# Diagonale de l'opérateur de rigidité des semelles en vue de la direction de recherche
                pos = [param.dim*self.sole[jj].mesh.NO['Nt'].index(e) for e in self.sole[jj].mesh.NO['N']]
                for i in range(len(pos)-1,-1,-1):
                    pos.insert(i+1,pos[i]+1)
                    if param.dim == 3:
                        pos.insert(i+2,pos[i]+2)
                self.sole[jj].model.dirRecherche = self.pondDDR['up'] * matAsse[pos]
            ### Factorization of the stiffness matrix
            self.sole[jj].model.set_factorization(param.solver)
            # Extract the sparse matrix with the Dirichlet neumann_conditions
            matPython = self.sole[jj].model.stiffness.EXTR_MATR(sparse=True)
            spcoo = sp.coo_matrix((matPython[0],(matPython[1],matPython[2])),shape=(param.dim * len(self.sole[jj].mesh.NO['Nt']),param.dim * len(self.sole[jj].mesh.NO['Nt'])))
#            if self.multipleNodes:
#                nparray = spcoo.toarray()
#                dict_nodes = {node:pos for pos,node in enumerate(self.sole[jj].mesh.NO['N'])}
#                pos_in_nodes = [dict_nodes[node] for node in self.multipleNodes]
#                pos_in_dofs = [param.dim * ii + pp for ii in pos_in_nodes for pp in range(param.dim)]
#                nparray[pos_in_dofs,pos_in_dofs] = nparray[pos_in_dofs,pos_in_dofs] / np.array(self.multipleNodes.values())
#                spcoo = sp.coo_matrix(nparray)
            self.sole[jj].model.stiffness_python = spcoo.tocsr()
            self.sole[jj].model.stiffness_python_lu = sp.linalg.splu(self.sole[jj].model.stiffness_python)
            ### Compute stiffness matrix for full Dirichlet boundary condition on the interface
            ## Dirichlet boundary conditions on the two side of the soles
            self.sole[jj].model.boundary_conditions['bloc_full'] = basics.block_gno(self.sole[jj].model.MODELE,[self.sole[jj].mesh.GNO['BN'],self.sole[jj].mesh.GNO['GN']],self.dim)
            ## Assembly stiffness matrix
            matele = basics.compute_matele(self.sole[jj].model.MODELE,self.sole[jj].model.assigned_material)
            nume_ddl = basics.compute_nume_ddl(matele)
            self.sole[jj].model.stiffness_dirichlet_full = basics.compute_stiffness(matele,nume_ddl,[self.sole[jj].model.boundary_conditions['bloc_full']])
            param.solver.matrixFactorization(self.sole[jj].model.stiffness_dirichlet_full)
            self.sole[jj].model.dirichlet_full_right_hand_side = basics.compute_dirichlet_right_hand_side(nume_ddl,[self.sole[jj].model.boundary_conditions['bloc_full']])


    def set_type_interface(self,kk,subStructuredMesh,param):
        """
        Modifiy the class Interface to define the type of the interface
        according to the parameters and the substructured mesh
        Input: -kk: number of the interface of the associated subdomain
               -subStructuredMesh: class containing the mesh of the subdomain
                                  and informations of connectivities
               -param: class of parameters
        """
        if param.decomposer != 'MANUAL':
            if subStructuredMesh.connecInterfOldInterf[kk]:
                if not (subStructuredMesh.connecInterfOldInterf[kk] in list(param.interface.keys())):
                    setTypeInterface(self,param.defaultInterface)
                else:
                    setTypeInterface(self,param.interface[subStructuredMesh.connecInterfOldInterf[kk]])
            else:
                setTypeInterface(self,param.defaultInterface)
        else:
            if not (kk in list(param.interface.keys())):
                setTypeInterface(self,param.defaultInterface)
            else:
                setTypeInterface(self,param.interface[kk])

    def set_weighting_stiffness(self):
        """
        Modify the attribute of weighting for the stiffness of soles
        """
        if self.pondDDR['down']:
            for jj in range(2):
                self.pond[jj] = self.pondDDR['down']
        else:
            if not(self.corner[rank][self.connecSD[1-self.connecSD.index(rank)]]):
                meanNormal = np.mean(np.reshape(self.Operator['Normal'],(int(self.Operator['Normal'].shape[0] / self.dim),self.dim)).T,axis=1)
            else:
                meanNormal = 0.
            for jj in range(2):
                if np.linalg.norm(meanNormal) < 10**(-5):
                    self.pond[jj] = np.linalg.norm(self.sole[jj].model.boundingBox['sole']) / np.linalg.norm(self.sole[jj].model.boundingBox['SD'])
                else:
                    self.pond[jj] = abs(self.sole[jj].model.boundingBox['sole'][self.sole[jj].model.boundingBox['sole']!=0].dot(meanNormal) / self.sole[jj].model.boundingBox['SD'][self.sole[jj].model.boundingBox['sole']!=0].dot(meanNormal))
        self.pond['mean'] = (self.pond[0] + self.pond[1]) / 2.

    def transferNormalTangential(self):
        """
        Compute the transfert matrix from the canonical basis to the normal /
        tangential basis
        Input:
            -param: class of parameters
        Output:
            -P_interf: numpy array
        """
        # vecteur x
        x = np.zeros((self.dim))
        x[0] = 1
        # vecteur x
        y = np.zeros((self.dim))
        y[1] = 1
        P = dict()
        for ii in range(len(self.sole[0].mesh.NO['N'])):
            normale = self.Operator['Normal'][self.dim*ii:(self.dim*ii+self.dim)]
            if self.dim == 3:
                t1 = np.cross(x,normale)
                if t1[0] == 0. and t1[1] == 0. and t1[2] == 0.:
                    t1 = y
                else:
                    t1 = t1 / np.linalg.norm(np.cross(x,normale))
                t2 =  np.cross(normale,t1)
                P[ii] = np.concatenate((normale,t1,t2),axis = 0)
            elif self.dim == 2:
                t1 = np.array([- normale[1],normale[0]])
                P[ii] = np.concatenate((normale,t1),axis = 0)
            P[ii] = np.reshape(P[ii],(self.dim,self.dim)).T

        P_interf = sp.block_diag([P[ii] for ii in range(0,len(self.sole[0].mesh.NO['N']))]).toarray()
        return P_interf


    def set_cohesive(self,param):
        '''
        Modifiy the class Interface to define the mass matrix of the interface
        Input:
            -param: class of parameters
        '''
        ### In case of a 3D problem
        if param.dim == 3:
            ### Load the group of elements
            GMA = ('GM'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')
            ### Assign the material of the interface
            CHMAT=basics.assign_material(GMA,param.mesh,self.sole[0].model.matDict[list(self.sole[0].model.matDict.keys())[0]])
            ### Assign the C_PLAN modele of the interface
            POVOL = AFFE_MODELE(MAILLAGE = param.mesh,
                                AFFE = _F(GROUP_MA = GMA,
                                          MODELISATION = 'C_PLAN',
                                          PHENOMENE = param.phenomenon))
            ### Compute the mass matrix of the interface
            study = code_aster.StudyDescription(POVOL,CHMAT)
            dProblem = code_aster.DiscreteProblem(study)
            matelem = dProblem.computeMechanicalMassMatrix()
            nume_ddl = basics.compute_nume_ddl(matelem)
            M = code_aster.AssemblyMatrixDouble()
            M.appendElementaryMatrix(matelem)
            M.setDOFNumbering(nume_ddl)
            M.build()
            M = M.EXTR_MATR()
            ### Extract the diagonal of the matrix
            M = np.diag(M)
            Mass = []
            ### Add the third componant for each node
            for ii in range (0,len(M),2):
                Mass += [M[ii],M[ii],M[ii]]
            Mass = np.diag(Mass)
            ### Divide with the surface of the interface
            Mass = 1./self.S * Mass
            self.mass = Mass

        ### In case of a 2D problem
        elif param.dim == 2:
            ### Load the group of elements
            GMA = ('GM'+str(self.connecSD[0])+'_'+str(self.connecSD[1])).ljust(24,' ')
            ### Assign the material of the interface
            CHMAT=basics.assign_material(GMA,param.mesh,self.sole[0].model.matDict[list(self.sole[0].model.matDict.keys())[0]])
            ### Assign the DIS_T modele of the interface
            POVOL = AFFE_MODELE(MAILLAGE = param.mesh,
                                AFFE = _F(GROUP_MA = GMA,
                                          MODELISATION = 'DIS_T',
                                          PHENOMENE = param.phenomenon))
            ### Define complementary characteristics
            CARE=AFFE_CARA_ELEM(MODELE=POVOL,
                     DISCRET=(_F(CARA='M_T_D_L',
                                 GROUP_MA = GMA,
                                 VALE=1.,),),)
#            ASSEMBLAGE(MODELE=POVOL,
#                       CARA_ELEM=CARE,
#                       NUME_DDL=CO("NU"),
#                       MATR_ASSE=(_F(  MATRICE = CO("M"),  OPTION = 'MASS_MECA'),)
#                       );
            matelem = CALC_MATR_ELEM(OPTION = 'MASS_MECA',
                                 MODELE = POVOL,
                                 CHAM_MATER = CHMAT,
                                 CARA_ELEM = CARE)
            nume_ddl = NUME_DDL(MODELE = POVOL)
            M = ASSE_MATRICE(MATR_ELEM = matelem,
                             NUME_DDL = nume_ddl)

            Mass = M.EXTR_MATR()
            ### Supression of the third componant for each node
            L = []
            for ii in range (0,np.shape(Mass)[0]-2,3):
                L+=[ii,ii+1]
            Mass = sp.diags(Mass[L,L])
#            pdb.set_trace()
            self.mass = Mass

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 15:22:38 2017

@author: oumaziz
"""
from latindd4py.aster_functions import basics
import copy

class mesh:
    """
    Class regrouping information about groups of nodes and elements
    """
    def __init__(self):
        self.GNO = dict()                                                      # Dictionnary of groups of nodes
        self.GMA = dict()                                                      # Dictionnary of groups of elements
        self.NO = dict()                                                       # Dictionnary of list of nodes
    def set_gno(self,gno):
        """
        Load the groups of nodes and modifiy the attribute gno of the class
        mesh.
        Input: -gno: dictionnary with keys and name of groups in the mesh
        """
        self.GNO = gno.copy()
    def set_gma(self,gma):
        """
        Load the groups of elements and modifiy the attribute gno of the class
        mesh.
        Input: -gno: dictionnary with keys and name of groups in the mesh
        """
        self.GMA = gma.copy()
    def set_no(self,no):
        """
        Load the list of nodes and modifiy the attribute no of the class
        mesh.
        Input: -no: dictionnary with keys and name of groups in the mesh
        """
        self.NO = no.copy()

class model:
    """
    Class regrouping information about the model:
        -model
        -material
        -assigned material
        -nume_ddl
        -boundary conditions...
    """
    def __init__(self):
        self.dim = None                                                        # Dimension of the problem
        self.MODELE = None                                                     # Aster model
        self.mat = None                                                        # Aster material
        self.assigned_material = None                                          # Assigned material
        self.stiffness = None                                                  # Stiffness matrix
        self.nume_ddl = None                                                   # Aster numerotation
        self.boundary_conditions = {'imposed_dirichlet':None}                  # Dictionnary of boundary conditions
        self.factorized_stiffness = None                                       # Factorized stiffness operator
        self.boundingBox = dict()                                              # Dictionnary of bounding box
        self.Operator = dict()                                                 # Dictionnay of python operator
        self.search_direction = None                                           # Search direction  at numpy array format

    def extract_elim_stiffness(self,stiffness,nodesList,nodesTot):
        """
        Extract the part of the matrix corresponding to Dirichlet dofs before
        elimination.
        Input:
            -nodesList: list of nodes eliminated
            -nodesTot: list of nodes of the structure
        Output:
            sparse matrix to apply to Dirichlet conditions in the right hand-side
        """
        stiffness_elim =  basics.extract_stiffness_on_nodes(stiffness,nodesList,nodesTot)
        return stiffness_elim

    def set_assemblyStiffnessMatrix(self,block_overlap = True,imposed_dirichlet = True):
        """
        Compute the assembled stiffness matrix. Modify the attributes nume_ddl
        and stiffness
        Input:
            -block_overlap: True or False, to know if the edge of the overlaps
            have to be block before factorization
        """

        dirichlet_conditions = []
        if self.boundary_conditions['imposed_dirichlet']:
            # dirichlet_conditions.append(self.boundary_conditions['imposed_dirichlet'])
            dirichlet_conditions = [self.boundary_conditions['imposed_dirichlet'][cond] for cond in sorted(self.boundary_conditions['imposed_dirichlet'].keys())]
        if block_overlap:
            dirichlet_conditions.append(self.boundary_conditions['BlocS'])
            matele = basics.compute_matele(self.MODELE,self.assigned_material)
            self.nume_ddl = basics.compute_nume_ddl(matele)
            self.dirichlet_right_hand_side = basics.compute_dirichlet_right_hand_side(self.nume_ddl,dirichlet_conditions)
            self.stiffness = basics.compute_stiffness(matele,self.nume_ddl,dirichlet_conditions)
        else:
            matele = basics.compute_matele(self.MODELE,self.assigned_material)
            self.nume_ddl_noOverlap = basics.compute_nume_ddl(matele)
            if dirichlet_conditions and imposed_dirichlet:
                self.dirichlet_right_hand_sided_side_noOverlap = basics.compute_dirichlet_right_hand_side(self.nume_ddl,dirichlet_conditions)
            self.stiffness_noOverlap = basics.compute_stiffness(matele,self.nume_ddl,dirichlet_conditions)

    def set_blocked_dirichlet_boundary_conditions(self,gno_dirichlet,dim):
        """
        Compute the zeros Dirichlet boundary conditions on a group of nodes
        Input:
            -gno_dirichlet: name of the group of nodes on which the boundary
            condition is applied
            -param: class of parameters
        """
        self.boundary_conditions['BlocS'] = basics.block_gno(self.MODELE,gno_dirichlet,dim)

    def set_factorization(self,solver):
        """
        Compute the facorization of the attribute stiffness
        Input:
            -solver: aster solver to factorize the stiffness operators
        """
        basics.factorize(self.stiffness,solver)

    def set_imposed_dof(self,list_gno,neighbourDiri,param):
        """
        Compute the imposed Dirichlet boundary conditions on the subdomain and
        modify the dictionnary boundary_conditions of the class model
        Input:
            -list_gno: list of the node groups of the boundary conditions
            -neighbourDiri: list of the Dirichlet conditions attached to the
            subdomain
            -param: class of parameters
        """
        valueBC = dict()
        self.boundary_conditions['imposed_dirichlet'] = dict()
        for tt in range(param.numberTimeStep):
            valueBC[tt] =  dict()
            for ii in range(len(list_gno)):
                valueBC[tt][ii] = dict()
                for pp in list(param.boundaryDirichlet[neighbourDiri[ii]].keys()):
                    if pp != 'evol':
                        valueBC[tt][ii][pp] = param.boundaryDirichlet[neighbourDiri[ii]]['evol'][tt]*param.boundaryDirichlet[neighbourDiri[ii]][pp]
            self.boundary_conditions['imposed_dirichlet'][tt] = basics.imposed_dof_gno(self.MODELE,list_gno,valueBC[tt])

    def set_material(self,E,nu,gma,meshAster,param):
        """
        Compute the model and modify the attribute mat and assigned_material
        of the class
        Input:
            -E: Young modulus
            -nu: Poisson ration
            -gma: group of elements which constitutes the base of the model
            -meshAster: mesh aster
            -param: class of parameters
        """
        ### Define the material
        self.mat = basics.define_material(E,nu)
        ### Assign the material
        self.assigned_material = basics.assign_material(gma,meshAster,self.mat)

    def set_material_dict(self,mat_dict,meshAster,param):
        """
        Compute the model and modify the attribute mat and assigned_material
        of the class
        Input:
            -mat_dict: dictionnary for which keys are the name of groups of
        elements and the values are list of type [young,poisson]
            -meshAster: mesh aster
            -param: class of parameters
        """
        mat = dict()
        for group in list(mat_dict.keys()):
            ### Define the material
            mat[group] = basics.define_material(mat_dict[group][0],mat_dict[group][1])
        ### Assign the material
        self.assigned_material = basics.assign_material_dict(mat,meshAster)

    def set_model(self,gma,meshAster,param):
        """
        Compute the model and modify the attribute MODELE of the class
        Input:
            -gma: group of elements which constitutes the base of the model
            -meshAster: mesh aster
            -param: class of parameters
        """
        ### Assign the model
        self.MODELE = basics.assign_model(gma,meshAster,param)


class struct(mesh,model):
    """
    Class of a mechanical structure. It regroups an attribute mesh and an
    attribute model.
    """
    def __init__(self):
        mesh.__init__(self)
        model.__init__(self)
        self.mesh = mesh()
        self.model = model()
    def set_mesh(self,mesh):
        """
        Set the attribute mesh
        """
        self.mesh = mesh
    def set_model(self,model):
        """
        Set the attribute model
        """
        self.model = model

class Value:
    def __init__(self,param=False):
        self.W = dict()								# Champ de deplacement a l'etape lineaire
        self.V = dict()								# Champ de vitesse a l'etape lineaire
        self.T = dict()								# Champ d'effort intermediaire a l'etape lineaire
        self.Tw = dict()								# Champ d'effort intermediaire a deplacement impose
        self.F = dict()								# Champ d'effort a l'etape lineaire
        self.Wc = dict()								# Champ de deplacement a l'etape locale
        self.Vc = dict()								# Champ de vitesse a l'etape locale
        self.Tc = dict()								# Champ d'effort intermediaire a l'etape locale
        self.Twc = dict()
        self.Fc = dict()								# Champ d'effort a l'etape locale
        self.Wm = dict()								# Champ de deplacement micro
        self.Tm = dict()								# Champ d'effrot intermediaire micro
        self.Cal = dict()								# Calcul a l'etape lineaire
        if param:
            self.listTimeSteps = [param.listTimeSteps[0]-1]+param.listTimeSteps
    def init(self,tt):
        self.W[tt] = dict()								# Champ de deplacement a l'etape lineaire
        self.V[tt] = dict()								# Champ de vitesse a l'etape lineaire
        self.T[tt] = dict()								# Champ d'effort intermediaire a l'etape lineaire
        self.Tw[tt] = dict()							# Champ d'effort intermediaire a deplacement impose
        self.F[tt] = dict()								# Champ d'effort a l'etape lineaire
        self.Wc[tt] = dict()							# Champ de deplacement a l'etape locale
        self.Vc[tt] = dict()							# Champ de vitesse a l'etape locale
        self.Tc[tt] = dict()							# Champ d'effort intermediaire a l'etape locale
        self.Twc[tt] = dict()
        self.Fc[tt] = dict()							# Champ d'effort a l'etape locale
        self.Wm[tt] = dict()							# Champ de deplacement micro
        self.Tm[tt] = dict()							# Champ d'effrot intermediaire micro

    def copy(self):
        """
        Return a copy of the class
        Output:
            -newValue: classe Value
        """
        newValue = Value()
        newValue.W = copy.deepcopy(self.W)
        newValue.V = copy.deepcopy(self.V)
        newValue.T = copy.deepcopy(self.T)
        newValue.Tw = copy.deepcopy(self.Tw)
        newValue.F = copy.deepcopy(self.F)
        newValue.Wc = copy.deepcopy(self.Wc)
        newValue.Vc = copy.deepcopy(self.Vc)
        newValue.Tc = copy.deepcopy(self.Tc)
        newValue.Twc = copy.deepcopy(self.Twc)
        newValue.Fc = copy.deepcopy(self.Fc)
        newValue.Wm = copy.deepcopy(self.Wm)
        newValue.Tm = copy.deepcopy(self.Tm)
        newValue.Cal = copy.deepcopy(self.Cal)
        newValue.listTimeSteps = self.listTimeSteps
        return newValue

    def __add__(self,toto):
        newValue = self.copy()
#        newValue.listTimeSteps = self.listTimeSteps
#        for tt in self.W.keys():
        for tt in self.listTimeSteps:
            newValue.init(tt)
            for jj in self.W[tt]:
                newValue.W[tt][jj] = self.W[tt][jj] + toto.W[tt][jj]
            for jj in self.F[tt]:
                newValue.V[tt][jj] = self.V[tt][jj] + toto.V[tt][jj]
                newValue.T[tt][jj] = self.T[tt][jj] + toto.T[tt][jj]
                newValue.F[tt][jj] = self.F[tt][jj] + toto.F[tt][jj]
            for jj in self.Tw[tt]:
                newValue.Tw[tt][jj] = self.Tw[tt][jj] + toto.Tw[tt][jj]
#                newValue.Twc[tt][jj] = self.Twc[tt][jj] + toto.Twc[tt][jj]
#            for jj in self.Wc[tt]:
#                newValue.Wc[tt][jj] = self.Wc[tt][jj] + toto.Wc[tt][jj]
#                newValue.Vc[tt][jj] = self.Vc[tt][jj] + toto.Vc[tt][jj]
#                newValue.Tc[tt][jj] = self.Tc[tt][jj] + toto.Tc[tt][jj]
#                newValue.Fc[tt][jj] = self.Fc[tt][jj] + toto.Fc[tt][jj]

        return newValue

    def __neg__(self):
        newValue = self.copy()
#        newValue.listTimeSteps = self.listTimeSteps
#        for tt in self.W.keys():
        for tt in self.listTimeSteps:
            newValue.init(tt)
            for jj in self.W[tt]:
                newValue.W[tt][jj] = - self.W[tt][jj]
            for jj in self.F[tt]:
                newValue.V[tt][jj] = - self.V[tt][jj]
                newValue.T[tt][jj] = - self.T[tt][jj]
                newValue.F[tt][jj] = - self.F[tt][jj]
            for jj in self.Tw[tt]:
                newValue.Tw[tt][jj] = - self.Tw[tt][jj]
#                newValue.Twc[tt][jj] = - self.Twc[tt][jj]
#            for jj in self.Wc[tt]:
#                newValue.Wc[tt][jj] = - self.Wc[tt][jj]
#                newValue.Vc[tt][jj] = - self.Vc[tt][jj]
#                newValue.Tc[tt][jj] = - self.Tc[tt][jj]
#                newValue.Fc[tt][jj] = - self.Fc[tt][jj]
        return newValue


    def __sub__(self,toto):
        newValue = self.copy()
#        newValue.listTimeSteps = self.listTimeSteps
#        for tt in self.W.keys():
        for tt in self.listTimeSteps:
            newValue.init(tt)
            for jj in self.W[tt]:
                newValue.W[tt][jj] = self.W[tt][jj] - toto.W[tt][jj]
            for jj in self.F[tt]:
                newValue.V[tt][jj] = self.V[tt][jj] - toto.V[tt][jj]
                newValue.T[tt][jj] = self.T[tt][jj] - toto.T[tt][jj]
                newValue.F[tt][jj] = self.F[tt][jj] - toto.F[tt][jj]
            for jj in self.Tw[tt]:
                newValue.Tw[tt][jj] = self.Tw[tt][jj] - toto.Tw[tt][jj]
#                newValue.Twc[tt][jj] = self.Twc[tt][jj] - toto.Twc[tt][jj]
#            for jj in self.Wc[tt]:
#                newValue.Wc[tt][jj] = self.Wc[tt][jj] - toto.Wc[tt][jj]
#                newValue.Vc[tt][jj] = self.Vc[tt][jj] - toto.Vc[tt][jj]
#                newValue.Tc[tt][jj] = self.Tc[tt][jj] - toto.Tc[tt][jj]
#                newValue.Fc[tt][jj] = self.Fc[tt][jj] - toto.Fc[tt][jj]
        return newValue

    def __mul__(self,alpha):
        newValue = self.copy()
#        newValue.listTimeSteps = self.listTimeSteps
#        for tt in self.W.keys():
        for tt in self.listTimeSteps:
            newValue.init(tt)
            for jj in self.W[tt]:
                newValue.W[tt][jj] = alpha * self.W[tt][jj]
            for jj in self.F[tt]:
                newValue.V[tt][jj] = alpha * self.V[tt][jj]
                newValue.T[tt][jj] = alpha * self.T[tt][jj]
                newValue.F[tt][jj] = alpha * self.F[tt][jj]
            for jj in self.Tw[tt]:
                newValue.Tw[tt][jj] = alpha * self.Tw[tt][jj]
#                newValue.Twc[tt][jj] = alpha * self.Twc[tt][jj]
#            for jj in self.Wc[tt]:
#                newValue.Wc[tt][jj] = alpha * self.Wc[tt][jj]
#                newValue.Vc[tt][jj] = alpha * self.Vc[tt][jj]
#                newValue.Tc[tt][jj] = alpha * self.Tc[tt][jj]
#                newValue.Fc[tt][jj] = alpha * self.Fc[tt][jj]
        return newValue

    def __rmul__(self,alpha):
        newValue = self.copy()
#        newValue.listTimeSteps = self.listTimeSteps
#        for tt in self.W.keys():
        for tt in self.listTimeSteps:
            newValue.init(tt)
            for jj in self.W[tt]:
                newValue.W[tt][jj] = alpha * self.W[tt][jj]
            for jj in self.F[tt]:
                newValue.V[tt][jj] = alpha * self.V[tt][jj]
                newValue.T[tt][jj] = alpha * self.T[tt][jj]
                newValue.F[tt][jj] = alpha * self.F[tt][jj]
            for jj in self.Tw[tt]:
                newValue.Tw[tt][jj] = alpha * self.Tw[tt][jj]
#                newValue.Twc[tt][jj] = alpha * self.Twc[tt][jj]
#            for jj in self.Wc[tt]:
#                newValue.Wc[tt][jj] = alpha * self.Wc[tt][jj]
#                newValue.Vc[tt][jj] = alpha * self.Vc[tt][jj]
#                newValue.Tc[tt][jj] = alpha * self.Tc[tt][jj]
#                newValue.Fc[tt][jj] = alpha * self.Fc[tt][jj]
        return newValue

    def multByArray(self,alpha):
        newValue = self.copy()
#        newValue.listTimeSteps = self.listTimeSteps
#        timeStep = self.W.keys()
        timeStep = copy.deepcopy(self.listTimeSteps)
        timeStep.remove(-1)
        alpha = {tt:alpha[tt] for tt in timeStep}
        alpha[-1] = 0.5
#        for tt in self.W.keys():
        for tt in self.listTimeSteps:
            newValue.init(tt)
            for jj in self.W[tt]:
                newValue.W[tt][jj] = alpha[tt] * self.W[tt][jj]
            for jj in self.F[tt]:
                newValue.V[tt][jj] = alpha[tt] * self.V[tt][jj]
                newValue.T[tt][jj] = alpha[tt] * self.T[tt][jj]
                newValue.F[tt][jj] = alpha[tt] * self.F[tt][jj]
            for jj in self.Tw[tt]:
                newValue.Tw[tt][jj] = alpha[tt] * self.Tw[tt][jj]

        return newValue
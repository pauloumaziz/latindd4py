#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 14:21:37 2017
Functions to load meshes and information about the meshes
@author: oumaziz
"""
try:
    import code_aster
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
####
import shelve
from salomedd4aster.prepareSubStructuring import SubStructuredMesh
from latindd4py.aster_functions.basics import get_aster_mesh
import pdb

def get_mesh_and_mesh_info(param):
    """
    Load the mesh of the subdomain and the information related to it
    Input:
        -param: class of parameters
    Return a class already defined in the salomedd4aster.prepareSubStructuring
    and modify param to add new information
    """
    d = shelve.open(param.resPrepare+"Parameters/Parameters_{}/parameters".format(rank))
    subStructuredMesh = d['subStructuredMesh']
    d.close()
    mesh = get_aster_mesh(param.resPrepare+"MED/S{}.med".format(rank))
    subStructuredMesh.Mail = mesh
#    param.solver = code_aster.MultFrontSolver(code_aster.Renumbering.Sans)
    param.solver = code_aster.MultFrontSolver()
    param.mesh = mesh
    if param.method == 'latin':
        ### Case of corners
        if param.corners == 'NO':
            for jj,neighbour in enumerate(subStructuredMesh.corner[rank]):
                if subStructuredMesh.corner[rank][neighbour]:
                    subStructuredMesh.neighbourSD[rank].remove(neighbour)
        ### Update data about connectivity of subdomains and interfaces
        neighbourSD = comm.allgather(subStructuredMesh.neighbourSD)
        pp = 0
        for kk in range(param.numberSubDomain):
            subStructuredMesh.neighbourSD[kk] = neighbourSD[kk][kk]
            subStructuredMesh.connecSDInterface[kk] = []
        for kk in range(param.numberSubDomain):
            for jj in subStructuredMesh.neighbourSD[kk]:
                if kk < jj:
                    subStructuredMesh.connecInterfSD.append([kk,jj])
                    subStructuredMesh.connecSDInterface[kk].append(pp)
                    subStructuredMesh.connecSDInterface[jj].append(pp)
                    pp += 1
#        pdb.set_trace()
        param.numberInterface = pp


#    else:

    return subStructuredMesh


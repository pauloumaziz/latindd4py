#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 16:59:40 2017
Computation of the global operator for the multiscale approach
@author:  oumaziz
"""
from latindd4py.aster_functions.solve_mechanics import problemOnSolePython, subDomainResolutionPython
import scipy.sparse as sp
from scipy.sparse import linalg as splinalg
import numpy as np
import itertools

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class globalOperator():
    """
    Class to build global operators
    """
    def __init__(self,subDomain,param):
        """
        Init function.
        Input:
            -subDomain: class of Subdomain
            -param: class of parameters
        """
        param.logger.info("          Computation of the macro operator")
        self.compute_macro_operator(subDomain,param)

    def compute_macro_operator(self,subDomain,param):
        """
        Compute the macro operator and add attributes in the class
        globalOperator.
        Input:
            -subDomain: class of Subdomain
            -param: class of parameters
        """

        if param.macroBasis == 'GENEO':
            ### Computation of the local contributions of the subdomains
            kAtWp = dict()
            KNtkAtWp = dict()
            for jj,SD in enumerate(subDomain.neighbourSD):
                ### Calcul de kAtW
                kAtW = dict()
                kAtW[SD] = dict()
                kAtW[rank] = dict()
                toto = dict()
                for pp in subDomain.Macro[SD]['ConnecMacro']:
                    kAtW[SD][pp] = problemOnSolePython(subDomain.sole[jj],
                                                       subDomain.Macro[SD][pp],
                                                       param)
                toto[SD] = np.concatenate([kAtW[SD][pp].valField.reshape((kAtW[SD][pp].valField.shape[0],1)) for pp in subDomain.Macro[SD]['ConnecMacro']],axis=1)
                for pp in subDomain.Macro[rank][jj]['ConnecMacro']:
                    kAtW[rank][pp] = problemOnSolePython(subDomain.sole[jj],
                                                       subDomain.Macro[rank][jj][pp],
                                                       param)
                toto[rank] = np.concatenate([kAtW[rank][pp].valField.reshape((kAtW[rank][pp].valField.shape[0],1)) for pp in subDomain.Macro[rank][jj]['ConnecMacro']],axis=1)
                kAtWp[jj] = np.concatenate([toto[kk] for kk in sorted([rank,SD])],axis=1)
                KNtkAtWp[jj] = subDomain.soleSD.model.K_sparse_lu.solve(subDomain.Operator['N'][jj].T.dot(kAtWp[jj]))
            ### Creation of the operator kAtW in a sparse matrix
            kAtWg = sp.lil_matrix((sum([subDomain.dim*len(subDomain.soleSD.mesh.NO['N{}'.format(jj)]) for jj in range(len(subDomain.neighbourSD))]),sum([subDomain.listNumberModes[jj] for jj in subDomain.neighbourSD+[rank]])))
            posColIni = 0
            listSD = sorted([rank]+subDomain.neighbourSD)
            for jj,SD in enumerate(subDomain.neighbourSD):
                posCol = []
                posLine = []
                for kk in sorted([rank,SD]):
                    posColIni = sum([subDomain.listNumberModes[pp] for pp in listSD[:listSD.index(kk)]])
                    posCol += list(range(posColIni,posColIni+subDomain.listNumberModes[kk]))
                posLineIni = sum([subDomain.dim*len(subDomain.soleSD.mesh.NO['N{}'.format(pp)]) for pp in range(sorted(subDomain.neighbourSD).index(SD))])
                posLine += list(range(posLineIni,posLineIni+subDomain.dim*len(subDomain.soleSD.mesh.NO['N{}'.format(jj)])))
                kAtWg[np.ix_(posLine,posCol)] = kAtWp[jj]

            WtAkNKNtkAtWp = {jj:dict() for jj in range(len(subDomain.neighbourSD))}
            WtAkAtWp = {jj:dict() for jj in range(len(subDomain.neighbourSD))}
            opeMacro = sp.lil_matrix((sum(subDomain.listNumberModes),sum(subDomain.listNumberModes)))
            KNtkAtWg = sp.lil_matrix((subDomain.dim*len(subDomain.soleSD.mesh.NO['N_SD']),sum(subDomain.listNumberModes)))
            for jj,SD in enumerate(subDomain.neighbourSD):
                posCol = list()
                for kk in sorted([rank,SD]):
                    posIni = sum(subDomain.listNumberModes[:kk])
                    posCol += list(range(posIni,posIni+subDomain.listNumberModes[kk]))
                KNtkAtWg[np.ix_(list(range(subDomain.dim*len(subDomain.soleSD.mesh.NO['N_SD']))),posCol)] += sp.lil_matrix(subDomain.Operator['N']['SD'].dot(KNtkAtWp[jj]))
                WtAkAtWp[jj] = subDomain.Operator['AtW'][jj].T.dot(kAtWp[jj])
                opeMacro[np.ix_(posCol,posCol)] += -WtAkAtWp[jj]
                for ii,SD1 in enumerate(subDomain.neighbourSD):
                    posLine = list()
                    for kk in sorted([rank,SD1]):
                        posIni = sum(subDomain.listNumberModes[:kk])
                        posLine += list(range(posIni,posIni+subDomain.listNumberModes[kk]))
                    WtAkNKNtkAtWp[jj][ii] = kAtWp[ii].T.dot(subDomain.Operator['N'][ii].dot(KNtkAtWp[jj]))
                    opeMacro[np.ix_(posLine,posCol)] += WtAkNKNtkAtWp[jj][ii]

            opeMacroGlob = comm.allreduce(opeMacro,op=MPI.SUM)
#
            self.kAtW = kAtWg
            self.Km = opeMacroGlob.tocsc()
            self.luKm = sp.linalg.splu(self.Km)
            self.KNtkAtW = KNtkAtWg
        else:
            kAtW = dict()
            ### Parallel computation of kAtW
            param.logger.info("               Problem on soles : kAtW")
            # Loop over the neighbour
            for jj in range(len(subDomain.connecInterf)):
                kAtW[jj] = dict()
                # Loop over the macro modes of the interface
                for pp in range(len(subDomain.Macro[rank][jj]['ConnecMacro'])):
                    # Solve a mechanical problem with Dirichlet conditions
                    kAtW[jj][pp] = problemOnSolePython(subDomain.sole[jj],
                                       subDomain.Macro[rank][jj][subDomain.Macro[rank][jj]['ConnecMacro'][pp]],param)
            ### Parallel computation of KNtkAtW and NKNtkAtW
            param.logger.info("               Resolution on sub-domain : NKNtkAtW and kNKNtkAtW")
            NKNtkAtW,kNKNtkAtW = self.macroResSD(kAtW,subDomain,param)

            ### Conversion in python field for the global communication
            param.logger.info("               Conversion in python for communication")
            KNtkAtWp = self.localAssembly(NKNtkAtW,subDomain,param,case='SD')
            kAtWp = self.localAssembly(kAtW,subDomain,param,case='interf')

            NKNtkAtWp = subDomain.Operator['Ne'].dot(KNtkAtWp)
            localMacroOpe = kAtWp.T.dot(NKNtkAtWp - subDomain.Operator['AtW'])
            # Compute the position in the global macro operators of the contribution
            # of the subdomain
            pos = list(itertools.chain.from_iterable([list(range(sum(subDomain.listNumberModes[:jj]),sum(subDomain.listNumberModes[:jj+1]))) for jj in subDomain.connecInterf]))
            opeMacro = sp.lil_matrix((sum(subDomain.listNumberModes),sum(subDomain.listNumberModes)))

            param.logger.info("               All reduce")
            gatherMacroOpe = comm.allgather([localMacroOpe,pos])
            # Fill the global macro operator
            for jj in range(param.numberSubDomain):
                opeMacro[np.ix_(gatherMacroOpe[jj][1],gatherMacroOpe[jj][1])] += gatherMacroOpe[jj][0]
            self.Km = opeMacro
            param.logger.info("               Computation of the pseudo inverse")
            self.luKm = splinalg.splu(self.Km)
            self.kAtW = kAtWp
            self.KNtkAtW = KNtkAtWp

    def localAssembly(self,dictField,subDomain,param,case):
        """
        Local assembly on the sub-domain of the macro interface distribution
        Input:
            -dictField: field stored in a dictionnary
            -subDomain: class Subdomain
            -param: class of parameters
            -case: case if the assembly is about the subdomain or on the interfaces
        Output:
            -scipy sparse matrix
        """
        if case == 'SD':
            numberModesInterfaces = sum([len(subDomain.Macro[rank][kk]['ConnecMacro']) for kk in range(len(subDomain.connecInterf))])
            c = sp.lil_matrix((dictField[0][0]['W'].valField.shape[0],numberModesInterfaces))
            for jj in range(len(subDomain.connecInterf)):
                pos0 = sum([len(subDomain.Macro[rank][kk]['ConnecMacro']) for kk in range(jj)])
                pos = list(range(sum([len(subDomain.Macro[rank][kk]['ConnecMacro']) for kk in range(jj)]),pos0 + len(subDomain.Macro[rank][jj]['ConnecMacro'])))
                c[np.ix_(list(range(0,c.shape[0])),pos)] = sp.lil_matrix(np.concatenate(tuple(dictField[jj][pp]['W'].valField.reshape(dictField[jj][pp]['W'].valField.shape[0],1) for pp in range(len(subDomain.Macro[rank][jj]['ConnecMacro']))), axis = 1))
        if case == 'interf':
            c = sp.block_diag([np.concatenate([dictField[jj][pp].valField.reshape(dictField[jj][pp].valField.shape[0],1) for pp in range(len(subDomain.Macro[rank][jj]['ConnecMacro']))], axis = 1) for jj in range(len(subDomain.connecInterf))],format="csr")

        return c.tocsc()

    def macroResSD(self,kAtW,subDomain,param):
            """
            Compute the mechanical problems on the subdomains. Return a
            displacment field on the edge of the sub-domain for each interface
            and macro-modes.
            Input:
                -kAtW : dictionnary of the right-handed member
                -subDomain : class SubDomain
                -param : class of parameters
            Output:
                -NKNtkAtW: dictionnary of fields
                -kNKNtkAtW: dictionnary of fields
            """
            NKNtkAtW = dict()
            kNKNtkAtW = dict()
            # Loop over the neighbour
            for jj in range(len(subDomain.connecInterf)):
                NKNtkAtW[jj] = dict()
                kNKNtkAtW[jj] = dict()
                # Loop over the macro modes of the interface
                for pp in range(len(subDomain.Macro[rank][jj]['ConnecMacro'])):
                    # Solve a mechanical problem on subdomain with neumann conditions
                    NKNtkAtW[jj][pp],kNKNtkAtW[jj][pp] = subDomainResolutionPython(subDomain,kAtW[jj][pp],param,multi = 'YES')
            return NKNtkAtW, kNKNtkAtW

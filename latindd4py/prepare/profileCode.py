#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 11:19:41 2017

@author:  oumaziz
"""

import cProfile, pstats, io

class profileCode:
    """
    Class to profile a part of the code
    """
    def __init__(self):
        """
        Create where the profiling need to begin
        """
        self.prof = cProfile.Profile()
        self.prof.enable()

    def end(self,name,param):
        """
        Ends the profiling and save it in a file
        Input:
            -name: string, name of the file to save the profile
            -param: class of parameters
        """
        self.prof.disable()
        s = io.StringIO()
        sortby = 'filename'
        ps = pstats.Stats(self.prof, stream=s).sort_stats(sortby)
        ps.print_stats()
        f = open(param.resDir+name+".txt","w")
        f.write(s.getvalue())
        f.close()

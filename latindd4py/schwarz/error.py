#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  8 09:29:14 2017

@author: oumaziz
"""

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

import pdb

class Error():
    """
    Class for the error
    """
    def __init__(self):
        """
        Initialisation of the class
        """
        self.norm_residual = 1.
        self.ref = 1.

    def compute(self,residual):
        """
        Compute the norm of the residual
        Input:
            -residual: vector of residual
        """
        local_contribution = residual ** residual

        self.norm_residual = comm.allreduce(local_contribution,op=MPI.SUM) / self.ref

    def set_reference(self,norm_ref):
        """
        Set the reference for the normalization
        Input:
            -norm_ref: value of the norm of the residual to normalize
        """
        self.ref = norm_ref

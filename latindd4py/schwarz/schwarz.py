#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 15:18:31 2017
Class for an Additive Schwarz Method
@author: oumaziz
"""

import numpy as np
from latindd4py.aster_functions import compute_nodal_reaction_from_field_on_group,imposed_boundary_conditions,subDomainResolution
from latindd4py.aster_functions.interface_field import pythonField
from latindd4py.aster_functions.save_data import saveTxt
from latindd4py.aster_functions.basics import get_nodes_from_gno
from latindd4py.aster_functions.save_data import exportResults2MED
from .error import Error

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

import pdb

class Schwarz:
    """
    Class of Schwarz method
    """
    def __init__(self,subDomain,param):
        """
        Initiate the class
        Input:
            -param: class of parameters
        """
        self.error_criterion = param.error
        self.maxIteration = param.maxIteration
        self.err = 1
        self.iteration = 0
        self.field = subDomain.Value.copy()
        self.norm_residual = Error()

    def computeAdditive(self,subDomain,param):
        """
        Launch the computation of the additive Schwarz method.
        Input:
            -subDomain: class Subdomain
            -param: class of parameters
        """

        while self.norm_residual.norm_residual > self.error_criterion and self.iteration < self.maxIteration:
            param.logger.info(" ### --------------------------------------------")
            param.logger.info(" ### Beginning of iteration {}".format(self.iteration))
            param.logger.info(" ### --------------------------------------------")
            ### Compute the residual over the subdomain
            param.logger.info("          Compute nodal reaction on subdomain : Au")
            Au = compute_nodal_reaction_from_field_on_group(self.field.W[0]['all_nodes'].convert2aster(param).field,subDomain.soleSD.model.MODELE,subDomain.soleSD.model.assigned_material,subDomain.soleSD.mesh.GMA['Soverlap'],subDomain.soleSD.model.boundary_conditions['imposed_dirichlet'])
            param.logger.info("          Compute the residual on the subdomain : f-Au")
            residual = (subDomain.f - Au).extract_field_on_nodes(subDomain.soleSD.mesh.NO['SD_without_diri'])
            self.residual = residual
            self.Au = Au
            param.logger.info("          Compute the norm of the residual")
            self.norm_residual.compute(residual)
            if self.iteration == 0:
                self.norm_residual.set_reference(self.norm_residual.norm_residual)
                self.norm_residual.compute(residual)                            # The residual is recompute to have a normed residual
            saveTxt(self.norm_residual.norm_residual,param.resDir+"schwarz_residual.txt")
            ### Compute the displacement due to the residual
            param.logger.info("          Compute the displacement on the subdomain under the residual load")
            W,T = subDomainResolution(subDomain,residual,param,multi = 'OUI')
            ### Communication to exchange displacement fields
            param.logger.info("          Communication to other subdomains")
            for kk,SD in enumerate(subDomain.neighbourSD):
                ### Send the fields
                param.logger.info("          Send and Recieve W,T,F to "+str(SD))
                Wkk = comm.sendrecv(W[kk],
                                    dest = SD,
                                    sendtag = rank,
                                    recvtag=SD
                                    )
                ### Remap the topology
                Wkk.nodes = subDomain.soleSD.mesh.NO['N{}b'.format(kk)]

                W[kk] = Wkk
            W['all_nodes'].replace_part_of_field([W[kk] for kk in range(len(subDomain.neighbourSD))])

            for kk in range(len(subDomain.neighbourSD)):
                self.field.W[0][kk] = self.field.W[0][kk] + W[kk]
            self.field.W[0]['all_nodes'] = self.field.W[0]['all_nodes'] + W['all_nodes']
            self.field.W[0]['W'] = self.field.W[0]['W'] + W['W']
            self.iteration += 1
        exportResults2MED(self.field,subDomain,param.resDir+"MED/iteration{}".format(self.iteration),param)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 10:24:30 2018

@author: oumaziz
"""

import sys
import argparse
import xml.etree.ElementTree as et
import glob
import os
import re
from os import system as osym
from tqdm import tqdm


parser = argparse.ArgumentParser(description="Preparation of the meshes of subdomains")
parser.add_argument('-data',help='Input data file',type=str,required=True)
parser.add_argument('-iteration',help='Number of the iteration',nargs='+',default=None)
parser.add_argument('-results',help='Location of the directory of results',type=str,required=True)

args = parser.parse_args()
dataFile = args.data
iteration = args.iteration
results = args.results.rstrip("/")

from salome_instance import SalomeInstance
instance = SalomeInstance.start()
print("Instance created and now running on port", instance.get_port())



print(("Directory of results : "+results))
print(("Data file            : "+dataFile))
print(("Iteration            : "+str(iteration)))


import salome
salome.salome_init()
theStudy = salome.myStudy
import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)

tree = et.parse(dataFile)
root = tree.getroot()
rootResDir = root.find("method/resDir").text+"/"
nameStudy = root.find("nameOfStudy").text
numberSubDomain = int(root.find("method/substructure").text)
if root.find("method/quasistatic").attrib['choice'] == 'YES':
    numberTimeStep = int(root.find("method/quasistatic/Ntimestep").text)
else:
    numberTimeStep = 1




import pvsimple
#### import the simple module from the paraview
from pvsimple import *
#### disable automatic camera reset on 'Show'
pvsimple._DisableFirstRenderCameraReset()

### Obtain the iterations if the iteration is not given in argument
if not iteration:
    files = [file for file in os.listdir(results+"/"+nameStudy+"_0/MED") if os.path.isfile(os.path.join(results+"/"+nameStudy+"_0/MED",file))]
    iteration = sorted(list(set([int(re.sub(r"(iteration|-[0-9]*t.med)",'',kk)) for kk in files])))

print(("Iterations : "+str(iteration)))


SD = dict()
osym("mkdir -p "+results+"/Macro")
## Load MED file for the group data set
for ii in tqdm(list(range(numberSubDomain)),desc='SD ',file=sys.stdout,ascii=True,unit=' SD',dynamic_ncols=True):
    SD[ii] = MEDReader(FileName=results+'/'+nameStudy+'_{}/MED/iteration{}-{}t.med'.format(ii,iteration[0],0))
    SD[ii].AllArrays = ['TS1/00000001/ComSup1/numberMacroModes@@][@@P1']
## Group the data set
structure = GroupDatasets(Input=[SD[ii] for ii in range(numberSubDomain)])
## Export cell data to point data
#eLNOMesh1 = ELNOfieldToSurface(Input=structure)
## Export results to vtm
SaveData(results+'/Macro/mapNumberMacro.vtm')

instance.stop()

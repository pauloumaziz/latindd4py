#/usr/bin/env python3
# -*- coding: utf-8 -*-

from tqdm import tqdm
import xml.etree.ElementTree as et
import re
import os
import argparse
parser = argparse.ArgumentParser(description="Preparation of the meshes of subdomains")
parser.add_argument('-data',help='Input data file',type=str,required=True)
parser.add_argument('-iteration',help='Number of the iteration',nargs='+',default=None)
parser.add_argument('-results',help='Location of the directory of results',type=str,required=True)
parser.add_argument('-reference',help='Location of the directory of reference',type=str,required=True)

args = parser.parse_args()
dataFile = args.data
iteration = args.iteration
results = args.results.rstrip("/")
reference = args.reference.rstrip("/")

from salome_instance import SalomeInstance
instance = SalomeInstance.start()
print("Instance created and now running on port", instance.get_port())

print(("Directory of results : "+results))
print(("Data file            : "+dataFile))
print(("Iteration            : "+str(iteration)))

import salome
salome.salome_init()
theStudy = salome.myStudy
import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)

tree = et.parse(dataFile)
root = tree.getroot()
rootResDir = root.find("method/resDir").text+"/"
nameStudy = root.find("nameOfStudy").text
dimension = root.find("method/dimension").text
numberSubDomain = int(root.find("method/substructure").text)
if root.find("method/quasistatic").attrib['choice'] == 'YES':
    numberTimeStep = int(root.find("method/quasistatic/Ntimestep").text)
else:
    numberTimeStep = 1

import pvsimple
#### import the simple module from the paraview
from pvsimple import *
#### disable automatic camera reset on 'Show'
pvsimple._DisableFirstRenderCameraReset()

### Obtain the iterations if the iteration is not given in argument
if not iteration:
    files = [file for file in os.listdir(results+"/"+nameStudy+"_0/MED") if os.path.isfile(os.path.join(results+"/"+nameStudy+"_0/MED",file))]
    iteration = sorted(list(set([int(re.sub(r"(iteration|-[0-9]*t.med)",'',kk)) for kk in files])))

print(("Iterations : "+str(iteration)))
#import pdb
#pdb.set_trace()
for counter,it in tqdm(enumerate(iteration),desc='Iterations ',file=sys.stdout,ascii=True,unit=' iteration',dynamic_ncols=True):
    for tt in tqdm(list(range(numberTimeStep)),desc='Timestep ',file=sys.stdout,ascii=True,unit=' timestep',dynamic_ncols=True):
        calculator3 = dict()
        calculator4 = dict()
        for kk in tqdm(list(range(numberSubDomain)),desc='SD ',file=sys.stdout,ascii=True,unit=' SD',dynamic_ncols=True):
            # create a new 'MED Reader'
            itera = MEDReader(FileName=results+'/'+nameStudy+'_{}/MED/iteration{}-{}t.med'.format(kk,it,tt))
            # create a new 'Merge Blocks'
            mergeBlocks1 = MergeBlocks(Input=itera)
            # create a new 'Calculator' Z
            if dimension==3:
                calculator1z = Calculator(Input=mergeBlocks1)
                # Properties modified on calculator1z
                calculator1z.Function = ''
                # Properties modified on calculator1z
                calculator1z.ResultArrayName = 'deplZ'
                calculator1z.Function = 'depl_DZ'
            # create a new 'Calculator' X
            calculator1x = Calculator(Input=mergeBlocks1)
            # Properties modified on calculator1z
            calculator1x.Function = ''
            # Properties modified on calculator1z
            calculator1x.ResultArrayName = 'deplX'
            calculator1x.Function = 'depl_DX'
            # create a new 'Calculator' Y
            calculator1y = Calculator(Input=mergeBlocks1)
            # Properties modified on calculator1z
            calculator1y.Function = ''
            # Properties modified on calculator1z
            calculator1y.ResultArrayName = 'deplY'
            calculator1y.Function = 'depl_DY'
            # create a new 'MED Reader'
            refMed = MEDReader(FileName=reference+'/S{}.med'.format(kk))
            # create a new 'Merge Blocks'
            mergeBlocks2 = MergeBlocks(Input=refMed)
            if dimension==3:
                # create a new 'Calculator'
                calculator2z = Calculator(Input=mergeBlocks2)
                # Properties modified on calculator2
                calculator2z.Function = ''
                # Properties modified on calculator2
                calculator2z.ResultArrayName = 'refZ'
                calculator2z.Function = 'depl_DZ'
            # create a new 'Calculator'
            calculator2x = Calculator(Input=mergeBlocks2)
            # Properties modified on calculator2
            calculator2x.Function = ''
            # Properties modified on calculator2
            calculator2x.ResultArrayName = 'refX'
            calculator2x.Function = 'depl_DX'
            # create a new 'Calculator'
            calculator2y = Calculator(Input=mergeBlocks2)
            # Properties modified on calculator2
            calculator2y.Function = ''
            # Properties modified on calculator2
            calculator2y.ResultArrayName = 'refY'
            calculator2y.Function = 'depl_DY'
            # set active source
            SetActiveSource(calculator1x)
            # create a new 'Append Attributes'
            if dimension==3:
                appendAttributes1 = AppendAttributes(Input=[calculator1x, calculator2x,calculator1y, calculator2y,calculator1z, calculator2z])
            else:
                appendAttributes1 = AppendAttributes(Input=[calculator1x, calculator2x,calculator1y, calculator2y])
            # create a new 'Calculator'
            calculator3[kk] = Calculator(Input=appendAttributes1)
            calculator4[kk] = Calculator(Input=appendAttributes1)
            calculator3[kk].ResultArrayName = 'Ecart relatif global'
            calculator4[kk].ResultArrayName = 'Ecart relatif local'
            if dimension==3:
                # Properties modified on calculator3
                calculator4[kk].Function = 'sqrt((refX-deplX)^2+(refY-deplY)^2+(refZ-deplZ)^2)/sqrt((refX+deplX)^2+(refY+deplY)^2+(refZ+deplZ)^2)'
                calculator3[kk].Function = 'sqrt((refX-deplX)^2+(refY-deplY)^2+(refZ-deplZ)^2)/0.057'
            else:
                calculator4[kk].Function = 'sqrt((refX-deplX)^2+(refY-deplY)^2)/sqrt((refX+deplX)^2+(refY+deplY)^2)'
                calculator3[kk].Function = 'sqrt((refX-deplX)^2+(refY-deplY)^2)/0.057'
            #pdb.set_trace()
#            SaveData(results+'/comparison{}.med'.format(kk), proxy=calculator4[kk])


#        structure3 = GroupDatasets(Input=[calculator3[ii] for ii in range(numberSubDomain)]+[calculator4[ii] for ii in range(numberSubDomain)])
        structure4 = GroupDatasets(Input=[calculator3[ii] for ii in range(numberSubDomain)])
        eLNOMesh1 = ELNOfieldToSurface(Input=structure4)
        SaveData(results+'/compareRef{}_{}.vtm'.format(it,tt), proxy=eLNOMesh1)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 17:06:58 2018

@author: oumaziz
"""

import sys
import argparse
import xml.etree.ElementTree as et
import glob
import os
import re
from os import system as osym
from tqdm import tqdm


parser = argparse.ArgumentParser(description="Preparation of the meshes of subdomains")
parser.add_argument('-data',help='Input data file',type=str,required=True)
parser.add_argument('-iteration',help='Number of the iteration',nargs='+',default=None)
parser.add_argument('-results',help='Location of the directory of results',type=str,required=True)

args = parser.parse_args()
dataFile = args.data
iteration = args.iteration
results = args.results.rstrip("/")

from salome_instance import SalomeInstance
instance = SalomeInstance.start()
print("Instance created and now running on port", instance.get_port())



print(("Directory of results : "+results))
print(("Data file            : "+dataFile))
print(("Iteration            : "+str(iteration)))


import salome
salome.salome_init()
theStudy = salome.myStudy
import salome_notebook
notebook = salome_notebook.NoteBook(theStudy)

tree = et.parse(dataFile)
root = tree.getroot()
rootResDir = root.find("method/resDir").text+"/"
nameStudy = root.find("nameOfStudy").text
numberSubDomain = int(root.find("method/substructure").text)
if root.find("method/quasistatic").attrib['choice'] == 'YES':
    numberTimeStep = int(root.find("method/quasistatic/Ntimestep").text)
else:
    numberTimeStep = 1




import pvsimple
#### import the simple module from the paraview
from pvsimple import *
#### disable automatic camera reset on 'Show'
pvsimple._DisableFirstRenderCameraReset()

### Obtain the iterations if the iteration is not given in argument
if not iteration:
    files = [file for file in os.listdir(results+"/"+nameStudy+"_0/MED") if os.path.isfile(os.path.join(results+"/"+nameStudy+"_0/MED",file))]
    iteration = sorted(list(set([int(re.sub(r"(iteration|-[0-9]*t.med)",'',kk)) for kk in files])))

print(("Iterations : "+str(iteration)))


### Create dataset and export cell data to point data
for timeStep in tqdm(list(range(numberTimeStep)),desc='Timestep ',file=sys.stdout,ascii=True,unit=' timestep',dynamic_ncols=True):
    osym("mkdir -p "+results+"/Iterations")
    for jj,kk in tqdm(enumerate(iteration),desc='Iterations ',file=sys.stdout,ascii=True,unit=' iteration',dynamic_ncols=True):
        SD = dict()
        ## Load MED file for the group data set
        for ii in tqdm(list(range(numberSubDomain)),desc='SD ',file=sys.stdout,ascii=True,unit=' SD',dynamic_ncols=True):
            SD[ii] = MEDReader(FileName=results+'/'+nameStudy+'_{}/MED/iteration{}-{}t.med'.format(ii,kk,timeStep))
        ## Group the data set
        structure = GroupDatasets(Input=[SD[ii] for ii in range(numberSubDomain)])
        ## Export cell data to point data
        eLNOMesh1 = ELNOfieldToSurface(Input=structure)
        ## Export results to vtm
        SaveData(results+'/Iterations/Iterations_{}/Iterations_{}t_{}.vtm'.format(timeStep,timeStep,kk), proxy=eLNOMesh1)



### Create collection for timestep according to the iterations
for jj,kk in tqdm(enumerate(iteration),desc='Iterations ',file=sys.stdout,ascii=True,unit=' iteration',dynamic_ncols=True):
    osym("mkdir -p "+results+"/EvolTimeStep/Iterations_{}".format(kk))
    for timeStep in tqdm(list(range(numberTimeStep)),desc='Timestep ',file=sys.stdout,ascii=True,unit=' timestep',dynamic_ncols=True):
        root = et.Element('VTKFile')
        root.set('type','vtkMultiBlockDataSet')
        root.set('version','1.0')
        root.set('byte_order',"LittleEndian")
        root.set('header_type',"UInt64")
        collection = et.Element('vtkMultiBlockDataSet')
        files = sorted(glob.glob(results+'/Iterations/Iterations_{}/Iterations_{}t_{}/*.vtu'.format(timeStep,timeStep,kk)))
        for i,f in enumerate(files):
            dataset = et.Element('DataSet')
            dataset.set('iteration',str(timeStep))
            dataset.set('index',str(i))
            dataset.set('file',f)
            collection.append(dataset)
        root.append(collection)
        with open(results+'/EvolTimeStep/Iterations_{}/Iterations_{}.vtm'.format(kk,timeStep),'w') as fileoutput:
            fileoutput.write(et.tostring(root).decode())

### Create collection for the iterations according to the timestep
for timeStep in tqdm(list(range(numberTimeStep)),desc='Timestep ',file=sys.stdout,ascii=True,unit=' timestep',dynamic_ncols=True):
    osym("mkdir -p "+results+"/Iterations/EvolTimeStep_{}".format(timeStep))
    for jj,kk in tqdm(enumerate(iteration),desc='Iterations ',file=sys.stdout,ascii=True,unit=' iteration',dynamic_ncols=True):
        root = et.Element('VTKFile')
        root.set('type','vtkMultiBlockDataSet')
        root.set('version','1.0')
        root.set('byte_order',"LittleEndian")
        root.set('header_type',"UInt64")
        collection = et.Element('vtkMultiBlockDataSet')
        files = sorted(glob.glob(results+'/Iterations/Iterations_{}/Iterations_{}t_{}/*.vtu'.format(timeStep,timeStep,kk)))
        for i,f in enumerate(files):
            dataset = et.Element('DataSet')
            dataset.set('timestep',str(timeStep))
            dataset.set('index',str(i))
            dataset.set('file',f)
            collection.append(dataset)
        root.append(collection)
        with open(results+'/Iterations/EvolTimeStep_{}/Iterations_{}.vtm'.format(timeStep,kk),'w') as fileoutput:
            fileoutput.write(et.tostring(root).decode())


instance.stop()

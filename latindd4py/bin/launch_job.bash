#!/bin/bash
source ~/.bashrc

### Test if data file is given
# if [ -z $1 ];
#     then
#         echo "Error: No data file given"
#         exit 1
# fi
PARSED_OPTIONS=$(getopt -o ha:s:d: --long help,aster:,sources:,data:  -- "$@")

if [ $? -ne 0 ];
then
  exit 1
fi

eval set -- "$PARSED_OPTIONS"

usage(){
    echo "Usage launch_job.bash -h -a -s -d or --help --aster --sources --data"
    echo "-h or --help    : print help"
    echo "-a or --aster   : precise the directory of installation of asterxx"
    echo "-d or --data    : specify the data file written in xml"
    echo "-s or --sources : precise the location of the package latindd4py to add it in PYTHONPATH"
    echo "-w or --wordir  : precise the directory where to launch the computation"
}
if [ -f ~/.asterxxrc ]; then
    source ~/.asterxxrc
fi

while true;
do
  case "$1" in
    -h|--help)
      usage ;
      exit 0 ;;
    -a|--aster)
      case "$2" in
        *) aster_dir=$2 ; shift 2;;
      esac;;
    -s|--sources)
      case "$2" in
        *) sources=$2 ; shift 2;;
      esac;;
    -d|--data)
      case "$2" in
        *) dataFile=$2 ; shift 2 ;;
      esac;;
    -w|--workdir)
      case "$2" in
        *) work_dir=$2 ; shift 2 ;;
      esac;;
    --) shift ; break;;
    *) break ;;
  esac
done

if [ -v $aster_dir ]; then
  echo "No path to asterxx install ! Define it in the .asterxxrc or give it with the option -a (--aster)" ;
  exit 2;
fi

if [ -v $sources ]; then
  echo "No path to the sources of latindd4py ! Define it in the .asterxxrc or give it with the option -s (--sources)" ;
  exit 2;
fi

if [ -v $dataFile ]; then
    echo "No data file given ! Give it with the option -d (--data)";
    exit 2;
fi

if [ -v $work_dir ]; then
    echo "No wordir path given ! Define it in the .asterxxrc or give it with the option -d (--data)";
    exit 2;
fi

echo "Source asterxx  : $aster_dir/share/aster/profile.sh"
echo "Sources of code : $sources"
echo "Work directory  : $work_dir"
echo "Data file       : $dataFile"

### Export the aster profile
source $aster_dir/share/aster/profile.sh
export PYTHONPATH=$PYTHONPATH:$sources
### To avoid multithreading in python numpy and scipy and potential segmentation faults
export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=1
### ---------------------------------------------------------------------------



# Define the commande file
# command_file=~/dev/latindd4py/latindd4py/bin/launch_latin.py
command_file=$(cd "$(dirname "$0")" && pwd -P)/launch_latin.py

# Define the directory where to launch the asterxx instance
launch_dir=$work_dir/launch_asterxx_1
i=1
while [ -d "$launch_dir/job_$OMPI_COMM_WORLD_RANK" ];
    do
    i=$((i+1))
    launch_dir=$work_dir/launch_asterxx_$i
done
# Directory for the job
job_dir=$launch_dir/job_$OMPI_COMM_WORLD_RANK

mkdir -p $job_dir

# Copy the command file into the job directory
cp $command_file $job_dir/job.py
cd $job_dir

# Launch asterxx
# The --memory option enables to choose the max RAM that can be used by each process
#xterm -T "Subdomain $OMPI_COMM_WORLD_RANK" -e python -i job.py --memory 10000 $1
python3 job.py --memory 10000 $dataFile
#xterm -geometry "80x24+$((15*$OMPI_COMM_WORLD_RANK))+$((15*$OMPI_COMM_WORLD_RANK))" -hold -T "rank_$OMPI_COMM_WORLD_RANK" -e python job.py --memory 10000 $dataFile

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 16:19:50 2018

@author: oumaziz
"""

import pvsimple
pvsimple.ShowParaviewView()
#### import the simple module from the paraview
from pvsimple import *
#### disable automatic camera reset on 'Show'
pvsimple._DisableFirstRenderCameraReset()

# create a new 'MED Reader'
titi = GetActiveSource()
# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1480, 732]

# create a new 'Calculator'
calculator1 = Calculator(Input=titi)

# Properties modified on calculator1
calculator1.Function = 'depl_DX*iHat+jHat*depl_DY'

# create a new 'Warp By Vector'
warpByVector1 = WarpByVector(Input=calculator1)

# Properties modified on warpByVector1
warpByVector1.ScaleFactor = 10.0

# show data in view
warpByVector1Display = Show(warpByVector1, renderView1)

# trace defaults for the display properties.
warpByVector1Display.Representation = 'Surface'



#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Wed Oct 11 13:50:29 2017

@author: oumaziz
"""
import numpy as np
# try:
#     import code_aster
#     from code_aster.Commands import *
#     code_aster.init()
# except ImportError:
#     print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

from latindd4py import prepare

import sys
import time
from mpi4py import MPI

time0 = time.time()
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
prof = prepare.profileCode()
### Import the parameters and create the class of parameters
param = prepare.readParameter(sys.argv[-1])

try:
    import code_aster
    from code_aster.Commands import *
    code_aster.init()
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

### Import the mesh and data associated to the substructuration
subStructuredMesh = prepare.get_mesh_and_mesh_info(param)

if param.method == 'latin':

    from latindd4py.latin import latin

    ### Creation of the class of the interfaces
    param.logger.info(" Creation of the class of Interface")
    interface = dict()
    interface = prepare.prepareInterface(subStructuredMesh,param)
    param.logger.info(" End of the interfaces's class")

    ### Creation of the class of subdomain
    param.logger.info(" Creation of the class of Subdomain")
    subDomain = prepare.Subdomain(subStructuredMesh,interface,param)
    param.logger.info(" End of the subdomain's class")

    #### Latin computation

    param.logger.info(" Latin computation")
    ddm = latin.Latin(subDomain,interface,param)
    ddm.compute(subDomain,interface,param)

    duree = time.time() - time0
else:

    from latindd4py.schwarz import schwarz

    ### Creation of the class of subdomain
    param.logger.info(" Creation of the class of Subdomain")
    subDomain = prepare.SubdomainOverLap(subStructuredMesh,param)
    param.logger.info(" End of the subdomain's class")

    ### Schwarz computation
    param.logger.info(" Schwarz computation")
    ddm = schwarz.Schwarz(subDomain,param)
    ddm.computeAdditive(subDomain,param)

prof.end("profile",param)
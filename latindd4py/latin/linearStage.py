#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 13:20:42 2017
Linear stage
@author: oumaziz
"""

from latindd4py.aster_functions import *
from latindd4py.aster_functions import solve_mechanics
from scipy import sparse as sp
import numpy as np
import itertools

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class linearStage:
    """
    Class for linear stage
    """
    def __init__(self,field_linear,subDomain,param):
        """
        Initialisation of the linear stage
        Input:
            -field_linear: class value for the fields required at the linear stage
            -intef: class Subdomain
            -param: class of parameters
        """
        param.logger.info("          Linear stage begins")
        self.linearQuasiStaticStage(field_linear,subDomain,param)
        param.logger.info("          Linear stage ends")

#==============================================================================
# Quasistatic linear stage
#==============================================================================
    def linearQuasiStaticStage(self,field_linear,subDomain,param):
        """
        Linear quasistatic stage. Modify the field_linear fields.
        Input:
            -field_linear: class Value for the fields of the linear stage
            -subDomain: class Subdomain
            -param: class of parameters
        """
        for tt in param.listTimeSteps:
            ## Assembly of interface's field on the whole edge of the subdomain
            Fc = assemblyField(subDomain,field_linear.Fc[tt],param)
            for jj in range(len(subDomain.connecInterf)):
                field_linear.Tc[tt][jj] = problemOnSolePython(subDomain.sole[jj],field_linear.Vc[tt][jj],param)
            Tc = assemblyField(subDomain,field_linear.Tc[tt],param)
            Tw = assemblyField(subDomain,field_linear.Tw[tt-1],param)
            ## Solve the problem SD+Sem with (Fc+Tc+Tw) imposed
            param.logger.info("                 Sub-domain resolution: time step {}".format(tt))
            field_linear.W[tt],field_linear.Tw[tt] = subDomainResolutionPython(subDomain,Fc + Tc + Tw,param,pp = tt)
            ## For the multiscale approach
            if param.multiscale == 'YES':
                for jj in range(len(subDomain.connecInterf)):
                    field_linear.T[tt][jj] = problemOnSolePython(subDomain.sole[jj],field_linear.W[tt][jj],param)
                ## Conversion of the micro part into python field
                umrank = field_linear.W[tt]['W'].valField
                ## Resolution of the macro problem
                param.logger.info("              Computation of the macro problem")
                uM, alpha = self.macroComputation(field_linear,subDomain,param,pp=tt)
                ## Computation of the displacement
                u = umrank + uM
                ## Save the sub-domain's displacement in aster field
                field_linear.W[tt]['W'] = pythonField(u,subDomain.soleSD.mesh.NO['N_SD'])
                ## Computation of the intermediate force for the search direction and the computation of F
                Ttilde = subDomain.global_operators.kAtW.dot(alpha)
                for jj in range(len(subDomain.connecInterf)):
                    field_linear.W[tt][jj] = pythonField(subDomain.Operator['N_SD'][jj].dot(u),subDomain.sole[jj].mesh.NO['N'])
                    field_linear.Tw[tt][jj] = problemOnSolePython(subDomain.sole[jj],field_linear.W[tt][jj],param)
                    ## Computation of the velocity
                    field_linear.V[tt][jj] = 1. / param.dt * (field_linear.W[tt][jj] - field_linear.W[tt-1][jj])
                    field_linear.T[tt][jj] = (field_linear.Tw[tt][jj] - field_linear.Tw[tt-1][jj]) / param.dt
                    ## Extraction of the intermediate force distribution on the jj interface
                    T_tilde = pythonField(subDomain.Operator['Ninterf'][jj].dot(Ttilde),subDomain.sole[jj].mesh.NO['N'])
                    field_linear.F[tt][jj] = field_linear.Fc[tt][jj] + field_linear.Tc[tt][jj] - field_linear.T[tt][jj] - T_tilde
            else:
                ## Resolution des problemes annexes et Calcul de F = Fc + Tc - T
                for jj in range(0,len(subDomain.connecInterf)):
                    ## Computation of the velocity
                    field_linear.V[tt][jj] = 1. / param.dt * (field_linear.W[tt][jj] - field_linear.W[tt-1][jj])
                    ## Computation of the intermediate force
                    field_linear.T[tt][jj] = (field_linear.Tw[tt][jj] - field_linear.Tw[tt-1][jj]) / param.dt
                    ## Computation of the force distribution
                    field_linear.F[tt][jj] = field_linear.Fc[tt][jj] + field_linear.Tc[tt][jj] - field_linear.T[tt][jj]



#==============================================================================
# Macro computation
#==============================================================================
    def macroComputation(self,field_linear,subDomain,param,pp = 0):
        """
        Compute the macro part of the displacement and the Lagrange's multiplier
        Input:
            -subDomain: class Subdomain
            -param: class of parameters
            -pp: timestep
        Output:
            -uM: numpy array, macro part of the displacement over the subdomain
            -alpha[pos]: numpy array, multiplier coming from the macro problem
        """
        Tt = dict()
        ### Computation of the right-handed member of the macro problem
        for jj in range(len(subDomain.neighbourSD)):
            Tt[jj] = (field_linear.T[pp][jj] - field_linear.Tc[pp][jj] - field_linear.Tw[pp-1][jj])
        ### Computation of the local contribution of the macro right-hand side
        if param.macroBasis == 'GENEO':
            localFM = subDomain.Operator['AtW']['localSD'].T.dot(np.concatenate([Tt[jj].valField for jj in range(len(subDomain.neighbourSD))]))
            FMg = np.zeros(sum(subDomain.listNumberModes))
            pos = list()
            for jj,SD in enumerate(sorted([rank]+subDomain.neighbourSD)):
                posIni = sum([subDomain.listNumberModes[dd] for dd in range(SD)])
                pos += list(range(posIni,posIni+subDomain.listNumberModes[SD]))
            FMg[pos] = localFM
            FM = comm.allreduce(FMg,op=MPI.SUM)
            ### Computation of the Lagrange's multiplier
            alpha = subDomain.global_operators.luKm.solve(FM)
            ### Computation of the macro part of the displacement
            uM =  - subDomain.global_operators.KNtkAtW.dot(alpha)
        else:
            localFM = sp.csc_matrix(subDomain.Operator['AtW'].T.dot(np.concatenate([Tt[jj].valField for jj in range(len(subDomain.connecInterf))])))
            pos = list(itertools.chain.from_iterable([list(range(sum(subDomain.listNumberModes[:jj]),sum(subDomain.listNumberModes[:jj+1]))) for jj in subDomain.connecInterf]))

            ### Allreduce to get the complete macro right-hand-side
            param.logger.info("                     MPI allreduce:  time step {}".format(pp))
#            FM = sp.csc_matrix((sum(subDomain.listNumberModes),1))
            FM = np.zeros(sum(subDomain.listNumberModes))
            gatherFM = comm.allgather([localFM,pos])

            for jj in range(param.numberSubDomain):
                FM[np.ix_(gatherFM[jj][1])] += gatherFM[jj][0].T.toarray().ravel()
            param.logger.info("                     MPI allreduce END:  time step {}".format(pp))
            ### Computation of the Lagrange's multiplier
    #        alpha = subDomain.global_operators.luKm.solve(FM.toarray()).ravel()
            alpha = subDomain.global_operators.luKm.solve(FM)
            ### Computation of the macro part of the displacement
            uM =  - subDomain.global_operators.KNtkAtW.dot(alpha[np.ix_(pos)])

        return uM, alpha[pos]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 13:29:31 2017
Local stage
@author: oumaziz
"""
import numpy as np
import scipy.sparse as sp
from latindd4py.aster_functions.interface_field import pythonField
from latindd4py.aster_functions.solve_mechanics import problemOnDoubleSolePython,problemOnSolePython


import pdb

def perfectLocalStage(field_local,interface,numInterface,param):
    """
    Resolution of a perfect local stage
    Input:
        -field_local: class value for the fields required at the local stage
        -inteface: class Interface
        -numInterface: number of the interface for the Value class
        -param: class of parameters
    """ 
    param.logger.info("                     Perfect computation")
    for tt in param.listTimeSteps:
        field_local[numInterface].T[tt][0] = problemOnSolePython(interface.sole[0],field_local[numInterface].V[tt][0],param)
        field_local[numInterface].T[tt][1] = problemOnSolePython(interface.sole[1],field_local[numInterface].V[tt][1],param)
        ### Computation of the force to applied on the double sole (-F1-F2+(T1+T2)+(Twc1+Twc2)[t-1])
        Ft = (field_local[numInterface].T[tt][0] + field_local[numInterface].T[tt][1] + field_local[numInterface].Twc[tt-1][0] + field_local[numInterface].Twc[tt-1][1] - field_local[numInterface].F[tt][0] - field_local[numInterface].F[tt][1])
        ### Resolution on the double sole
        [field_local[numInterface].Wc[tt],field_local[numInterface].Twc[tt]] = problemOnDoubleSolePython(interface.doubleSole,interface.sole,Ft,param)
        for jj in range(2):
            ### Computation of the velocity
            field_local[numInterface].Vc[tt][jj] = 1. / param.dt * (field_local[numInterface].Wc[tt][jj] - field_local[numInterface].Wc[tt-1][jj])
            ### Resolution of the intermediate problems
            field_local[numInterface].Tc[tt][jj] = (field_local[numInterface].Twc[tt][jj] - field_local[numInterface].Twc[tt-1][jj]) / param.dt
            ### Computation of the force distribution
            field_local[numInterface].Fc[tt][jj] = field_local[numInterface].F[tt][jj] + field_local[numInterface].Tc[tt][jj] - field_local[numInterface].T[tt][jj]

def contactLocalStage(field_local,interface,numInterface,param):
    """
    Resolution of a contact local stage
    Input:
        -field_local: class value for the fields required at the local stage
        -inteface: class Interface
        -numInterface: number of the interface for the Value class
        -param: class of parameters
    """ 
    param.logger.info("                     Contact computation")
    w = dict()
    wc_n = dict()
    v = dict()
    f = dict()
    W = dict()
    V = dict()
    F = dict()
    Wc = dict()
    Wc_n = dict()
    Vc = dict()
    Fc = dict()
    Tc = dict()

    for tt in param.listTimeSteps:
        for jj in range(2):
            ### Conversion of the aster field into python field and extraction of the value
            w[jj] = field_local[numInterface].W[tt][jj].valField
            wc_n[jj] = field_local[numInterface].Wc[tt-1][jj].valField
            v[jj] = field_local[numInterface].V[tt][jj].valField
            f[jj] = field_local[numInterface].F[tt][jj].valField
            ### Passage into the normal / tangential basis
            W[jj] = interface.Operator['P'].T.dot(w[jj])
            Wc_n[jj] = interface.Operator['P'].T.dot(wc_n[jj])
            V[jj] = interface.Operator['P'].T.dot(v[jj])
            F[jj] = interface.Operator['P'].T.dot(f[jj])
            ### Initialisation of the field of the local stage
            Wc[jj] = np.zeros(param.dim * len(interface.sole[0].mesh.NO['N']))
            Vc[jj] = np.zeros(param.dim * len(interface.sole[0].mesh.NO['N']))
            Fc[jj] = np.zeros(param.dim * len(interface.sole[0].mesh.NO['N']))
        ### Selection of the normal component of the interface distribution
        n = [param.dim * ii for ii in range(len(interface.sole[0].mesh.NO['N']))]
        t = list(set(interface.sole[0].mesh.NO['N']) - set(n))
        ### Computation of the normal indicator Cn
        Cn = (Wc_n[1][n] - Wc_n[0][n] + interface.gap + param.dt * (V[1][n] - V[0][n])
            - param.dt * (1. / interface.sole[1].model.dirRecherche.valField[n] * F[1][n] - 1. / interface.sole[0].model.dirRecherche.valField[n] * F[0][n]))
        if interface.preloadCase == 'YES':
            Cn = -Cn
        ### Extraction of the positive indicator : contactless and the related normal component
        CnContactLess = Cn[Cn>0]
        nContactLess = np.array(n)[Cn>0].tolist()
        #if nContactLess and interface.preloadCase!= 'YES':
        if nContactLess:
            tContactLess = np.sort(np.concatenate([np.array(nContactLess)+dd for dd in range(1,param.dim)])).tolist()
            contactLess = sorted(nContactLess+tContactLess)
            if interface.preloadCase == 'YES':
                for jj in range(0,2):
                    ### Tangential part for the pre-load case
                    Wc[jj][tContactLess] = ((1. / (interface.sole[jj].model.dirRecherche.valField[tContactLess] + interface.sole[1-jj].model.dirRecherche.valField[tContactLess] ))
                        * (- F[jj][tContactLess] - F[1-jj][tContactLess]
                            + param.dt * interface.sole[jj].model.dirRecherche.valField[tContactLess] * V[jj][tContactLess]
                            + param.dt * interface.sole[1-jj].model.dirRecherche.valField[tContactLess] * V[1-jj][tContactLess]
                            + interface.sole[jj].model.dirRecherche.valField[tContactLess] * Wc_n[jj][tContactLess]
                            + interface.sole[1-jj].model.dirRecherche.valField[tContactLess] * Wc_n[1-jj][tContactLess]))
                    Vc[jj][tContactLess] = 1. / param.dt * (Wc[jj][tContactLess] - Wc_n[jj][tContactLess])
                    Fc[jj][tContactLess] = F[jj][tContactLess] + param.dt * interface.sole[jj].model.dirRecherche.valField[tContactLess] * (Vc[jj][tContactLess] - V[jj][tContactLess])
                    ### Normal part for the pre-load case
                    ## Computation of null force distribution
                    Fc[jj][nContactLess] = np.zeros(len(nContactLess))
                    ## Computation of the velocity
                    Vc[jj][nContactLess] = V[jj][nContactLess] - 1. / (param.dt * interface.sole[jj].model.dirRecherche.valField[nContactLess]) * F[jj][nContactLess]
                    ## Computation of the displacement
                    Wc[jj][nContactLess] = Wc_n[jj][nContactLess] + param.dt * Vc[jj][nContactLess]

            else:
                for jj in range(0,2):
                    ### Computation of null force distribution
                    Fc[jj][contactLess] = np.zeros(len(contactLess))
                    ### Computation of the velocity
                    Vc[jj][contactLess] = V[jj][contactLess] - 1. / (param.dt * interface.sole[jj].model.dirRecherche.valField[contactLess]) * F[jj][contactLess]
                    ### Computation of the displacement
                    Wc[jj][contactLess] = Wc_n[jj][contactLess] + param.dt * Vc[jj][contactLess]
        CnContact = Cn[Cn<=0]
        nContact = np.array(n)[Cn<=0].tolist()
        #if nContact or interface.preloadCase == 'YES':
        if nContact:
            tContact = np.sort(np.concatenate([np.array(nContact)+dd for dd in range(1,param.dim)])).tolist()
            if interface.preloadCase == 'YES':
                CnContact = - CnContact
            ### Case of contact
            ## Computation of the normal force
            Fc[0][nContact] = -(- 1. / (1. / (param.dt * interface.sole[0].model.dirRecherche.valField[nContact]) + 1. / (param.dt * interface.sole[1].model.dirRecherche.valField[nContact]))
                                    * (CnContact / param.dt))
            Fc[1][nContact] = - Fc[0][nContact]
            ## Computation of the normal velocity
            Vc[0][nContact] = V[0][nContact] + 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[nContact]) * (Fc[0][nContact] - F[0][nContact])
            Vc[1][nContact] = V[1][nContact] + 1. / (param.dt * interface.sole[1].model.dirRecherche.valField[nContact]) * (Fc[1][nContact] - F[1][nContact])
            ## Computation of the normal displacement
            Wc[0][nContact] = Wc_n[0][nContact] + param.dt * Vc[0][nContact]
            Wc[1][nContact] = Wc_n[1][nContact] + param.dt * Vc[1][nContact]

            if interface.preloadCase == 'YES':
                for jj in range(2):
                    Wc[jj][tContact] = ((1. / (interface.sole[jj].model.dirRecherche.valField[tContact] + interface.sole[1-jj].model.dirRecherche.valField[tContact]))
                        * (- F[jj][tContact] - F[1-jj][tContact]
                            + param.dt * interface.sole[jj].model.dirRecherche.valField[tContact] * V[jj][tContact]
                            + param.dt * interface.sole[1-jj].model.dirRecherche.valField[tContact] * V[1-jj][tContact]
                            + interface.sole[jj].model.dirRecherche.valField[tContact] * Wc_n[jj][tContact]
                            + interface.sole[1-jj].model.dirRecherche.valField[tContact] * Wc_n[1-jj][tContact]))
                    Vc[jj][tContact] = 1. / param.dt * (Wc[jj][tContact] - Wc_n[jj][tContact])
                    Fc[jj][tContact] = F[jj][tContact] + param.dt * interface.sole[jj].model.dirRecherche.valField[tContact] * (Vc[jj][tContact] - V[jj][tContact])
            else:
                if interface.frictionCase == 'YES':
                    Gt = (F[0][tContact]
                                - param.dt * interface.sole[0].model.dirRecherche.valField[tContact]
                                * 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tContact] + param.dt * interface.sole[1].model.dirRecherche.valField[tContact])
                                * (F[0][tContact] + F[1][tContact])
                                - (param.dt * interface.sole[0].model.dirRecherche.valField[tContact] * V[0][tContact]
                                    - param.dt * interface.sole[0].model.dirRecherche.valField[tContact]
                                    * 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tContact]
                                            + param.dt * interface.sole[1].model.dirRecherche.valField[tContact])
                                    * (param.dt * interface.sole[0].model.dirRecherche.valField[tContact] * V[0][tContact]
                                        + param.dt * interface.sole[1].model.dirRecherche.valField[tContact] * V[1][tContact])))
                    Gtp = (F[1][tContact]
                                - param.dt * interface.sole[0].model.dirRecherche.valField[tContact]
                                * 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tContact] + param.dt * interface.sole[1].model.dirRecherche.valField[tContact])
                                * (F[0][tContact] + F[1][tContact])
                                - (param.dt * interface.sole[0].model.dirRecherche.valField[tContact] * V[1][tContact]
                                    - param.dt * interface.sole[0].model.dirRecherche.valField[tContact]
                                    * 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tContact]
                                            + param.dt * interface.sole[1].model.dirRecherche.valField[tContact])
                                    * (param.dt * interface.sole[0].model.dirRecherche.valField[tContact] * V[0][tContact]
                                        + param.dt * interface.sole[1].model.dirRecherche.valField[tContact] * V[1][tContact])))
                    normGt = np.array([np.linalg.norm(Gt[(param.dim-1)*jj:(param.dim-1)*jj+param.dim-1]) for jj in range(len(nContact))])
                    normGtp = np.array([np.linalg.norm(Gtp[(param.dim-1)*jj:(param.dim-1)*jj+param.dim-1] - Gt[(param.dim-1)*jj:(param.dim-1)*jj+param.dim-1]) for jj in range(len(nContact))])
                    ### Extraction of the sticking indicator and the related components
                    nSticking = np.array(nContact)[normGt <= interface.friction * abs(Fc[0][nContact])].tolist()
                    if nSticking:
                        GtSticking = Gt[np.sort(np.concatenate([np.array(list(range(len(nContact))))[normGt <= interface.friction * abs(Fc[0][nContact])] * (param.dim-1)+dd for dd in range(param.dim-1)])).tolist()]
#                                    saveNumpyTxt(np.sort(np.concatenate([np.array(range(len(nContact)))[normGt <= interface.friction * abs(Fc[0][nContact])] * (param.dim-1)+dd for dd in range(param.dim-1)])).tolist(),"indiceGt{}".format(tt),param.resDir)
                        tSticking = np.sort(np.concatenate([np.array(nSticking)+dd for dd in range(1,param.dim)])).tolist()
                        ### Computation of the tangential force
                        Fc[0][tSticking] = GtSticking
                        Fc[1][tSticking] = - Fc[0][tSticking]
                        ### Computation of the tangential velocity
                        Vc[0][tSticking] = (1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tSticking] + param.dt * interface.sole[1].model.dirRecherche.valField[tSticking]) *
                                            (param.dt * interface.sole[0].model.dirRecherche.valField[tSticking] * V[0][tSticking] + param.dt * interface.sole[1].model.dirRecherche.valField[tSticking] * V[1][tSticking]
                                            - (F[0][tSticking] + F[1][tSticking])))
                        #Vc[0][tSticking] = V[0][tSticking] + 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tSticking]) * (Fc[0][tSticking] - F[0][tSticking])
                        Vc[1][tSticking] = Vc[0][tSticking]

                        ### Computation of the tangential displacement
                        Wc[0][tSticking] = Wc_n[0][tSticking] + param.dt * Vc[0][tSticking]
                        Wc[1][tSticking] = Wc_n[1][tSticking] + param.dt * Vc[1][tSticking]
                    ### Extraction of the sliding indicator and the related components
                    nSliding = np.array(nContact)[normGt > interface.friction * abs(Fc[0][nContact])].tolist()
                    if nSliding:
                        GtSliding = Gt[np.sort(np.concatenate([np.array(list(range(len(nContact))))[normGt > interface.friction * abs(Fc[0][nContact])] * (param.dim-1)+dd for dd in range(param.dim-1)])).tolist()]
                        GtpSliding = Gtp[np.sort(np.concatenate([np.array(list(range(len(nContact))))[normGt > interface.friction * abs(Fc[0][nContact])] * (param.dim-1)+dd for dd in range(param.dim-1)])).tolist()]
                        tSliding = np.sort(np.concatenate([np.array(nSliding)+dd for dd in range(1,param.dim)])).tolist()
                        ### Computation of the tangential force
                        if param.dim == 3:
                            Fc[0][tSliding] = interface.friction * np.column_stack((abs(Fc[0][nSliding]),abs(Fc[0][nSliding]))).ravel() * GtSliding / np.column_stack((normGt[normGt > interface.friction * abs(Fc[0][nContact])],normGt[normGt > interface.friction * abs(Fc[0][nContact])])).ravel()
                        else:
                            Fc[0][tSliding] = interface.friction * abs(Fc[0][nSliding]) * GtSliding / normGt[normGt > interface.friction * abs(Fc[0][nContact])]
                        Fc[1][tSliding] = - Fc[0][tSliding]
                        ### Computation of the tangential velocity
                        Vc[0][tSliding] = V[0][tSliding] + 1. / (param.dt * interface.sole[0].model.dirRecherche.valField[tSliding]) * (Fc[0][tSliding] - F[0][tSliding])
                        Vc[1][tSliding] = V[1][tSliding] + 1. / (param.dt * interface.sole[1].model.dirRecherche.valField[tSliding]) * (Fc[1][tSliding] - F[1][tSliding])
                        ### Computation of the tangential displacement
                        Wc[0][tSliding] = Wc_n[0][tSliding] + param.dt * Vc[0][tSliding]
                        Wc[1][tSliding] = Wc_n[1][tSliding] + param.dt * Vc[1][tSliding]
                else:
                    for jj in range(0,2):
                        ### Computation of null force distribution
                        Fc[jj][tContact] = np.zeros(len(tContact))
                        ### Computation of the velocity
                        Vc[jj][tContact] = V[jj][tContact] - 1. / (param.dt * interface.sole[jj].model.dirRecherche.valField[tContact]) * F[jj][tContact]
                        ### Computation of the displacement
                        Wc[jj][tContact] = Wc_n[jj][tContact] + param.dt * Vc[jj][tContact]

        for jj in range(2):
            field_local[numInterface].Wc[tt][jj] = pythonField(interface.Operator['P'].dot(Wc[jj]),interface.sole[jj].mesh.NO['N'])
            field_local[numInterface].Vc[tt][jj] = pythonField(interface.Operator['P'].dot(Vc[jj]),interface.sole[jj].mesh.NO['N'])
            field_local[numInterface].Fc[tt][jj] = pythonField(interface.Operator['P'].dot(Fc[jj]),interface.sole[jj].mesh.NO['N'])
            ### Computation of the intermediate force for the linear stage
            field_local[numInterface].Tc[tt][jj] = param.dt * problemOnSolePython(interface.sole[jj],field_local[numInterface].Vc[tt][jj],param)

def cohesiveLocalStage(field_local,interface,numInterface,param):
    """
    Resolution of a contact local stage
    Input:
        -field_local: class value for the fields required at the local stage
        -inteface: class Interface
        -numInterface: number of the interface for the Value class
        -param: class of parameters
    """
    param.logger.info("                     Cohesive computation")
    w = dict()
    wc_n = dict()
    v = dict()
    vc_n = dict()
    f = dict()
    W = dict()
    V = dict()
    F = dict()
    Wc = dict()
    Wc_n = dict()
    Vc = dict()
    Vc_n = dict()
    Fc = dict()
    
    for tt in param.listTimeSteps:
        for jj in range(2):
            ### Conversion of the aster field into python field and extraction of the value
            w[jj] = field_local[numInterface].W[tt][jj].valField
            wc_n[jj] = field_local[numInterface].Wc[tt-1][jj].valField
            v[jj] = field_local[numInterface].V[tt][jj].valField
            vc_n[jj] = field_local[numInterface].Vc[tt-1][jj].valField
            f[jj] = field_local[numInterface].F[tt][jj].valField
            ### Passage into the normal / tangential basis
            W[jj] = interface.Operator['P'].T.dot(w[jj])
            Wc_n[jj] = interface.Operator['P'].T.dot(wc_n[jj])
            V[jj] = interface.Operator['P'].T.dot(v[jj])
            Vc_n[jj] = interface.Operator['P'].T.dot(vc_n[jj])
            F[jj] = interface.Operator['P'].T.dot(f[jj])
        ### Selection of the normal component of the interface distribution
        n = np.array([param.dim * ii for ii in range(len(interface.sole[0].mesh.NO['N']))])
        ### Selection of the tangential component of the interface distrubution
        t1 = np.array([param.dim * ii + 1 for ii in range(len(interface.sole[0].mesh.NO['N']))])
        t = [t1]
        if param.dim == 3 :
            ### In case of a 3D problem
            # t1 = [param.dim * ii + 1 for ii in range(len(interface.sole[0].mesh.NO['N']))]
            t2 = [param.dim * ii + 2 for ii in range(len(interface.sole[0].mesh.NO['N']))]
            t.append(t2)
        ### Solve the local stage
        [Wc,Vc,Fc,damage] = Solvers.get(interface.solver,None)(W,Wc_n,V,F,t,n,interface,param)
        interface.damage[tt] = damage
        for jj in range(2):
            field_local[numInterface].Wc[tt][jj] = pythonField(interface.Operator['P'].dot(Wc[jj]),interface.sole[jj].mesh.NO['N'])
            field_local[numInterface].Vc[tt][jj] = pythonField(interface.Operator['P'].dot(Vc[jj]),interface.sole[jj].mesh.NO['N'])
            field_local[numInterface].Fc[tt][jj] = pythonField(interface.Operator['P'].dot(Fc[jj]),interface.sole[jj].mesh.NO['N'])
            ### Computation of the intermediate force for the linear stage
            field_local[numInterface].Tc[tt][jj] = param.dt * problemOnSolePython(interface.sole[jj],field_local[numInterface].Vc[tt][jj],param)

def newton(W,Wc_n,V,F,t,n,interface,param):
    """
    Newton solver to solve the non linear problem of the linear stage
    Input:
        -gap: gap at the interface
        -t: tangential component of the interface fields
        -n: normal component of thr interface fields
        -interface: class Interface 
        -param: class of Parameters
    """
    Wc = dict()
    Vc = dict()
    Fc = dict()
    ### Newton method to solve the non linear system
    inew = 0
    critere = 0.000000001
    nodesNewton = list(range(len(interface.sole[0].mesh.NO['N'])))

    ### Computation of the displacement gap
    Saut = W[1] - W[0]
    ### Computation of the right hand side of the non linear system
    rhs = -(1./interface.sole[1].model.dirRecherche.valField*F[1] - 1./interface.sole[0].model.dirRecherche.valField*F[0]) + 1./param.dt*(Wc_n[1]-Wc_n[0]) + (V[1]-V[0])
    ### Computation of the rest of the Newton method
    R = np.ones(len(interface.sole[0].mesh.NO['N']))
    while inew < 500 and len(nodesNewton) > 0 :
        [one,damage] = CohesiveLaw.get(interface.cohesiveLaw,None)(Saut,t,n,interface,param)
        ### Computation of the stiffness
        K_diag = np.reshape((one - damage)*interface.initial_stiffness,(damage.shape[0],1))
        K = sp.diags(np.concatenate([K_diag for ii in range(param.dim)],axis=1).ravel())
        ### Weighting of the stiffness matrix with the mass matrix
        K = K.dot(sp.diags(interface.mass.diagonal()[[param.dim * ii + jj for ii in nodesNewton for jj in range(param.dim)]]))
        K_W = sp.diags(1./interface.sole[1].model.dirRecherche.valField + 1./interface.sole[0].model.dirRecherche.valField).dot(K) + 1./param.dt*sp.eye(K.shape[0])
        ### Computation of the gap displacement
        dwc = sp.linalg.spsolve(K_W,rhs - K_W.dot(Saut))
        Saut[[param.dim * ii + jj for ii in nodesNewton for jj in range(param.dim)]] += dwc
        ### Computation of the rest of the Newton method
        rr = rhs - K_W.dot(Saut[[param.dim * ii + jj for ii in nodesNewton for jj in range(param.dim)]])
        for nodes in range(len(nodesNewton)):
            R[nodes] = np.linalg.norm(rr[[param.dim*nodes,param.dim*nodes+1]]) / max(np.linalg.norm(rhs[[param.dim*nodes,param.dim*nodes+1]]),0.00001)
        ### Select the nodes to keep going with the Newton
        nodesNewton = np.where(R>critere)[0]
        inew += 1
    ### Computation of the force (Allix modele)
    Fc[0] = K.dot(Saut)
    Fc[1] = -K.dot(Saut)
    for jj in range(2):
        ### Computation of the velocity
        Vc[jj] = V[jj] + 1./interface.sole[jj].model.dirRecherche.valField * (Fc[jj] - F[jj])
        ### Computation of the displacement
        Wc[jj] = param.dt * Vc[jj] + Wc_n[jj]
    return Wc,Vc,Fc,damage

def infinite(W,Wc_n,V,F,t,n,interface,param):
    """
    infinite search direction to solve the non linear problem of the linear stage
    Input:
        -gap: gap at the interface
        -t: tangential component of the interface fields
        -n: normal component of thr interface fields
        -interface: class Interface 
        -param: class of Parameters
    """
    Wc = dict()
    Vc = dict()
    Fc = dict()
    nodes= list(range(len(interface.sole[0].mesh.NO['N'])))
    ### Computation of the displacement gap
    Saut = W[1] - W[0]
    [one,damage] = CohesiveLaw.get(interface.cohesiveLaw,None)(Saut,t,n,interface,param)
    ### Computation of the stiffness
    K_diag = np.reshape((one - damage)*interface.initial_stiffness,(damage.shape[0],1))
    K = sp.diags(np.concatenate([K_diag for ii in range(param.dim)],axis=1).ravel())
    ### Weighting of the stiffness matrix with the mass matrix
    K = K.dot(sp.diags(interface.mass.diagonal()[[param.dim * ii + jj for ii in nodes for jj in range(param.dim)]]))
    ### Computation of the force
    Fc[0] = K.dot(Saut)
    Fc[1] = -K.dot(Saut)
    for jj in range(2):
        ### Computation of the displacement
        Wc[jj] = W[jj]
        ### Computation of the velocity
        Vc[jj] = V[jj]
    return Wc,Vc,Fc,damage

def lawAllix(gap,t,n,interface,param):
    """
    Compute the damage behaviour with the Allix model
    Input:
        -gap: gap at the interface
        -t: tangential component of the interface fields
        -n: normal component of thr interface fields
        -interface: class Interface 
        -param: class of Parameters
    Output:
        -damage
    """
    t1 = t[0]
    if param.dim == 3:
        t2 = t[2]
    ### Computation of thermodynamic forces
    Y1 = 1./2.*interface.initial_stiffness*(gap[t1])**2
    if param.dim == 3 :
        Y2 = 1./2.*interface.initial_stiffness*(gap[t2])**2
    else:
        Y2 = np.zeros(len(interface.sole[0].mesh.NO['N']))
    Y3 = 1./2.*interface.initial_stiffness*((gap[n]+np.abs(gap[n]))/2)**2
    Yn = (Y3**interface.alpha+(interface.gamma1*Y1)**interface.alpha+(interface.gamma2*Y2)**interface.alpha)**(1/interface.alpha)
    ### Computation of the damage variable
    one = np.ones(np.size(Yn))
    damage=np.minimum(((interface.fragility/(interface.fragility+1))*(Yn/interface.Y_critique))**interface.fragility,one)
    return one,damage

Localstage = {'PERFECT' : perfectLocalStage,
              'CONTACT' : contactLocalStage,
              'COHESIVE' : cohesiveLocalStage,
             }

CohesiveLaw = {'Allix' : lawAllix}

Solvers = {'infinite' : infinite,
           'newton' : newton}



class localStage:
    """
    Class for the local stage
    """
    def __init__(self,field_local,interf,param):
        """
        Initialization of the local stage
        Input:
            -field_local: class value for the fields required at the local stage
            -intef: dictionnary of class Interface
            -param: class of parameters
        """
        param.logger.info("          Local stage begins")
        self.localStageQuasiStatic(field_local,interf,param)
        param.logger.info("          Local stage ends")

#==============================================================================
# Local stage for quasi-static study
#==============================================================================
    def localStageQuasiStatic(self,field_local,interf,param):
        """
        Local stage for quasi-static
        Input:
            -field_local: class value for the fields required at the local stage
            -intef: dictionnary of class Interface
            -param: class of parameters
        """
        for numInterface in interf:
            param.logger.info("                 Interface {}".format(numInterface))
            Localstage.get(interf[numInterface].Type,None)(field_local,interf[numInterface],numInterface,param)




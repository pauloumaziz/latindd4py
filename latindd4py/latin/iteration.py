#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 12:34:13 2017
Iteration function
@author: oumaziz
"""

from .localStage import localStage
from .linearStage import linearStage
from .transfer_functions import local2linear

def iteration(field_local,field_linear,interface,subDomain,param):
    """
    Function to compute one iteration of the Latin method
    Input:
        -field_local: fields for the local stage
        -field_linear: fields for the linear stage
        -inerface: dictionnary of class Interface
        -subDomain: class Subdomain
        -param: class of parameters
    """
    ### Local stage
    loc = localStage(field_local,interface,param)
    ### Communication from local stage to linear stage
    param.logger.info("          - - - - - - - - - - - - - - - - - - - -")
    param.logger.info("          Transfer from local to linear : NO MPI")
    local2linear(field_local,field_linear,interface,param)
    param.logger.info("          Transfer ends")
    param.logger.info("          - - - - - - - - - - - - - - - - - - - -")
    ### Linear stage
    lin = linearStage(field_linear,subDomain,param)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  3 15:19:36 2017
Class to compute the error indicator
@author: oumaziz
"""
from latindd4py.aster_functions.solve_mechanics import problemOnSole,problemOnSolePython
from mpi4py import MPI
import numpy as np
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class Error():
    """
    Class for the error
    """
    def __init__(self,param):
        """
        Initialisation of the class
        """
        self.quasistatic = param.quasistatic
        if self.quasistatic == 'YES':
            self.numberTimeStep = param.numberTimeStep

    def compute(self,field_local,interface,param):
        """
        Compute the error indicator
        Input:
            -field_local: dictionnnary of interface fields
            -interface: dictionnary of classes Interface
            -logger: logger to print message in log file
        """

        self.error = self.quasiStaticIndicator(field_local,interface,param)

    def staticIndicator(self,field_local,interface,param):
        """
        Compute and return the error indicator under a static assumption.
        Input:
            -field_local: dictionnnary of interface fields
            -interface: dictionnary of classes Interface
            -logger: logger to print message in log file
        """
        num = 0
        den = 0
        numW = 0
        numF = 0
        denW = 0
        denF = 0

        param.logger.info("               Computation local contribution")
        for kk,interf in enumerate(interface.values()):
            Wt = problemOnSole(interf.sole[interf.connecSD.index(rank)],field_local[kk].F[interf.connecSD.index(rank)],param,case='force').valField
            Wtc = problemOnSole(interf.sole[interf.connecSD.index(rank)],field_local[kk].Fc[interf.connecSD.index(rank)],param,case='force').valField

            F = field_local[kk].F[interf.connecSD.index(rank)].valField
            T = field_local[kk].T[interf.connecSD.index(rank)].valField
            W = field_local[kk].W[interf.connecSD.index(rank)].valField
            Fc = field_local[kk].Fc[interf.connecSD.index(rank)].valField
            Tc = field_local[kk].Tc[interf.connecSD.index(rank)].valField
            Wc = field_local[kk].Wc[interf.connecSD.index(rank)].valField

            num += ((F - Fc).dot(Wt - Wtc) + (W - Wc).dot(T - Tc))
            den += ((F+Fc).dot(Wt+Wtc)+(W+Wc).dot(T+Tc))

            numW += (W-Wc).dot(W-Wc)
            numF += (F-Fc).dot(F-Fc)

            denW += (W+Wc).dot(W+Wc)
            denF += (F+Fc).dot(F+Fc)

        param.logger.info("               End of computation local contribution")

        param.logger.info("               MPI all reduce ")
        NUM = comm.allreduce(num,op=MPI.SUM)
        DEN = comm.allreduce(den,op=MPI.SUM)
        param.logger.info("               End of MPI all reduce")
        if DEN == 0.:
            if NUM == 0.:
                return 0.
            else:
                return 1.
        else:
            return NUM / DEN

    def quasiStaticIndicator(self,field_local,interface,param):
        """
        Compute and return the error indicator under a quasistatic assumption.
        Input:
            -field_local: dictionnnary of interface fields
            -interface: dictionnary of classes Interface
            -logger: logger to print message in log file
        Output:
            -float number for the error
        """
        num = 0
        den = 0
        num = np.zeros(param.numberTimeStep+1)
        den = np.zeros(param.numberTimeStep+1)
        numW = 0
        numF = 0
        denW = 0
        denF = 0
        param.logger.info("               Computation local contribution")
        for tt in param.listTimeSteps:
            for kk,interf in enumerate(interface.values()):
                Vt = problemOnSolePython(interf.sole[interf.connecSD.index(rank)],field_local[kk].F[tt][interf.connecSD.index(rank)],param,case='force').valField
                Vtc = problemOnSolePython(interf.sole[interf.connecSD.index(rank)],field_local[kk].Fc[tt][interf.connecSD.index(rank)],param,case='force').valField

                field_local[kk].T[tt][interf.connecSD.index(rank)] = problemOnSolePython(interf.sole[interf.connecSD.index(rank)],field_local[kk].V[tt][interf.connecSD.index(rank)],param)

                F = field_local[kk].F[tt][interf.connecSD.index(rank)].valField
                T = field_local[kk].T[tt][interf.connecSD.index(rank)].valField
                V = field_local[kk].V[tt][interf.connecSD.index(rank)].valField
                Fc = field_local[kk].Fc[tt][interf.connecSD.index(rank)].valField
                Tc = field_local[kk].Tc[tt][interf.connecSD.index(rank)].valField
                Vc = field_local[kk].Vc[tt][interf.connecSD.index(rank)].valField

                num[tt] += ((F - Fc).dot(Vt - Vtc) + (V - Vc).dot(T - Tc))
                den[tt] += ((F+Fc).dot(Vt+Vtc)+(V+Vc).dot(T+Tc))

                numW += (V-Vc).dot(V-Vc)
                numF += (F-Fc).dot(F-Fc)

                denW += (V+Vc).dot(V+Vc)
                denF += (F+Fc).dot(F+Fc)

        param.logger.info("               End of computation local contribution")

        param.logger.info("               MPI all reduce ")
        NUM = comm.allreduce(num,op=MPI.SUM)
        DEN = comm.allreduce(den,op=MPI.SUM)
        param.logger.info("               End of MPI all reduce")
        if param.incremental:
            if np.sum(DEN) == 0.:
                den = 1.
            else:
                den = np.sum(DEN)
        else:
            den = np.sum(DEN)
        globalError = np.sum(NUM) / den
        DEN[np.where(DEN==0.)] = 1.
        
        return globalError, NUM / DEN

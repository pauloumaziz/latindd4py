#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 11:28:46 2017
Transfer functions between interfaces and subdomains
@author: oumaziz
"""
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

#==============================================================================
# From local stage to linear stage
#==============================================================================
def local2linear(field_local,field_linear,interface,param):
    """
    Modify the field_linear to include the interface field compute during the
    local stage/
    Input:
        -field_local: local fields
        -field_linear: linear fields
        -interface: dictionnary of class of Interface
        -param: class of parameters
    """
    for kk in range(len(list(interface.keys()))):
        for tt in param.listTimeSteps:
            field_linear.Wc[tt][kk] = field_local[kk].Wc[tt][interface[kk].connecSD.index(rank)]
            field_linear.Vc[tt][kk] = field_local[kk].Vc[tt][interface[kk].connecSD.index(rank)]
            field_linear.Tc[tt][kk] = field_local[kk].Tc[tt][interface[kk].connecSD.index(rank)]
            field_linear.Fc[tt][kk] = field_local[kk].Fc[tt][interface[kk].connecSD.index(rank)]

#==============================================================================
# From linear stage to local stage
#==============================================================================
def linear2local(field_linear,field_local,subDomain,interf,param):
    """
    Communication from linear to local stage
    Input:
        -field_linear: linear fields
        -field_local: local fields
        -subDomain: dictionnary of class of Subdomain
        -inter: dictionnary of class of Interface
        -param: class of parameters
    """
    for kk,SD in enumerate(subDomain.neighbourSD):
        for tt in param.listTimeSteps:
            ### Send the fields
            param.logger.info("          Send and Recieve W,T,F to "+str(SD))

            listData = comm.sendrecv([field_linear.W[tt][kk],
                                      field_linear.V[tt][kk],
                                      field_linear.T[tt][kk],
                                      field_linear.F[tt][kk]],
                                     dest = SD,
                                     sendtag = rank,
                                     recvtag=SD
                                     )

            remapFields(listData,SD,subDomain,param)

            field_local[kk].W[tt][interf[kk].connecSD.index(SD)] = listData[0]
            field_local[kk].V[tt][interf[kk].connecSD.index(SD)] = listData[1]
            field_local[kk].T[tt][interf[kk].connecSD.index(SD)] = listData[2]
            field_local[kk].F[tt][interf[kk].connecSD.index(SD)] = listData[3]

            field_local[kk].W[tt][1-interf[kk].connecSD.index(SD)] = field_linear.W[tt][kk]
            field_local[kk].V[tt][1-interf[kk].connecSD.index(SD)] = field_linear.V[tt][kk]
            field_local[kk].T[tt][1-interf[kk].connecSD.index(SD)] = field_linear.T[tt][kk]
            field_local[kk].F[tt][1-interf[kk].connecSD.index(SD)] = field_linear.F[tt][kk]

#==============================================================================
# Remap the interface fields
#==============================================================================
def remapFields(listData,numSD,subDomain,param):
    """
    Remap the fields received from MPI transfer to be adapted to the
    numbering of the mesh
    Input:
        -listData: list of fields to remap
        -numSD: number of a neighbour subdomain
        -subDomain: class Subdomain
        -param: class of parameters
    """
    for jj,field in enumerate(listData):
        field.nodes = subDomain.sole[subDomain.neighbourSD.index(numSD)].mesh.NO['N']
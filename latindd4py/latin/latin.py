#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 10:20:58 2017
Class Latin
@author: oumaziz
"""
import numpy as np
import shelve
from .localStage import localStage
from .linearStage import linearStage
from .iteration import iteration
from latindd4py.latin.transfer_functions import local2linear, linear2local
from .error import Error
from latindd4py.aster_functions.save_data import exportResults2MED, saveTxt
from .relaxation import Relaxation
import pdb
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
import pdb
class Latin:
    """
    Class latin
    """
    def __init__(self,subDomain,interface,param):
        """
        Initialisation of the class
        Input:
            -subDomain: class of the subdomain
            -param: class of parameters
        """
        self.error_criterion = param.error
        self.maxIteration = param.maxIteration
        self.err = 1
        self.iteration = 0
        self.field_local = dict(list(zip(list(range(len(subDomain.connecInterf))),[interface[ii].Value for ii in list(interface.keys())])))
        self.field_linear = subDomain.Value.copy()
        self.error = Error(param)
        self.relaxation = Relaxation(param)
        self.iterParam = 0

    def compute(self,subDomain,interface,param):
        """
        Launch the computation of the Latin method.
        Input:
            -subDomain: class Subdomain
            -interface: dictionnary of class Interface
            -param: class of parameters
        """
        for incr in param.iterIncremental:
            param.logger.info(" ------------------------------------------------")
            param.logger.info(" Increment {}".format(incr))
            param.logger.info(" ------------------------------------------------")
            param.iteration = 0
            self.iteration = 0
            self.err = 1
            while self.err > self.error_criterion and self.iteration < self.maxIteration:
                param.logger.info(" ### --------------------------------------------")
                param.logger.info(" ### Beginning of iteration {}".format(self.iteration))
                param.logger.info(" ### --------------------------------------------")
                ### Save fields for the relaxation
                if self.relaxation.relaxation_type == 'ORTHOMIN':
                    field_hist = self.relaxation.saveFieldRelax(subDomain.Value,param)
                else:
                    field_hist = self.relaxation.saveFieldRelax(self.field_linear,param)
                ### Iteration
                iteration(self.field_local,self.field_linear,interface,subDomain,param)
                ### Relaxation step before communication
                param.logger.info("                 Relaxation:")
                self.field_linear = self.relaxation.compute(self.field_linear,field_hist,subDomain,self.iteration)
                ### Communication from linear stage to local stage
                param.logger.info("          - - - - - - - - - - - - - - - - - - - -")
                param.logger.info("          Transfer from linear to local : MPI")
                linear2local(self.field_linear,self.field_local,subDomain,interface,param)
                param.logger.info("          Transfer ends")
                param.logger.info("          - - - - - - - - - - - - - - - - - - - -")
                ### Computation of the error
                param.logger.info("          Computation of the error")
                if self.relaxation.relaxation_type in ('CLASSICAL','AITKEN'):
                    self.error.compute(self.field_local,interface,param)
                    param.logger.info("          Error : {}".format(self.error.error[0]))
                    param.logger.info("          Error computed")
                    param.logger.info("          - - - - - - - - - - - - - - - - - - - -")
                    saveTxt(self.error.error[0],param.resDir+"latin_error.txt")
                    for tt in param.listTimeSteps:
                        saveTxt(self.error.error[1][tt],param.resDir+"latin_error_{}.txt".format(tt))
                    saveTxt(self.relaxation.normResiSum,param.resDir+"normResiSum.txt")
                    saveTxt(self.relaxation.normResiIni,param.resDir+"normResiIni.txt")
                    saveTxt(self.relaxation.resi,param.resDir+"normResi.txt")
                    if param.errorTimeStep:
                        for tt in param.listTimeSteps:
                            saveTxt(1,param.resDir+'test_{}.txt'.format(tt))
                            if self.error.error[1][tt] < self.error_criterion:
                                param.listTimeSteps.remove(tt)
                                self.field_linear.listTimeSteps = param.listTimeSteps
                                for jj,interf in enumerate(interface):
                                    self.field_local[jj].listTimeSteps = param.listTimeSteps
                            if not(param.listTimeSteps):
                                break
                    self.err = self.error.error[0]
                else:
                    saveTxt(self.relaxation.normResi,param.resDir+"orthomin_error.txt")
                    param.logger.info("          Error : {}".format(self.relaxation.normResi))
                    param.logger.info("          Error computed")
                    param.logger.info("          - - - - - - - - - - - - - - - - - - - -")
                    self.err = self.relaxation.normResi
                if param.saveField == "YES":
                    self.saveField(subDomain,param)
                if param.resumption:
                    resumption = shelve.open(param.resDir+"Resumption/Resumption_{}/resumption_{}".format(rank,rank))
                    resumption['linear'] = self.field_linear
                    resumption['local'] = self.field_local
                    # if param.multiscale == 'YES':
                    #     resumption['macroOpe'] = subDomain.global_operators
                    resumption.close()
                if param.multiparam['choice'] == 'YES':
                    if self.error.error < self.error_criterion and self.iterParam < param.multiparam['numCase']-1:
                        self.error.error = 1.
                        self.iterParam += 1
                        self.modifMultiParam(self.iterParam,subDomain,interface,param)
                        exportResults2MED(subDomain.Value,subDomain,param.resDir+"MED/iteration{}".format(self.iteration),param)
                        self.iteration += 1
                        self.maxIteration += self.iteration
                    else:
                        self.iteration += 1
                else:
                    self.iteration +=1
                    param.iteration += 1
            if self.relaxation.relaxation_type == 'ORTHOMIN':
                self.field_linear = subDomain.Value
            if param.incremental:
                self.iteration = 0
            exportResults2MED(self.field_linear,subDomain,param.resDir+"MED/iteration{}".format(self.iteration),param)
            self.updateListTimeSteps(param)


    def modifMultiParam(iterParam,subDomain,interface,param):
        """
        Modification of parameters for the multi-parametric study. It is
        necessary to write this function specificly for each case.
        Input:
            -iterParam: number of the parameter iteration
            -subDomain: class Subdomain
            -interface: dictionnary of class Interface
            -param: class of parameters
        """
        mu1 = np.column_stack([np.array(param.multiparam['parameter'][0]) for ii in range(len(param.multiparam['parameter'][1]))]).ravel()
        mu2 = np.concatenate([np.array(param.multiparam['parameter'][1]) for ii in range(len(param.multiparam['parameter'][0]))])
        if rank == 0:
            interface[0].friction = mu1[iterParam]
        elif rank == 1:
            interface[0].friction = mu1[iterParam]
            interface[1].friction = mu2[iterParam]
        elif rank == 2:
            interface[0].friction = mu2[iterParam]


    def saveField(self,subDomain,param):
        """
        Function to save the fields in text files
        Input:
            -subDomain: class Subdomain
            -param: class of Parameters
        """
        for tt in range(param.numberTimeStep):
            for jj,neighbour in enumerate(subDomain.neighbourSD):
                self.field_linear.W[tt][jj].saveTxt(param.resDir+"Fields/Fields_{}/W{}_{}".format(rank,rank,neighbour))
                self.field_linear.T[tt][jj].saveTxt(param.resDir+"Fields/Fields_{}/T{}_{}".format(rank,rank,neighbour))
                self.field_linear.F[tt][jj].saveTxt(param.resDir+"Fields/Fields_{}/F{}_{}".format(rank,rank,neighbour))
                self.field_linear.Wc[tt][jj].saveTxt(param.resDir+"Fields/Fields_{}/Wc{}_{}".format(rank,rank,neighbour))
                self.field_linear.Tc[tt][jj].saveTxt(param.resDir+"Fields/Fields_{}/Tc{}_{}".format(rank,rank,neighbour))
                self.field_linear.Fc[tt][jj].saveTxt(param.resDir+"Fields/Fields_{}/Fc{}_{}".format(rank,rank,neighbour))

    def updateListTimeSteps(self,param):
        """
        Function to update the list of timesteps in case of incremental Latin
        Input:
            -param: class of parameters
        """
        if param.listTimeSteps[-1] < param.numberTimeStep - 1:
            param.listTimeSteps.append(param.listTimeSteps[-1]+1)
            timePrevious = param.listTimeSteps.pop(0)
            self.field_linear.listTimeSteps = [param.listTimeSteps[0]-1]+param.listTimeSteps

            for tt in self.field_linear.listTimeSteps[1:]:
                for jj in list(self.field_linear.F[tt].keys()):
                    self.field_linear.W[tt][jj] = self.field_linear.W[timePrevious][jj]
                    self.field_linear.V[tt][jj] = self.field_linear.V[timePrevious][jj]
                    self.field_linear.T[tt][jj] = self.field_linear.T[timePrevious][jj]
                    self.field_linear.Tw[tt][jj] = self.field_linear.Tw[timePrevious][jj]
                    self.field_linear.F[tt][jj] = self.field_linear.F[timePrevious][jj]
                    for pp in range(2):
                        self.field_local[jj].W[tt][pp] = self.field_local[jj].W[timePrevious][pp]
                        self.field_local[jj].V[tt][pp] = self.field_local[jj].V[timePrevious][pp]
                        self.field_local[jj].T[tt][pp] = self.field_local[jj].T[timePrevious][pp]
                        self.field_local[jj].F[tt][pp] = self.field_local[jj].F[timePrevious][pp]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 17:19:05 2019

@author: oumaziz
"""
try:
    import code_aster
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

def threeD():
    return {'aster':[code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy,code_aster.PhysicalQuantityComponent.Dz],
            'python':['DX','DY','DZ']}

def planeStrain():
    return {'aster':[code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy],
            'python':['DX','DY']}

def planeStress():
    return {'aster':[code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy],
            'python':['DX','DY']}

def mechanical(modelisation):
    func = modelisationSwitch.get(modelisation,None)
    return func()

modelisationSwitch = {
        'C_PLAN' : planeStress,
        'D_PLAN' : planeStrain,
        '3D' : threeD,
        }
phenomenonSwitch = {
        'MECANIQUE' : mechanical
        }

def getDofsFromModelisation(phenomenon,modelisation):
    """
    Select the degrees of freedom associated to the phenomenon and the modeli-
    -sation.
    Input:
        -phenomenon: type of phenomenon (see doc aster of AFFE_MODELE)
        -modelisation: type of modelisation (see doc aster of AFFE_MODELE)
    Return a dictionnary with a list of aster dofs and a list of "python" dofs
    """
    func = phenomenonSwitch.get(phenomenon,None)
    return func(modelisation)

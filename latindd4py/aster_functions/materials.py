#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 17:29:03 2019
Functions to manage the materials
@author: oumaziz
"""
try:
    from lxml import etree as et
except:
    print("No module lxml")

try:
    import code_aster
    from code_aster.Commands import DEFI_MATERIAU,_F
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

import pdb

# =============================================================================
# Loader for isotropic material
# =============================================================================
def isotropicLoader(xmlPath,ID=''):
    """
    Load the information for an isotropic material
    Input:
        -material: xml balise of the material obtained by root.find(....)
    Output:
        return a dictionnary with the information about the material
    """

    mat = dict()
    mat['type'] = 'isotropic'
    mat['E'] = float(root.find(xmlPath+'/young').text)
    mat['nu'] = float(root.find(xmlPath+'/poisson').text)
    mat['rho'] = float(root.find(xmlPath+'/rho').text)
    return mat

# =============================================================================
# Loader for orthotropic material (in construction)
# =============================================================================
def orthotropicLoader(material):
    """
    Load the information for an isotropic material
    Input:
        -material: xml balise of the material obtained by root.find(....)
    Output:
        return a dictionnary with the information about the material
    """

    mat = dict()
    return mat

# =============================================================================
# Builder for isotropic material
# =============================================================================
def isotropicBuilder(matDict,weight=1.):
    """
    Build the aster material
    Input:
        -matDict: dictionnary which describes the material
        -weight: potential weighting of mechanical caracteristics
    Output:
        - a concept material aster
    """
    ### Get the material properties
    young  = weight * matDict['E']
    poisson = matDict['nu']
    rho = matDict['rho']
    ### Create the aster concept
#    mater = code_aster.ElasMaterialBehaviour()
#    mater.setDoubleValue("E",young)
#    mater.setDoubleValue("Nu",poisson)
#    mater.setDoubleValue("Rho",rho)
#
#    mat = code_aster.Material()
#    mat.addMaterialBehaviour(mater)
#    mat.build()
    mat = DEFI_MATERIAU(ELAS=_F(E = young,NU=poisson,RHO=rho))

    return mat


materialLoader = {
        'isotropic' : isotropicLoader,
        'orthotropic' : orthotropicLoader
        }

materialBuilder = {
        'isotropic' : isotropicBuilder}

def assign_material_dict(mat_dict,meshAster):
    """
    Return an aster material instance
    Input:
        -mat_dict: dictionnary for which keys are the name of groups of
        elements and the aster material
        -meshAster: mesh aster
    Output:
        - aster material assigned on a mesh
    """
    affectMat = code_aster.MaterialOnMesh(meshAster)
    for group in list(mat_dict.keys()):
        affectMat.addMaterialOnGroupOfElements(mat_dict[group],[group])
    affectMat.buildWithoutInputVariables()
    return affectMat


def loadMaterialFromXML(xmlFile,numberSubDomain):
    """
    Load the information about the material ID in the xml
    Input:
        -xmlFile: path to the xml file
        -numberSubDomain : number of subdomains
    Output:
        return a dictionnary with the information about the material
    """
    global root
    tree = et.parse(xmlFile)
    root = tree.getroot()
    # Default materials
    defaultMat = {'def' : materialLoader.get(root.find("structure/material/[@id='default']").attrib['type'],None)("structure/material/[@id='default']")}
    # Subdomain's default material
    if root.find("structure/material/[@id='default']/subdomain"):
        ### Number of default materials on subdomains
        numDefaultMaterial = [root.xpath("structure/material[@id='default']/subdomain")[ii].attrib['id']
                              for ii in range(len(root.xpath("structure/material[@id='default']/subdomain")))]
        ### Loop on the default materials on subdomains
        for sd in range(len(numDefaultMaterial)):
            subdomains = [int(jj) for jj in root.xpath("structure/material[@id='default']/subdomain")[sd].attrib['id'].split(",")]
            for pp in subdomains:
                defaultMat[pp] = materialLoader.get(root.xpath("structure/material[@id='default']/subdomain")[sd].attrib['type'],None)("structure/material/[@id='default']/subdomain[@id='"+numDefaultMaterial[sd]+"']")
    # Materials defined on groups
    if root.xpath("structure/material[not(@id='default')]"):
        numMaterial = [int(root.xpath("structure/material[not(@id='default')]")[ii].attrib['id'])
                              for ii in range(len(root.xpath("structure/material[not(@id='default')]")))]
    else:
        numMaterial = []

    material = {mat : materialLoader.get(root.find("structure/material[@id='{}']".format(mat)).attrib['type'],None)("structure/material[@id='{}']".format(mat)) for mat in numMaterial}

    # If defautl material is not given for a subdomain, the default is chosen
    defaultMaterialSD = dict()
    for ii in range(numberSubDomain):
        if not (ii in list(defaultMat.keys())):
            pp = 'def'
        else:
            pp = ii

        defaultMaterialSD[ii] = defaultMat[pp]

    return defaultMaterialSD,material


def createAsterMaterial(matDict,weight=1.):
    """
    Create an aster material.
    Input:
        -matDict: dictionnary which describes the material
        -weight: potential weighting of mechanical caracteristics
    Output:
        - a concept material aster
    """
    mat = materialBuilder.get(matDict['type'],None)(matDict,weight)
    return mat


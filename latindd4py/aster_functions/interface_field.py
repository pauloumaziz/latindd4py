#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 10:37:22 2017
Interface fields
@author: oumaziz
"""
import numpy as np
try:
    import code_aster
    from code_aster.Commands import CREA_CHAMP,CREA_TABLE,_F
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

class asterField():
    """
    Class of interface field with an aster format
    """
#==============================================================================
# Initialisation
#==============================================================================
    def __init__(self,*args):
        """
        If 3 arguments : Initialisation to zero mod,GNO,param
        Input:
            -mod = model
            -GNO = group of nodes
            -param: class of parameters
        If 1 argument  : Initialisation with an existing aster field
        """
        global AsterDim, o, AsterCount, memLoc
        if len(args) == 3:
            mod = args[0]
            GNO = args[1]
            param = args[2]
            comp = ('DX','DY','DZ')
            val = (0.,0.,0.)
            self.field = CREA_CHAMP(OPERATION = 'AFFE',
                                                    TYPE_CHAM = 'NOEU_DEPL_R',
                                                    MODELE = mod,
                                                    AFFE = _F(GROUP_NO = GNO,
                                                            NOM_CMP = comp[:param.dim],
                                                            VALE = val[:param.dim],
                                                            )
                                                    )
        elif len(args) == 1:
            self.field = args[0]

#==============================================================================
# Return the aster field
#==============================================================================
    def returnAster(self):
        """
        Return the aster field
        """
        return self.field

# =============================================================================
# Save the field in a txt file
# =============================================================================
    def saveTxt(self,location):
        """
        Save the value of the field in a text file
        Input:
            -location: path to the Filter
        """

        f = open(location+".txt","a")
        f.write(' '.join(str(e) for e in self.convert2python().valField)+'\n')
        f.close()

#==============================================================================
# Conversion into python field
#==============================================================================
    def convert2python(self):
        """
        Convert an aster field into a python field
        """
        ### temporaire en attendant EXTR_COMP(topo=1).comp
        comp = ['DX','DY','DZ']
        topo = self.field.EXTR_COMP(topo = 1).noeud
        if topo[2] == topo[0]:
            dim = 3
        else:
            dim = 2
        val = self.field.EXTR_COMP().valeurs
        NO = ['N{}'.format(ii) for ii in sorted(list(set(list(self.field.EXTR_COMP(topo = 1).noeud))))]                                                       # Extraction of the topology of the field
        pythonfield = pythonField(val,NO)
        return pythonfield

#==============================================================================
# Overload the + operator
#==============================================================================
    def __add__(self,field):
        """
        Overload the + operation
        Input:
            -field: aster field
        """
        Sum = CREA_CHAMP(TYPE_CHAM = 'NOEU_DEPL_R',
                         OPERATION = 'COMB',
                         COMB = (_F(CHAM_GD = self.field, COEF_R = 1.),
                                 _F(CHAM_GD = field.field, COEF_R = 1.),
                                 ),
                         )
        return asterField(Sum)

#==============================================================================
# Overload the - operator
#==============================================================================
    def __sub__(self,field):
        """
        Overload the - operation
        Input:
            -field: aster field
        """
        Sub = CREA_CHAMP(TYPE_CHAM = 'NOEU_DEPL_R',
                         OPERATION = 'COMB',
                         COMB = (_F(CHAM_GD = self.field, COEF_R = 1.),
                                 _F(CHAM_GD = field.field, COEF_R = -1.),
                                 ),
                         )
        return asterField(Sub)

#==============================================================================
# Overload the * operator
#==============================================================================
    def __mul__(self,scal):
        """
        Overload the * operation
        Dot by scalar or scalar product between 2 fields
        Input:
            -scal: scalar or aster field
        """
        ### Case of a multiplication by a scalar
        if isinstance(scal,float):
            Mul = CREA_CHAMP(TYPE_CHAM = 'NOEU_DEPL_R',
                            OPERATION = 'COMB',
                            COMB = _F(CHAM_GD = self.field, COEF_R = scal),
                            )
            return asterField(Mul)
        ### Case of a scalar product
        elif isinstance(scal,asterField):
            return np.vdot(self.field.EXTR_COMP().valeurs,scal.field.EXTR_COMP().valeurs)


#==============================================================================
# Overload the rmul operator
#==============================================================================
    def __rmul__(self,scal):
        """
        Overload the * operation for the case of a multiplication by a scalar
        Input:
            -scal: scalar
        """
        return self * (scal)

#==============================================================================
# Overload the div operator
#==============================================================================
    def __truediv__(self,scal):
        """
        Overload the / operation
        Input:
            -scal: scalar
        """
        Div = CREA_CHAMP(TYPE_CHAM = 'NOEU_DEPL_R',
                         OPERATION = 'COMB',
                         COMB = _F(CHAM_GD = self.field, COEF_R = 1. / scal),
                         )
        return asterField(Div)
#==============================================================================
# Overload the rdiv operator
#==============================================================================
    def __rtruediv__(self,scal):
        """
        Overload the / operation
        Input:
            -scal: scalar
        """
        return self / scal

class pythonField():
    """
    Class of interface field with a python format
    """
    def __init__(self,val,NO):
        self.valField = val                                                     # Value of the field
        self.nodes = NO                                                         # Nodes related to the field

#==============================================================================
# Convert the python field into an aster field
#==============================================================================
    def convert2aster(self,param):
        """
        Convert into an aster field
        """
        dim = self.valField.shape[0] // len(self.nodes)
        ### A modifier pour éviter le if dégueux
        ### Table creation
        if dim == 2:
            table = CREA_TABLE(LISTE = (_F(LISTE_K = self.nodes,PARA = 'NOEUD'),		# Liste des noeuds associés au champ
                                                _F(LISTE_R = self.valField[list(range(0,param.dim * len(self.nodes),2))],PARA = 'DX'),		# Composante DX
                                                _F(LISTE_R = self.valField[list(range(1,param.dim * len(self.nodes),2))],PARA = 'DY'),		# Composante DY
                                                )
                                        )
        elif dim == 3:
            table = CREA_TABLE(LISTE = (_F(LISTE_K = self.nodes,PARA = 'NOEUD'),		# Liste des noeuds associés au champ
                                                _F(LISTE_R = self.valField[list(range(0,param.dim * len(self.nodes),3))],PARA = 'DX'),		# Composante DX
                                                _F(LISTE_R = self.valField[list(range(1,param.dim * len(self.nodes),3))],PARA = 'DY'),		# Composante DY
                                                _F(LISTE_R = self.valField[list(range(2,param.dim * len(self.nodes),3))],PARA = 'DZ'),		# Composante DZ
                                                )
                                        )
        ### Creation of the aster field
        field = CREA_CHAMP(TYPE_CHAM = 'NOEU_DEPL_R',					# Type de champ (ici champ par noeud
                                    OPERATION = 'EXTR',
                                    TABLE = table,
                                    MAILLAGE = param.mesh,					# Maillage associé
                                    )
        return asterField(field)

#==============================================================================
# Extract a part on field defined on group of nodes
#==============================================================================
    def extract_field_on_nodes(self,nodes):
        """
        Return the field extracted on a list of nodes.
        Input:
            -nodes: list on nodes on which the field is extract
        Output:
            -pythonField instance
        """
        ### Test if nodes is included in self.field
        if set(nodes).issubset(set(self.nodes)):
            dim = self.valField.shape[0] // len(self.nodes)
            dict_nodes = {node:pos for pos,node in enumerate(self.nodes)}
            pos_in_nodes = [dict_nodes[node] for node in nodes]
            pos_in_valField = [dim * ii+jj for ii in pos_in_nodes for jj in range(dim)]
            if len(self.valField.shape) > 1:
                val = self.valField[pos_in_valField,:]
            else:
                val = self.valField[pos_in_valField]
            return pythonField(val,nodes)
        else:
            print("The list of nodes given in argument is not included in the topology of the original field")

# =============================================================================
# Merge 2 fields in one
# =============================================================================

    def mergeField(self,field,merge='sum'):
        """
        Merge 2 fields in one.
        Input:
            -field: python field to merge with
            -merge: option to merge in case of common nodes
                    -'sum': makes the sum of the contributions of each nodes
                    -0: chooses the 1st field for the common nodes
                    -1: chooses the 2nd field for the common nodes
        Output:
            -return a new pythonField
        """
        dim = self.valField.shape[0] // len(self.nodes)
        list_nodes = list(set(self.nodes+field.nodes))
        nodes = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(list_nodes))])))]
        valField = np.zeros(dim*len(nodes))
        dict_nodes = {node:pos for pos,node in enumerate(nodes)}
        pos0_in_nodes = [dict_nodes[node] for node in self.nodes]
        pos1_in_nodes = [dict_nodes[node] for node in field.nodes]
        if merge == 'sum':
            valField[[dim * ii + kk for ii in pos0_in_nodes for kk in range(dim)]] += self.valField
            valField[[dim * ii + kk for ii in pos1_in_nodes for kk in range(dim)]] += field.valField
        elif merge == 0:
            valField[[dim * ii + kk for ii in pos1_in_nodes for kk in range(dim)]] += field.valField
            valField[[dim * ii + kk for ii in pos0_in_nodes for kk in range(dim)]] = self.valField
        elif merge == 1:
            valField[[dim * ii + kk for ii in pos0_in_nodes for kk in range(dim)]] += self.valField
            valField[[dim * ii + kk for ii in pos1_in_nodes for kk in range(dim)]] = field.valField

        return pythonField(valField,nodes)

# =============================================================================
# Extend the field to zero on nodes
# =============================================================================
    def prol_zero(self,nodes_glob):
        """
        Create a new field extended to zeros from a inital field
        Input:
            -field: pythonField to extend
            -nodes: list of nodes of the final pythonField
        Output:
            -pythonField instance
        """
        dim = self.valField.shape[0] // len(self.nodes)
        prol_field = np.zeros(dim*len(nodes_glob))
        dict_nodes = {node:pos for pos,node in enumerate(nodes_glob)}
        pos_in_nodes = [dict_nodes[node] for node in self.nodes]
        prol_field[[dim * ii + kk for ii in pos_in_nodes for kk in range(dim)]] = self.valField
        self.valField = prol_field
        self.nodes = nodes_glob


# =============================================================================
# Replace a part of the field by other fields
# =============================================================================
    def replace_part_of_field(self,listOfField):
        """
        Replace a part of the field by the content of other fields.
        Input:
            -listOfField: list of pythonField
        """
        for field in listOfField:
            if set(field.nodes).issubset(set(self.nodes)):
                dim = self.valField.shape[0] // len(self.nodes)
                pos_in_nodes = [self.nodes.index(node) for node in field.nodes]
                pos_in_valField = [dim * ii+jj for ii in pos_in_nodes for jj in range(dim)]
                self.valField[pos_in_valField] = field.valField
            else:
                print("The topology of given fields are not included in the topology of the original field")

# =============================================================================
# Save the field in a txt file
# =============================================================================
    def saveTxt(self,location):
        """
        Save the value of the field in a text file
        Input:
            -location: path to the Filter
        """
        f = open(location+".txt","a")
        f.write(' '.join(str(e) for e in self.valField)+'\n')
        f.close()

#==============================================================================
# Overload the add operator
#==============================================================================
    def __add__(self,field):
        """
        Overload the + operator
        """
        if self.nodes == field.nodes:
            return pythonField(self.valField + field.valField,self.nodes)
        else:
            print("Error : non compatible topology")
#==============================================================================
# Overload the sub operator
#==============================================================================
    def __sub__(self,field):
        """
        Overload the + operator
        """
        if self.nodes == field.nodes:
            return pythonField(self.valField - field.valField,self.nodes)
        else:
            print("Error : non compatible topology")

#==============================================================================
# Overload the neg operator
#==============================================================================
    def __neg__(self):
        """
        Overload the + operator
        """
        return pythonField(- self.valField,self.nodes)


#==============================================================================
# Overload the mul operator
#==============================================================================
    def __mul__(self,scal):
        """
        Overload the * operator
        """
        if isinstance(scal,pythonField):
            return pythonField(self.valField * scal.valField,self.nodes)
        else:
            return pythonField(self.valField * scal,self.nodes)

#==============================================================================
# Overload the pow operator to do scalar product for python field
#==============================================================================
    def __pow__(self,field):
        """
        Overload the pow operator to do scalar product for python field
        """
        return self.valField.dot(field.valField)

#==============================================================================
# Overload the rmul operator
#==============================================================================
    def __rmul__(self,scal):
        """
        Overload the * operation for the case of a multiplication by a scalar
        """
        return self * scal

#==============================================================================
# Overload the div operator
#==============================================================================
    def __truediv__(self,scal):
        """
        Overload the / operator
        """
        return pythonField(self.valField / scal,self.nodes)

#==============================================================================
# Overload the rdiv operator
#==============================================================================
    def __rtruediv__(self,scal):
        """
        Overload the / operation
        """
        return pythonField(scal / self.valField,self.nodes)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 14:54:48 2017
Functions to save data and export results
@author: oumaziz
"""
import os
from .interface_field import pythonField
from .basics import create_resu
import numpy as np
try:
    from code_aster.Commands import CALC_CHAMP,IMPR_RESU, DEFI_FICHIER,_F
except:
    print("asterxx is not sourced")
#==============================================================================
# Save a number in a text file
#==============================================================================
def saveTxt(toto,locate_file):
    """
    Save a number in a text file.
    Input:
        -toto: number to save
        -locate_file: location of the file where to save
    """
    f = open(locate_file,"a")
    if isinstance(toto,np.ndarray):
        f.write(' '.join(str(e) for e in toto)+'\n')
    else:
        f.write(str(toto)+'\n')
    f.close()

def saveField2MED(field,model,mater,locate_file,param):
    """
    Export a field in a MED file
    Input:
        -field: field to save
        -model: aster model
        -mater: assigned aster materials
        -locate_file: path wher to save the MED file
        -param: class of parameters
    """
    if isinstance(field,pythonField):
        fieldAster = field.convert2aster(param)
    else:
        fieldAster = field

    resu = create_resu(fieldAster.field,model,mater)
    DEFI_FICHIER(ACTION='ASSOCIER',UNITE=83,FICHIER=locate_file,TYPE = 'LIBRE')
    IMPR_RESU(UNITE = 83,
              FORMAT = 'MED',
              RESU = _F(RESULTAT = resu,
                        NOM_CHAM = ('DEPL'),
                        NOM_CHAM_MED = ('depl')
                        )
              )
    DEFI_FICHIER(ACTION='LIBERER',UNITE=83)
#==============================================================================
# Build results in MED file
#==============================================================================
def exportResults2MED(field,subDomain,locate_file_basis,param):
    """
    Build the result and export it into a med file.
    Input:
        -field: Value instance where the results are stored
        -subDomain: class of the subdomain
        -locate_file: location of the file to save the results
        -param: class of parameters
    """
#    if param.quasistatic == 'YES':
    for tt in param.listTimeSteps:
        resu = create_resu(field.W[tt]['W'].convert2aster(param).field,
                           subDomain.SD.model.MODELE,
                           subDomain.SD.model.assigned_material,
                           )
        ### Calcul des champs de contraintes et déformations avec CALC_CHAMP
        resu = CALC_CHAMP(reuse = resu,
                          RESULTAT = resu,
                          CONTRAINTE = ('SIGM_ELNO'),
                          DEFORMATION = ('EPSI_ELNO'),
                          FORCE = ('REAC_NODA'),
                          GROUP_MA = subDomain.soleSD.mesh.GMA['S'],
                          INST = 0.
                          )
        if os.path.isfile(locate_file_basis+'-{}t.med'.format(tt)):
            param.logger.warning("A result existed in the MED file : " + locate_file_basis+"-{}t.med".format(tt))
            os.remove(locate_file_basis+'-{}t.med'.format(tt))
        DEFI_FICHIER(ACTION='ASSOCIER',UNITE=83,FICHIER=locate_file_basis+'-{}t.med'.format(tt),TYPE = 'LIBRE')
        if param.multiscale == 'YES' and param.macroBasis == 'GENEO':
            IMPR_RESU(UNITE = 83,
                  FORMAT = 'MED',
                  RESU = (_F(RESULTAT = resu,
                            NOM_CHAM = ('DEPL','SIGM_ELNO','EPSI_ELNO','REAC_NODA'),
                            NOM_CHAM_MED = ('depl','sigm','epsi','reac')
                            ),
                          _F(CHAM_GD = subDomain.Macro['fieldNumberMacro'].field,
                             NOM_CHAM_MED = 'numberMacroModes')
                          ),
                  )
        else:
            IMPR_RESU(UNITE = 83,
                  FORMAT = 'MED',
                  RESU = (_F(RESULTAT = resu,
                            NOM_CHAM = ('DEPL','SIGM_ELNO','EPSI_ELNO','REAC_NODA'),
                            NOM_CHAM_MED = ('depl','sigm','epsi','reac')
                            ),
                          ),
                  )
        DEFI_FICHIER(ACTION='LIBERER',UNITE=83)


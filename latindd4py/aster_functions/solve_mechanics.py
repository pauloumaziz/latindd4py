#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 10:00:19 2017

@author: oumaziz
"""
try:
    import code_aster
    from code_aster.Commands import AFFE_CHAR_MECA,ASSE_VECTEUR,CALC_CHAMP,CALC_CHAR_CINE,CALC_VECT_ELEM,CREA_CHAMP,_F
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")
from latindd4py.aster_functions.interface_field import pythonField,asterField
from latindd4py.aster_functions.basics import build_result_from_displ,create_resu,prol_zero
import numpy as np

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# =============================================================================
# Compute nodal reaction from a displacement field on a specific group
# =============================================================================
def compute_nodal_reaction_from_field_on_group(field,model,mat,group,char_cine=None):
    """
    Compute nodal reaction from a displacement field on a specific group.
    Input:
        -field: aster displacement field
        -model: aster model,
        -mat: assigned material on a mesh
        -char_cine: dirichlet boundary conditions
        -group: group where the nodal reaction has to be computed
    Output:
        -asterField instance
    """
    ### Create result concept from the displacement field
    resu = create_resu(field,model,mat,char_cine)
    resu = CALC_CHAMP(reuse = resu,
                      FORCE = 'REAC_NODA',
                      GROUP_MA = group,
                      RESULTAT = resu,
                      INST=0.)
    toto = CREA_CHAMP(OPERATION = 'EXTR',
                                NOM_CHAM = 'REAC_NODA',
                                TYPE_CHAM = 'NOEU_DEPL_R',
                                RESULTAT = resu,
                                INST=0.
                                )
    nodal_reaction = CREA_CHAMP(OPERATION = 'ASSE',
                                TYPE_CHAM = 'NOEU_DEPL_R',
                                MODELE = model,
                                ASSE = _F(CHAM_GD = toto,GROUP_MA = group),
                                )

    return asterField(nodal_reaction).convert2python()


#==============================================================================
# Assigned the imposed boundary conditions defined in the input data file as an
# aster concept
#==============================================================================
def imposed_boundary_conditions(subDomain,param,tt = 0):
    """
    Function to assign the imposed boundary conditions as aster concepts.
    Return lists of the aster concept of boundary conditions
    Input:
        -subDomain: class Subdomain
        -param: class of parameters
        -tt: number of the timestep if the quasistatic assumption is chosen
    Output:
        -neumann_conditions: list of aster boundary conditions of type char_meca
        -dirichlet_conditions: list of aster boundary conditions of type char_cine
    """
    neumann_conditions = []
    dirichlet_conditions = []
    ### Neumann boundary conditions
    for jj,neumBC in enumerate(subDomain.neighbourNeum):
        if param.boundaryNeumann[neumBC]['type'] == 'FORCE_CONTOUR':
            imposedBC = code_aster.ForceDouble()
            imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fx,
                               param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FX'])
            imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fy,
                               param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FY'])
            boundary_condition = code_aster.LineicForceDouble(subDomain.soleSD.model.MODELE)
            boundary_condition.setValue(imposedBC,subDomain.soleSD.mesh.GMA['FdM{}'.format(jj)])
            boundary_condition.build()
        elif param.boundaryNeumann[neumBC]['type'] == 'PRES_REP':
            imposedBC = code_aster.PressureDouble()
            imposedBC.setValue(code_aster.PhysicalQuantityComponent.Pres,
                               param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['PRES'])

            boundary_condition = code_aster.DistributedPressureDouble(subDomain.soleSD.model.MODELE)
            boundary_condition.setValue(imposedBC,subDomain.soleSD.mesh.GMA['FdM{}'.format(jj)])
            boundary_condition.build()
        elif param.boundaryNeumann[neumBC]['type'] == 'FORCE_FACE':
            imposedBC = code_aster.ForceDouble()
            imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fx,
                               param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FX'])
            imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fy,
                               param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FY'])
            imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fz,
                               param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FZ'])
            boundary_condition = code_aster.ForceOnFaceDouble(subDomain.soleSD.model.MODELE)
            boundary_condition.setValue(imposedBC,subDomain.soleSD.mesh.GMA['FdM{}'.format(jj)])
            boundary_condition.build()
        elif param.boundaryNeumann[neumBC]['type'] == 'PESANTEUR':
            print('titi')
            boundary_condition = AFFE_CHAR_MECA(MODELE = subDomain.soleSD.model.MODELE,
                                                PESANTEUR = _F(GROUP_MA = subDomain.soleSD.mesh.GMA['S'],
                                                               GRAVITE = param.boundaryNeumann[neumBC]['GRAVITE'],
                                                               DIRECTION = (param.boundaryNeumann[neumBC]['DX'],
                                                                            param.boundaryNeumann[neumBC]['DY'],
                                                                            param.boundaryNeumann[neumBC]['DZ']),
                                                               )
                                            )
        neumann_conditions.append(boundary_condition)

    ### Dirichlet boundary conditions
    if subDomain.soleSD.model.boundary_conditions['imposed_dirichlet']:
        dirichlet_conditions.append(subDomain.soleSD.model.boundary_conditions['imposed_dirichlet'][tt])
    return neumann_conditions,dirichlet_conditions

#==============================================================================
# Problem on a double sole
#==============================================================================
def problemOnDoubleSole(doubleSole,field,param,case = 'force'):
    """
    Function to solve a mechanical problem on a double sole. Return the field on
    the interface.
    Input:
        -doubleSole: class Struct of the double sole
        -field: field to applied on the interface on the double sole
        -param: class of parameters
        -case: case if the field is a force or a displacement
    Output:
        -Wch: dictionnary of pythonField instance for the displacements at the
        interface
        -Tch: dictionnary of pythonField instance for the nodal reactions at
        the interface
    """
    if isinstance(field,pythonField):
        fieldAster = field.convert2aster(param)
    else:
        fieldAster = field
    if case == 'force':

        F_asse = CREA_CHAMP(OPERATION = 'ASSE',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            MODELE = doubleSole.model.MODELE,
                            NUME_DDL = doubleSole.model.nume_ddl,
                            PROL_ZERO = 'OUI',
                            ASSE = _F(TOUT='OUI', CHAM_GD = fieldAster.field, CUMUL='OUI')
                            )
        ## Solve the problem
        displacement = param.solver.solveDoubleLinearSystemWithKinematicsLoad(doubleSole.model.stiffness,doubleSole.model.dirichlet_right_hand_side,F_asse)
        ## Extract the displacement field on the interface
        W = CREA_CHAMP(OPERATION = 'ASSE',
                       TYPE_CHAM = 'NOEU_DEPL_R',
                       MODELE = doubleSole.model.MODELE,
                       ASSE = _F(CHAM_GD = displacement, GROUP_NO = doubleSole.mesh.GNO['GN'],)
                      )

        Wch = {0:asterField(W).convert2python(),1:asterField(W).convert2python()}
        ### Computation of nodal reaction
        resu = create_resu(displacement,doubleSole.model.MODELE,doubleSole.model.assigned_material)
        Tch = dict()

        for jj in range(2):
            resu = CALC_CHAMP(reuse = resu,FORCE = 'REAC_NODA',GROUP_MA = doubleSole.mesh.GMA['S{}'.format(jj)],RESULTAT = resu,INST = 0)

            T = CREA_CHAMP(OPERATION = 'EXTR',
                             NOM_CHAM = 'REAC_NODA',
                             TYPE_CHAM = 'NOEU_DEPL_R',
                             RESULTAT = resu,
                             INST = 0,
                             )
            Tc = CREA_CHAMP(OPERATION = 'ASSE',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            MODELE = doubleSole.model.MODELE,
                            ASSE = _F(CHAM_GD = T,GROUP_NO = doubleSole.mesh.GNO['GN']),
                            )
            Tch[jj] = asterField(Tc).convert2python()
        return Wch,Tch
#    elif case == 'displacement':
##        BC = assign_displacement_field_on_BC(fieldAster,doubleSole.model.MODELE,param)
##        mecaStatique = code_aster.StaticMechanicalSolver(doubleSole.model.MODELE,doubleSole.model.assigned_material)
##        ## Assign the boundary conditions
###        for dirichlet in doubleSole.model.boundary_conditions.values():
###            mecaStatique.addMechanicalLoad(dirichlet)
##        mecaStatique.addKinematicsLoad(doubleSole.model.boundary_conditions['BlocS'])
##        mecaStatique.addMechanicalLoad(BC)
###        ## Add the model
###        mecaStatique.setSupportModel(doubleSole.model.MODELE)
###        ## Add the material
###        mecaStatique.setMaterialOnMesh(doubleSole.model.assigned_material)
##        ## Add the solver
##        mecaStatique.setLinearSolver(param.solver)
##        ## Solve the problem
##        resu = mecaStatique.execute()
#        ## Extract the displacement
#        Wch = dict()
#        W = CREA_CHAMP(OPERATION = 'EXTR',
#                         NOM_CHAM = 'DEPL',
#                         TYPE_CHAM = 'NOEU_DEPL_R',
#                         RESULTAT = resu)
#        Wc = CREA_CHAMP(OPERATION = 'ASSE',
#                         TYPE_CHAM = 'NOEU_DEPL_R',
#                         MODELE = doubleSole.model.MODELE,
#                         ASSE = _F(CHAM_GD = W,GROUP_NO = doubleSole.mesh.GNO['GN'])
#                         )
#        Wch = {0:asterField(Wc).convert2python(),1:asterField(Wc).convert2python()}
#        return Wch

#==============================================================================
# Problem on a double sole with python
#==============================================================================
def problemOnDoubleSolePython(doubleSole,sole,field,param,case = 'force'):
    """
    Function to solve a mechanical problem on a double sole. Return the field on
    the interface.
    Input:
        -doubleSole: class Struct of the double sole
        -sole: dictionnary of the two soles associated to the interface
        -field: field to applied on the interface on the double sole
        -param: class of parameters
        -case: case if the field is a force or a displacement
    Output:
        -Wch: dictionnary of pythonField instance for the displacements at the
        interface
        -Tch: dictionnary of pythonField instance for the nodal reactions at
        the interface
    """
    if case == 'displacement':
        displacement_python = prol_zero(field,doubleSole.mesh.NO['Nt']).valField
        reac = pythonField(doubleSole.model.stiffness_noOverlap_python.dot(displacement_python),doubleSole.mesh.NO['Nt']).extract_field_on_nodes(doubleSole.mesh.NO['N'])
        return reac
    else:
        rhs = prol_zero(field,doubleSole.mesh.NO['Nt'])
        ## Solve the problem
        displacement = doubleSole.model.stiffness_python_factorized.solve(rhs.valField)
        ## Extract the displacement field on the interface
        Wc = pythonField(displacement,doubleSole.mesh.NO['Nt']).extract_field_on_nodes(doubleSole.mesh.NO['N'])

        Wch = {0:Wc,1:Wc}
        Tch = dict()
        for jj in range(2):
            Tch[jj] = pythonField(sole[jj].model.stiffness_noOverlap_python.dot(prol_zero(Wc,sole[jj].mesh.NO['Nt']).valField),sole[jj].mesh.NO['Nt']).extract_field_on_nodes(sole[jj].mesh.NO['N'])
        return Wch,Tch

#==============================================================================
# Problem on a sole
#==============================================================================
def problemOnSole(sole,field,param,case = 'displacement'):
    """
    Function to solve a mechanical problem on a sole. Return the field on the
    interface.
    Input:
        -sole: class Struct of the sole
        -field: field to applied on the interface of the sole
        -param: class of parameters
        -case: cas if the field is a force or a displacement
    Output:
        -if case='force', return a pythonField instance for a displacement
        field on the interface
        -if case='displacement', return a pythonField instance for a nodal
        reaction field on the interface
    """
    if isinstance(field,pythonField):
        fieldAster = field.convert2aster(param)
    else:
        fieldAster = field
    if case == 'force':
        F_asse = CREA_CHAMP(OPERATION = 'ASSE',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            MODELE = sole.model.MODELE,
                            NUME_DDL = sole.model.nume_ddl,
                            PROL_ZERO = 'OUI',
                            ASSE = _F(TOUT='OUI', CHAM_GD = fieldAster.field, CUMUL='OUI')
                            )
        displacement = param.solver.solveDoubleLinearSystemWithKinematicsLoad(sole.model.stiffness,sole.model.dirichlet_right_hand_side,F_asse)
        W = CREA_CHAMP(OPERATION = 'ASSE',
                       TYPE_CHAM = 'NOEU_DEPL_R',
                       MODELE = sole.model.MODELE,
                       ASSE = _F(CHAM_GD = displacement, GROUP_NO = sole.mesh.GNO['GN'],)
                      )
        return asterField(W).convert2python()
    elif case == 'displacement':
        U_asse = CREA_CHAMP(OPERATION = 'ASSE',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            MODELE = sole.model.MODELE,
                            NUME_DDL = sole.model.nume_ddl,
                            PROL_ZERO = 'OUI',
                            ASSE = _F(TOUT='OUI', CHAM_GD = fieldAster.field, CUMUL='OUI')
                            )
        displacement = param.solver.solveDoubleLinearSystemWithKinematicsLoad(sole.model.stiffness_dirichlet_full,U_asse,sole.model.dirichlet_full_right_hand_side)

        resu = build_result_from_displ(displacement,sole.model.MODELE,sole.model.assigned_material,param)
        ### Manque Calc_champ pour posttraiter les réactions nodales !!!!
        resu = CALC_CHAMP(reuse = resu,FORCE = 'REAC_NODA',GROUP_MA = sole.mesh.GMA['S'],RESULTAT = resu,INST = 0.)

        T = CREA_CHAMP(OPERATION = 'EXTR',
                         NOM_CHAM = 'REAC_NODA',
                         TYPE_CHAM = 'NOEU_DEPL_R',
                         RESULTAT = resu,
                         INST = 0.,
                         )
        Tc  = CREA_CHAMP(OPERATION = 'ASSE',
                         TYPE_CHAM = 'NOEU_DEPL_R',
                         MODELE = sole.model.MODELE,
                         ASSE = _F(CHAM_GD = T,GROUP_NO = sole.mesh.GNO['GN']),
                         )
        return asterField(Tc).convert2python()

#==============================================================================
# Problem on a sole MRHS
#==============================================================================
def problemOnSoleMRHS(sole,field,param,case = 'displacement'):
    """
    Function to solve a mechanical problem on a sole. Return the field on the
    interface.
    Input:
        -sole: class Struct of the sole
        -field: field to applied on the interface of the sole
        -param: class of parameters
        -case: cas if the field is a force or a displacement
    Output:
        -if case='force', return a pythonField instance for a displacement
        field on the interface
        -if case='displacement', return a pythonField instance for a nodal
        reaction field on the interface
    """
    import mumps
    size_problem = param.dim * len(sole.mesh.NO['Nt'])
    if case == 'displacement':
        displacement_python = np.hstack([np.reshape(prol_zero(field[tt],sole.mesh.NO['Nt']).valField,(size_problem,1)) for tt in range(param.numberTimeStep)])

        reac = sole.model.stiffness_noOverlap_python.dot(displacement_python)
        reac_python = dict()
        for tt in range(param.numberTimeStep):
            reac_python[tt] = pythonField(reac[:,tt],sole.mesh.NO['Nt']).extract_field_on_nodes(sole.mesh.NO['N'])

        return reac_python
    elif case == 'force':
        force_python = [prol_zero(field[tt],sole.mesh.NO['Nt']).valField for tt in range(param.numberTimeStep)]
        ## stack the rhs to obtain a rhs matrix : each column corresponds to each rhs
        F_rhs = np.vstack(force_python).T
        ## Initialization of mumps context
        ctx = mumps.DMumpsContext(sym=0,comm=MPI.COMM_SELF)
        ## Set the number of rhs
        ctx.id.nrhs = param.numberTimeStep
        ## Set the size of rhs
        ctx.id.lrhs = F_rhs.shape[0]
        # ctx.id.lrhs = size_problem
        ## Set some options
        ctx.set_icntl(21,0)
        # ctx.set_icntl(7,0)
        ctx.set_icntl(28,1) # To use a seqential version of mumps for the ordering
        ctx.set_icntl(13,1) # To use a seqential version for the root node
        ## Set the Stiffness matrix to sparse format
        ctx.set_centralized_sparse(sole.model.stiffness_python)
        ## Set the rhs
        ctx.set_rhs(F_rhs)
        ## Run
        ctx.run(job=6)
        W = dict()
        for tt in range(param.numberTimeStep):
            W[tt] = dict()
            ## Extraction of the displacement associated to the tt rhs
            displacement_python = pythonField(F_rhs[:,tt],sole.mesh.NO['Nt'])
            # displacement = displacement_python.convert2aster(param).field
            W[tt] = displacement_python.extract_field_on_nodes(sole.mesh.NO['N'])
        return W

#==============================================================================
# Problem on a sole python
#==============================================================================
def problemOnSolePython(sole,field,param,case = 'displacement'):
    """
    Function to solve a mechanical problem on a sole. Return the field on the
    interface.
    Input:
        -sole: class Struct of the sole
        -field: field to applied on the interface of the sole
        -param: class of parameters
        -case: cas if the field is a force or a displacement
    Output:
        -if case='force', return a pythonField instance for a displacement
        field on the interface
        -if case='displacement', return a pythonField instance for a nodal
        reaction field on the interface
    """
    if case == 'displacement':
        displacement_python = prol_zero(field,sole.mesh.NO['Nt']).valField
        reac = pythonField(sole.model.stiffness_noOverlap_python.dot(displacement_python),sole.mesh.NO['Nt']).extract_field_on_nodes(sole.mesh.NO['N'])
        return reac
    elif case == 'force':
        force_python = prol_zero(field,sole.mesh.NO['Nt']).valField
        ## stack the rhs to obtain a rhs matrix : each column corresponds to each rhs
        displ = sole.model.stiffness_python_lu.solve(force_python)
        ## Extraction of the displacement associated to the tt rhs
        displacement_python = pythonField(displ,sole.mesh.NO['Nt'])
        # displacement = displacement_python.convert2aster(param).field
        W = displacement_python.extract_field_on_nodes(sole.mesh.NO['N'])
        return W

#==============================================================================
# Problem on a subdomain
#==============================================================================
def subDomainResolution(subDomain,Ft,param,multi = 'NON',pp = 0):
    """
    Solve a problem on the subdomain and return the displacement on the
    interface and the nodal reaction on the interface.
    Input:
        -subDomain: class Subdomain
        -Ft: nodal force field on the whole edge of the subdomain
        -param: class of parameters
        -multi: choice if the function is used to compute the multi-scale
        operator or not
        -pp: number of the timestep if the quasistatic option is actived
    Output:
        -W: dictionnary of pythonField instance for displacement on the
        interfaces and subdomain
        -T: dictionnary of pythonField instance for nodal reaction on
        the interfaces
    """
    if isinstance(Ft,pythonField):
        FtAster = Ft.convert2aster(param)					# Transformation en champ Aster
    else:
        FtAster = Ft
    neumann_conditions = []
    dirichlet_conditions = []
    if multi == 'NON':
      [neumann_conditions,dirichlet_conditions] = imposed_boundary_conditions(subDomain,param,pp)
    dirichlet_conditions.append(subDomain.soleSD.model.boundary_conditions['BlocS'])
    dirichlet_rhs = CALC_CHAR_CINE(NUME_DDL = subDomain.soleSD.model.nume_ddl,
                               CHAR_CINE = dirichlet_conditions)
    asse = [_F(TOUT='OUI', CHAM_GD = FtAster.field, CUMUL='OUI')]
    if neumann_conditions:
        VEF = CALC_VECT_ELEM(OPTION ='CHAR_MECA',
                             CHARGE = neumann_conditions,
                             CHAM_MATER = subDomain.soleSD.model.assigned_material,
                             )

        Fd_asse = ASSE_VECTEUR(VECT_ELEM = VEF,
                              NUME_DDL = subDomain.soleSD.model.nume_ddl,
                              )
        asse.append(_F(TOUT='OUI', CHAM_GD = Fd_asse, CUMUL='OUI'))

    F_asse = CREA_CHAMP(OPERATION = 'ASSE',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            MODELE = subDomain.soleSD.model.MODELE,
                            NUME_DDL = subDomain.soleSD.model.nume_ddl,
                            PROL_ZERO = 'OUI',
                            ASSE = asse,
                            )

    displacement = param.solver.solveDoubleLinearSystemWithKinematicsLoad(subDomain.soleSD.model.stiffness,dirichlet_rhs,F_asse)
    resu = create_resu(displacement,subDomain.soleSD.model.MODELE,subDomain.soleSD.model.assigned_material)
    W = dict()
    W['W'] = asterField(CREA_CHAMP(OPERATION = 'ASSE',
                        TYPE_CHAM = 'NOEU_DEPL_R',
                        MODELE = subDomain.soleSD.model.MODELE,
                        ASSE = _F(CHAM_GD = displacement,GROUP_MA = subDomain.soleSD.mesh.GMA['S'],)
                        )).convert2python()
    T = dict()

    for jj in range(len(subDomain.neighbourSD)):
        W[jj] = asterField(CREA_CHAMP(OPERATION = 'ASSE',
                                    TYPE_CHAM = 'NOEU_DEPL_R',
                                    MODELE =subDomain.soleSD.model.MODELE,
                                    ASSE = _F(CHAM_GD = displacement, GROUP_NO = subDomain.soleSD.mesh.GNO['GN{}'.format(jj)],)
                                    )).convert2python()
        if param.method == 'latin':
            ### Il faut encore calculer les réactions nodales
            resu = CALC_CHAMP(reuse = resu,FORCE = 'REAC_NODA',GROUP_MA = subDomain.soleSD.mesh.GMA['S-{}'.format(jj)],RESULTAT = resu,INST=0.)
            Tjj_extr = CREA_CHAMP(OPERATION = 'EXTR',
                            NOM_CHAM = 'REAC_NODA',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            RESULTAT = resu,
                            INST=0.
                            )
            zob = CREA_CHAMP(OPERATION = 'ASSE',
                            TYPE_CHAM = 'NOEU_DEPL_R',
                            MODELE = subDomain.soleSD.model.MODELE,
                            ASSE = _F(CHAM_GD = Tjj_extr,GROUP_NO = subDomain.soleSD.mesh.GNO['GN{}'.format(jj)]),
                            )
            T[jj] = asterField(zob).convert2python()
    if param.method == 'asm':
        W['all_nodes'] = pythonField(displacement.EXTR_COMP().valeurs,subDomain.soleSD.mesh.NO['all_nodes'])
    return W,T

#==============================================================================
# Problem on a subdomain
#==============================================================================
def subDomainResolutionPython(subDomain,Ft,param,multi = 'NON',pp = 0):
    """
    Solve a problem on the subdomain and return the displacement on the
    interface and the nodal reaction on the interface.
    Input:
        -subDomain: class Subdomain
        -Ft: nodal force field on the whole edge of the subdomain
        -param: class of parameters
        -multi: choice if the function is used to compute the multi-scale
        operator or not
        -pp: number of the timestep if the quasistatic option is actived
    Output:
        -W: dictionnary of pythonField instance for displacement on the
        interfaces and subdomain
        -T: dictionnary of pythonField instance for nodal reaction on
        the interfaces
    """
    param.logger.info("                     Compute the rhs")
#    neumann_conditions = []
#    dirichlet_conditions = []

#    [neumann_conditions,dirichlet_conditions] = imposed_boundary_conditions(subDomain,param,pp)
    F_python  = prol_zero(Ft,subDomain.soleSD.mesh.NO['N_SD_tot']).valField
#    dirichlet_conditions.append(subDomain.soleSD.model.boundary_conditions['BlocS'])
#    dirichlet_rhs = CALC_CHAR_CINE(NUME_DDL = subDomain.soleSD.model.nume_ddl,
#                               CHAR_CINE = dirichlet_conditions)
#    asse = []
    if len(subDomain.neighbourNeum) > 0 and multi == 'NON':
#        VEF = CALC_VECT_ELEM(OPTION ='CHAR_MECA',
#                             CHARGE = neumann_conditions,
#                             CHAM_MATER = subDomain.soleSD.model.assigned_material,
#                             )
#
#        Fd_asse = ASSE_VECTEUR(VECT_ELEM = VEF,
#                              NUME_DDL = subDomain.soleSD.model.nume_ddl,
#                              )
#        asse.append(_F(TOUT='OUI', CHAM_GD = Fd_asse, CUMUL='OUI'))
#
#        F_asse = CREA_CHAMP(OPERATION = 'ASSE',
#                                TYPE_CHAM = 'NOEU_DEPL_R',
#                                MODELE = subDomain.soleSD.model.MODELE,
#                                NUME_DDL = subDomain.soleSD.model.nume_ddl,
#                                PROL_ZERO = 'OUI',
#                                ASSE = asse,
#                                )
#        F_python += F_asse.EXTR_COMP().valeurs
        F_python += subDomain.soleSD.model.neumann[pp]
    dict_nodes = {node:pos for pos,node in enumerate(subDomain.soleSD.mesh.NO['N_SD_tot'])}
    if subDomain.soleSD.model.boundary_conditions['imposed_dirichlet']:
        for ii,val in enumerate(subDomain.neighbourDiri):
            ### Select the dof imposed
            dofs = list(param.boundaryDirichlet[val].keys())
            dofs.remove('evol')
            indexDiri = [dict_nodes[noeud] for noeud in subDomain.soleSD.mesh.NO['Wd{}'.format(ii)]]
            indexDofDiri = sorted([param.dim * ii+jj for ii in indexDiri for jj in [param.dofs['python'].index(dof) for dof in dofs]])
            F_python[indexDofDiri] = subDomain.soleSD.model.dirichlet[pp][indexDofDiri]
    for jj in range(len(subDomain.neighbourSD)):
        indexSole = [dict_nodes[noeud] for noeud in subDomain.soleSD.mesh.NO['N{}b'.format(jj)]]
        indexDofSole = [param.dim * ii+jj for ii in indexSole for jj in range(param.dim)]
        F_python[indexDofSole] = np.zeros(len(indexDofSole))
    if len(subDomain.neighbourDiri) > 0 and multi == 'NON':
        F_python -= subDomain.soleSD.model.stiffness_extr_diri.dot(subDomain.soleSD.model.dirichlet[pp])
    displ = subDomain.soleSD.model.K_sparse_lu.solve(F_python)
    W = dict()
    T = dict()
    ## Extraction of the displacement associated to the tt rhs
    displacement_python = pythonField(displ,subDomain.soleSD.mesh.NO['N_SD_tot'])
    W['W'] = displacement_python.extract_field_on_nodes(subDomain.soleSD.mesh.NO['N_SD'])
    for jj in range(len(subDomain.neighbourSD)):
        W[jj] = displacement_python.extract_field_on_nodes(subDomain.soleSD.mesh.NO['N{}'.format(jj)])
        T[jj] = problemOnSolePython(subDomain.sole[jj],W[jj],param)
    return W,T

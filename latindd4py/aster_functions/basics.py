#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 16:10:00 2017

@author: oumaziz
"""
import numpy as np
import scipy.sparse as sp
try:
    import code_aster
#    from code_aster.Commands import *
    from code_aster.Commands import AFFE_CHAR_MECA,AFFE_MODELE,ASSE_VECTEUR,CALC_CHAR_CINE,CALC_VECT_ELEM,CREA_CHAMP,CREA_RESU,MACR_ELEM_STAT,_F
except ImportError:
    print("asterxx is not sourced !\n If it happens during installation of the package do not care about this message")

from latindd4py.aster_functions.interface_field import pythonField,asterField

def assemblyField(subDomain,field,param):
    """
    Return a field assembled on the whole edge of interface of the subdomain
    Input:
        -subDomain: class of subdomain
        -field: dictionnary of the fields to assemble
        -param: class of parameters
    Output:
        -asterField of the assembled fields given in input
    """
    nodes_tot_list = []
    for jj in range(len(subDomain.neighbourSD)):
        nodes_tot_list += field[jj].nodes
    nodes_tot = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(nodes_tot_list))])))]
    prol_tot = pythonField(np.zeros(param.dim * len(nodes_tot)),nodes_tot)
    for jj in range(len(subDomain.neighbourSD)):
        prol_tot += prol_zero(field[jj],nodes_tot)

    return prol_tot



def assign_displacement_field_on_BC(field,mod,param):
    """
    Assign a displacement field on a boundary condition; Return a boundary
    condition of type MechanicalLoad.
    Input:
        -field: field to assign
        -mod: aster model
        -param: class of parameters
    Output:
        -aster boundary condition of type char_meca
    """
    ### Test if the field is an asterField class or not to convert it in
    ### pythonField class
    if isinstance(field,asterField):
        fieldPython = field.convert2python()
    else:
        fieldPython = field
    if param.dim == 2:
            ddlImpo = [_F(NOEUD=fieldPython.nodes[ii],DX=fieldPython.valField[2*ii],DY=fieldPython.valField[2*ii+1]) for ii in range(len(fieldPython.nodes))]
    elif param.dim == 3:
        ddlImpo = [_F(NOEUD=fieldPython.nodes[ii],DX=fieldPython.valField[3*ii],DY=fieldPython.valField[3*ii+1],DZ=fieldPython.valField[3*ii+2]) for ii in range(len(fieldPython.nodes))]
    CharMeca = AFFE_CHAR_MECA(MODELE = mod,DDL_IMPO = ddlImpo)
    return CharMeca

def assign_force_field(field,mod,param):
    """
    Assign a nodal force field on a boundary condition. Return a boundary
    condition of type MechanicalLoad
    Input:
        -field: field to assign
        -mod: aster model
        -param: class of parameters
    Output:
        -aster boundary condition of type char_meca
    """
    ### Test if the field is an asterField class or not to convert it in
    ### pythonField class
    if isinstance(field,asterField):
        fieldPython = field.convert2python()
    else:
        fieldPython = field
    if param.dim == 2:
            mecaImpo = [_F(NOEUD=fieldPython.nodes[ii],FX=fieldPython.valField[2*ii],FY=fieldPython.valField[2*ii+1]) for ii in range(len(fieldPython.nodes))]
    elif param.dim == 3:
        mecaImpo = [_F(NOEUD=fieldPython.nodes[ii],FX=fieldPython.valField[3*ii],FY=fieldPython.valField[3*ii+1],FZ=fieldPython.valField[3*ii+2]) for ii in range(len(fieldPython.nodes))]
    CharMeca = AFFE_CHAR_MECA(MODELE = mod,FORCE_NODALE = mecaImpo)
    return CharMeca

def assign_model(gma,meshAster,param):
    """
    Return an aster model instance
    Input:
            -gma: group of elements which constitutes the base of the model
            -meshAster: mesh aster
            -param: class of parameters
    Output:
        -aster model
    """
    ### En attendant la nouvelle syntaxe
    model = AFFE_MODELE(MAILLAGE=meshAster,
                        AFFE=_F(GROUP_MA=gma,
                                PHENOMENE=param.phenomenon,
                                MODELISATION=param.modelisation,
                                ),
                        DISTRIBUTION=_F(METHODE='CENTRALISE'),
                        )
    return model

def assign_material(gma,meshAster,mat):
    """
    Return an aster material instance
    Input:
            -gma: group of elements which constitutes the base of the model
            -meshAster: mesh aster
            -mat: material define with define_material
    Output:
        - aster material assigned on a mesh
    """
    affectMat = code_aster.MaterialOnMesh(meshAster)
    if isinstance(gma,list):
        for GMA in gma:
            affectMat.addMaterialOnGroupOfElements(mat,[GMA])
    else:
        affectMat.addMaterialOnGroupOfElements(mat,[gma])
    affectMat.buildWithoutInputVariables()

    return affectMat

def block_gno(mod,gno_dirichlet,dim,multiplier=False):
    """
    Return a boundary condition instance
    Input:
        -mod: aster model
        -gno_dirichlet: name of the group of nodes on which the condition is
        applied
        -dim: dimension of the problem (default 3 dimensions)
    Output:
        -aster boundary conditions of type char_cine
    """

    dofs = [code_aster.PhysicalQuantityComponent.Dx,code_aster.PhysicalQuantityComponent.Dy,code_aster.PhysicalQuantityComponent.Dz]
    if multiplier:
        imposed = code_aster.DisplacementDouble()
        for ddl in range(dim):
            imposed.setValue(dofs[ddl], 0.0 )
        imposedDof = code_aster.ImposedDisplacementDouble(mod)
        for group in gno_dirichlet:
            imposedDof.setValue( imposed, group )
        imposedDof.build()
    else:
        imposedDof = code_aster.KinematicsMechanicalLoad()
        imposedDof.setModel(mod)
        if isinstance(gno_dirichlet, list):
            for group in gno_dirichlet:
                for ddl in range(dim):
                    imposedDof.addImposedMechanicalDOFOnNodes(dofs[ddl],0.0,group)
        else:
            for ddl in range(dim):
                imposedDof.addImposedMechanicalDOFOnNodes(dofs[ddl],0.0,gno_dirichlet)
        imposedDof.build()

    return imposedDof

#==============================================================================
# Build a results from a displacement field
#==============================================================================
def build_result_from_displ(field,model,mat,param):
    """
    Built a result aster concept from a displacement field.
    Input:
        -field: displacement field
        -model: aster model
        -mat: aster material assign on the mesh
        -param: class of parameters
    Output:
        -aster result
    """
    if isinstance(field,pythonField):
        fieldAster = field.convert2aster(param)
    else:
        fieldAster = asterField(field)
    resu = CREA_RESU(OPERATION = 'AFFE',
                     TYPE_RESU = 'EVOL_ELAS',
                     NOM_CHAM = 'DEPL',
                     AFFE = _F(CHAM_GD = fieldAster.field,
                               MODELE = model,
                               CHAM_MATER = mat,
                               INST = 0.
                               )
                    )
    return resu

def compute_dirichlet_right_hand_side(nume_ddl,dirichlet_conditions):
    """
    Compute the right hand slide due to the dirichlet conditions
    Input:
        -nume_ddl: numerotation of the mesh
        -dirichlet_conditions: list of the dirichlet conditions
    Output:
        -aster boundary conditon of type char_cine
    """
    if len(dirichlet_conditions) > 1:
        char_cine = [CALC_CHAR_CINE(NUME_DDL = nume_ddl,CHAR_CINE = dirichlet_conditions[ii]) for ii in range(len(dirichlet_conditions)-1)]
    else:
        char_cine = CALC_CHAR_CINE(NUME_DDL = nume_ddl,
                                   CHAR_CINE = dirichlet_conditions)
    return char_cine

def computeDirichlet(model,param):
    """
    Compute the aster concepts for the Dirichlet conditions and create the
    python
    RHS for the linear stage.
    Input:
        -model: class model
        -param: class of parameters
    """
    dirichlet = dict()
    for tt in range(param.numberTimeStep):
        dirichlet_conditions = list()
        ### Add tht block conditions from the edges of soles
        dirichlet_conditions.append(model.boundary_conditions['BlocS'])
        ### Add imposed Dirichlet boundary conditions
        if model.boundary_conditions['imposed_dirichlet']:
            dirichlet_conditions.append(model.boundary_conditions['imposed_dirichlet'][tt])
        dirichlet_rhs = CALC_CHAR_CINE(NUME_DDL = model.nume_ddl,
                                       CHAR_CINE = dirichlet_conditions)
        dirichlet[tt] = dirichlet_rhs.EXTR_COMP().valeurs
    return dirichlet

def computeNeumann(model,mesh,neighbourNeum,param):
    """
    Compute the aster concepts for the Neumann conditions and create the python
    RHS for the linear stage.
    Input:
        -model: class model
        -mesh: class mesh
        -neighbourNeum: list of the Neumann conditions applied on the subdomain
        -param: class of parameters
    """
    neumann = dict()
    for tt in range(param.numberTimeStep):
        neumann_conditions = list()
        neumann[tt] = np.zeros(param.dim*len(mesh.NO['N_SD_tot']))
        for jj,neumBC in enumerate(neighbourNeum):
            if param.boundaryNeumann[neumBC]['type'] == 'FORCE_CONTOUR':
                imposedBC = code_aster.ForceDouble()
                imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fx,
                                   param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FX'])
                imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fy,
                                   param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FY'])
                boundary_condition = code_aster.LineicForceDouble(model.MODELE)
                boundary_condition.setValue(imposedBC,mesh.GMA['FdM{}'.format(jj)])
                boundary_condition.build()
            elif param.boundaryNeumann[neumBC]['type'] == 'PRES_REP':
                imposedBC = code_aster.PressureDouble()
                imposedBC.setValue(code_aster.PhysicalQuantityComponent.Pres,
                                   param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['PRES'])

                boundary_condition = code_aster.DistributedPressureDouble(model.MODELE)
                boundary_condition.setValue(imposedBC,mesh.GMA['FdM{}'.format(jj)])
                boundary_condition.build()
            elif param.boundaryNeumann[neumBC]['type'] == 'FORCE_FACE':
                imposedBC = code_aster.ForceDouble()
                imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fx,
                                   param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FX'])
                imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fy,
                                   param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FY'])
                imposedBC.setValue(code_aster.PhysicalQuantityComponent.Fz,
                                   param.boundaryNeumann[neumBC]['evol'][tt]*param.boundaryNeumann[neumBC]['FZ'])
                boundary_condition = code_aster.ForceOnFaceDouble(model.MODELE)
                boundary_condition.setValue(imposedBC,mesh.GMA['FdM{}'.format(jj)])
                boundary_condition.build()
            elif param.boundaryNeumann[neumBC]['type'] == 'PESANTEUR':
                boundary_condition = AFFE_CHAR_MECA(MODELE = model.MODELE,
                                                    PESANTEUR = _F(GROUP_MA = mesh.GMA['S'],
                                                                   GRAVITE = param.boundaryNeumann[neumBC]['GRAVITE'],
                                                                   DIRECTION = (param.boundaryNeumann[neumBC]['DX'],
                                                                                param.boundaryNeumann[neumBC]['DY'],
                                                                                param.boundaryNeumann[neumBC]['DZ']),
                                                                   )
                                                )
            neumann_conditions.append(boundary_condition)
            VEF = CALC_VECT_ELEM(OPTION ='CHAR_MECA',
                             CHARGE = neumann_conditions,
                             CHAM_MATER = model.assigned_material,
                             )
            Fd_asse = ASSE_VECTEUR(VECT_ELEM = VEF,
                                  NUME_DDL = model.nume_ddl,
                                  )
            F_asse = CREA_CHAMP(OPERATION = 'ASSE',
                                TYPE_CHAM = 'NOEU_DEPL_R',
                                MODELE = model.MODELE,
                                NUME_DDL = model.nume_ddl,
                                PROL_ZERO = 'OUI',
                                ASSE = _F(TOUT='OUI', CHAM_GD = Fd_asse, CUMUL='OUI'),
                                )
            neumann[tt] += F_asse.EXTR_COMP().valeurs
    return neumann

def compute_neumann_right_hand_side(nume_ddl,assigned_material,neumann_conditions):
    """
    Compute the right hand slide due to the dirichlet conditions
    Input:
        -nume_ddl: numerotation of the mesh
        -assigned_material: aster material assigned on mesh
        -dirichlet_conditions: list of the dirichlet conditions
    Output:
        -aster vector
    """
    VEF = CALC_VECT_ELEM(OPTION = 'CHAR_MECA',
                         CHARGE = neumann_conditions,
                         CHAM_MATER = assigned_material
                         )

    char_meca = ASSE_VECTEUR(VECT_ELEM = VEF,
                             NUME_DDL = nume_ddl
                             )
    return char_meca

def compute_matele(mod,assigned_material,dirichlet_conditions = None):
    """
    Return an elementary matrix at aster format
    Input:
        -mod: aster model
        -assigned_material: aster material assigned to a mesh
        -dirichlet_conditions: list of dirichlet conditions type char_meca
    Output:
        aster elementary stiffness
    """
    study = code_aster.StudyDescription(mod,assigned_material)
    if dirichlet_conditions:
        for dirichlet in dirichlet_conditions:
            study.addMechanicalLoad(dirichlet)
    dProblem = code_aster.DiscreteProblem(study)
    matele = dProblem.computeMechanicalStiffnessMatrix()
    return matele

def compute_normal_gma(mod,gma):
    """
    Compute the normal on a edge group of element and return numpy array
    consisted in values of normal for each element of the edge
    Input:
        -mod: aster model of the structure
        -gma: group of elements where the normal is computed
    Output:
        -numpy array
    """
    normal_aster = CREA_CHAMP(OPERATION = 'NORMALE',
                              TYPE_CHAM = 'NOEU_GEOM_R',
                              MODELE = mod,
                              GROUP_MA = gma,
                              )
    normal_python = normal_aster.EXTR_COMP().valeurs
    return normal_python

def compute_nume_ddl(matele):
    """
    Return the numerotation of the degrees of freedom of a stiffness matrix
    Input:
        -matele: elementary stiffness matrix
    Output:
        -aster nume_ddl
    """
    numeDDL = code_aster.DOFNumbering()
    numeDDL.setElementaryMatrix(matele)
    numeDDL.computeNumbering()
    return numeDDL

def compute_stiffness(matele,nume_ddl,char_cine = None):
    """
    Return an assembled stiffness matrix
    Input:
        -matele: elementary stiffness matrix
        -nume_ddl: numerotation of degrees of freedom
        -char_cine: boundary conditions taken into account without multiplier
    Output:
        -aster stiffness
    """
    stiffness = code_aster.AssemblyMatrixDisplacementDouble()
    stiffness.appendElementaryMatrix(matele)
    if char_cine:
        for char in char_cine:
            stiffness.addKinematicsLoad(char)
    stiffness.setDOFNumbering(nume_ddl)
    stiffness.build()
    return stiffness

def create_resu(field,model,mat,char_cine=None):
    """
    Create an aster concept of results from an aster field obtained by a solve.
    Input:
        -field: aster displacement field
        -model: aster model
        -mat: aster assigned material
        -char_cine: dirichlet boundary conditions
    Output:
        -aster result
    """
    if char_cine:
        resu = CREA_RESU(OPERATION = 'AFFE',
                         TYPE_RESU = 'EVOL_ELAS',
                         NOM_CHAM = 'DEPL',
                         EXCIT = _F(CHARGE = char_cine),
                         AFFE = _F(CHAM_GD = field,
                                   MODELE = model,
                                   CHAM_MATER = mat,
                                   INST = 0.,)
                         )
    else:
        resu = CREA_RESU(OPERATION = 'AFFE',
                     TYPE_RESU = 'EVOL_ELAS',
                     NOM_CHAM = 'DEPL',
                     AFFE = _F(CHAM_GD = field,
                               MODELE = model,
                               CHAM_MATER = mat,
                               INST = 0.,)
                    )

    return resu

def define_material(young,poisson):
    """
    Return an aster material
    Input:
        -young: Young modulus
        -poisson: Poisson ration
    Output:
        -aster material
    """
    mater = code_aster.ElasMaterialBehaviour()
    mater.setDoubleValue("E",young)
    mater.setDoubleValue("Nu",poisson)
    mater.setDoubleValue("Rho",1.)

    mat = code_aster.Material()
    mat.addMaterialBehaviour(mater)
    mat.build()

    return mat

def extract_stiffness_on_nodes(stiffness,nodesTot,neighbourDiri,groupNO,param,block=True):
    """
    Extract the part of the matrix corresponding to Dirichlet dofs before
    elimination.
    Input:
        -stiffness: stiffness with sparse format
        -nodesTot: list of nodes of the structure
        -neighbourDiri: list of the boundary Dirichlet conditions associated 
        to the subdomain
        -groupNO: dictionnary of the node groups
        -param: class of parameters
        -block: to block edge of soles or not
    Output:
        sparse matrix to apply to Dirichlet conditions in the right hand-side
    """
    dim = stiffness.shape[0] // len(nodesTot)
    stiffness_extr = sp.lil_matrix((stiffness.shape[0],stiffness.shape[0]))

    pos_in_valField = []                                                       # list of the degree of freedom to keep
    ### Extract the position of the dofs of the edges of soles
    if block:
        dict_nodes = {node:pos for pos,node in enumerate(nodesTot)}
        pos_in_nodes = [dict_nodes[node] for node in groupNO['BN']]
        pos_edgesSoles_in_valField = [dim * ii+jj for ii in pos_in_nodes for jj in range(dim)]
        pos_in_valField += pos_edgesSoles_in_valField
    ### Extract the position of the dofs of the potential Dirichlet boundary conditions
    for jj,diri in enumerate(neighbourDiri):
        listDof = list(param.boundaryDirichlet[diri].keys())
        listDof.remove('evol')
        posOfDof = [param.dofs['python'].index(dof) for dof in listDof]
        pos_in_nodes = [dict_nodes[node] for node in groupNO['Wd{}'.format(jj)]]
        pos_diri_in_valField = [dim * ii+jj for ii in pos_in_nodes for jj in posOfDof]
        pos_in_valField += pos_diri_in_valField
    
    if pos_in_valField:
        pos_in_valField = sorted(pos_in_valField)
        stiff_inter = stiffness[np.ix_(list(range(dim*len(nodesTot))),pos_in_valField)]
        stiff_inter[np.ix_(pos_in_valField,list(range(len(pos_in_valField))))] = 0.*sp.eye(len(pos_in_valField),len(pos_in_valField))
        stiffness_extr[np.ix_(list(range(dim*len(nodesTot))),pos_in_valField)] = stiff_inter
        return stiffness_extr.tocsr()
    else:
        return None


def factorize(matr_asse,solver):
    """
    Factorize the assembled matrix
    Input:
        -matr_asse: assembled matrix
        -solver: aster concept of solver
    """
    solver.matrixFactorization(matr_asse)

def get_aster_mesh(meshPath):
    """
    Return the mesh aster
    Input:
        -meshPath: path to find the mesh file in MED format
    Output:
        -aster mesh
    """
    mesh = code_aster.Mesh()
    mesh.readMedFile(meshPath)
    return mesh

def get_nodes_from_gno(gno,mesh,dim):
    """
    Return the list of nodes which compose the group of nodes gno
    Input:
        -gno: name of the group of nodes
        -mesh: aster mesh
        -dim: dimension of the problem
    Output:
        -python list of string
    """
    ### Solution temporaire en attendant mieux de asterxx
    comp = ('DX','DY','DZ')
    val = (0.,0.,0.)
    champNO = CREA_CHAMP(OPERATION = "AFFE",
                         TYPE_CHAM = 'NOEU_DEPL_R',
                         MAILLAGE = mesh,
                         AFFE = _F(GROUP_NO = gno,
                                   NOM_CMP = comp[:dim],
                                   VALE = val[:dim],
                                   )
                         )
    no = ['N{}'.format(ii) for ii in sorted(list(set(list(champNO.EXTR_COMP(topo = 1).noeud))))]
    return no

def get_nodes_from_gma(gma,mesh,dim):
    """
    Return the list of nodes which compose the group of nodes gno
    Input:
        -gno: name of the group of nodes
        -mesh: aster mesh
        -dim: dimension of the problem
    Output:
        -python list of string
    """
    ### Solution temporaire en attendant mieux de asterxx
    comp = ('DX','DY','DZ')
    val = (0.,0.,0.)
    champNO = CREA_CHAMP(OPERATION = "AFFE",
                         TYPE_CHAM = 'NOEU_DEPL_R',
                         MAILLAGE = mesh,
                         AFFE = _F(GROUP_MA = gma,
                                   NOM_CMP = comp[:dim],
                                   VALE = val[:dim],
                                   )
                         )
    no = ['N{}'.format(ii) for ii in sorted(list(set(list(champNO.EXTR_COMP(topo = 1).noeud))))]
    return no

def imposed_dof_gno(mod,list_gno,valueBC,multiplier=False):
    """
    Return a kinematics boundary condition instance
    Input:
        -mod: aster model of the structure
        -list_gno: list of the node groups of the boundary conditions
        -valueBC: value and component of the boundary conditions
    Output:
        aster boundary condition of type char_cine
    """
    dof_connection = {
                'DX' : code_aster.PhysicalQuantityComponent.Dx,
                'DY' : code_aster.PhysicalQuantityComponent.Dy,
                'DZ' : code_aster.PhysicalQuantityComponent.Dz,
                }
    if not multiplier:
        imposedDof = code_aster.KinematicsMechanicalLoad()
        imposedDof.setModel(mod)
        for ii,gno in enumerate(list_gno):
            for dof in list(valueBC[ii].keys()):
                imposedDof.addImposedMechanicalDOFOnNodes(dof_connection[dof],valueBC[ii][dof],gno)
        imposedDof.build()

    else:
        imposedDof = code_aster.ImposedDisplacementDouble(mod)
        for ii,gno in enumerate(list_gno):
            imposedDofii = code_aster.DisplacementDouble()
            for dof in list(valueBC[ii].keys()):
                imposedDofii.setValue(dof_connection[dof],valueBC[ii][dof])
            imposedDof.setValue(imposedDofii,gno)
        imposedDof.build()

    return imposedDof

def mergeArrayOnNodes(listArray,listNodes):
    """
    Sew the numpy array following the list of nodes
    Input:
        -listArray: list of the numpy array
        -listNodes: list of the list of nodes associated to the arrays
    Output:
        Give a numpy array
    """
    dim = listArray[0].shape[0] // len(listNodes[0])
    list_nodes_tot = list(set(sum([nodes for nodes in listNodes],[])))
    nodes = ['N'+nume for nume in map(str,sorted(map(int,[elem.lstrip('N') for elem in list(set(list_nodes_tot))])))]
    newArray = np.zeros((dim*len(nodes),dim*len(nodes)))
    dict_nodes = {node:pos for pos,node in enumerate(nodes)}
    for jj,listNodesOfArray in enumerate(listNodes):
        pos_in_nodes = [dict_nodes[node] for node in listNodesOfArray]
        pos_in_array = [dim * ii + kk for ii in pos_in_nodes for kk in range(dim)]
        newArray[np.ix_(pos_in_array,pos_in_array)] += listArray[jj]

    return newArray

def prol_zero(field,nodes_glob):
    """
    Create a new field extended to zeros from a inital field
    Input:
        -field: pythonField to extend
        -nodes: list of nodes of the final pythonField
    Output:
        -pythonField instance
    """
    dim = field.valField.shape[0] // len(field.nodes)
    prol_field = np.zeros(dim*len(nodes_glob))
    dict_nodes = {node:pos for pos,node in enumerate(nodes_glob)}
    pos_in_nodes = [dict_nodes[node] for node in field.nodes]
    prol_field[[dim * ii + kk for ii in pos_in_nodes for kk in range(dim)]] = field.valField
    return pythonField(prol_field,nodes_glob)

def schurComplement(K,boundary,listNodesGlobal,allSubMatrix=False):
    """
    Compute the Schur complement
    Input:
        -K: the operator to condensed in scipy sparse
        -boundary: group of nodes where to condensed the operator
        -listNodesGlobal: list of all the nodes of the operator
        -param: class of parameters
    """
    dim = K.shape[0] // len(listNodesGlobal)
    dict_nodes = {node:pos for pos,node in enumerate(listNodesGlobal)}
    pos_in_nodes = [dict_nodes[node] for node in boundary]
    pos_in_valField = [dim * ii+jj for ii in pos_in_nodes for jj in range(dim)]
    bb = pos_in_valField
    ii = list(set(range(K.shape[0]))-set(pos_in_valField))
    Kbb = K[np.ix_(bb,bb)]
    Kbi = K[np.ix_(bb,ii)]
    Kib = K[np.ix_(ii,bb)]
    Kii = K[np.ix_(ii,ii)]
    luKii = sp.linalg.splu(Kii)
    schur = Kbb.toarray() - Kbi.dot(luKii.solve(Kib.toarray()))
    subMatrix = {'Kbb':Kbb,'Kbi':Kbi,'Kii':Kii,'Kib':Kib,'luKii':luKii}
    if allSubMatrix:
        return schur,subMatrix
    else:
        return schur

def schurComplementAster(mod,mat,dirichletDual,boundary,param):
    """
    Compute the Schur complement
    Input:
        -mod: aster model assigned to the structure
        -mat: aster material assigned to the structure
        -dirichletDuel: list of Dirichlet boundary conditions on intern nodes
        -boundary: group of nodes where to condensed the stiffness
        -param: class of parameters
    """
    schur = MACR_ELEM_STAT(DEFINITION=_F(MODELE = mod,
                                         CHAM_MATER = mat,
                                         CHAR_MACR_ELEM = dirichletDual,
                                         ),
                           EXTERIEUR=_F(GROUP_NO = boundary),
                           RIGI_MECA=_F(),
                           )
    return schur

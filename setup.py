#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 11:29:15 2017

@author: oumaziz
"""

from setuptools import setup, find_packages
import latindd4py

setup(name='latindd4py',
      version=latindd4py.__version__,
      description='Latin domain decomposition for python',
      url='',
      author='Paul Oumaziz',
      author_email='paul.oumaziz@gmail.com',
      license='MIT',
      packages=find_packages(),
      long_description=open('README.rst').read(),
      install_requires=['mpi4py',
                        'shelve2',
                        'numpy',
                        'scipy',
                        'lxml',
                        'logging'],
#      scripts=['bin/launch_latin.py']
      )
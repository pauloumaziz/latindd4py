latindd4py
----------

Must install asterxx and salome-meca before launching computations

Dependencies python:
    -shelve (for persisent python objects)
    -mpi4py (for MPI)
    -numpy
    -scipy
    -tqdm   (for progress bar)

Two modules are stored:
    -latindd4py: package to compute via a domain decomposition a mechanical pro-
                blem with asterXX. It requires a specific data input made with 
                the other package salomedd4aster.
    -salomedd4aster : salome tools to to prepare meshes and obtain information 
                      about connectivities between subdomains and interfaces. It 
                      builds directories of inputs which can be used with the 
                      package latindd4py

Thus 2 steps are necessary to run a simulation: the preparation and the computation.

1 ) Preparation
---------------
    *) Write an xml file for the data input. It contains the information of the 
    parameters of the method and characteristics of the structure. Examples are
    in the directory latindd4py/latindd4py/tests/Data/
    The structure of the xml file must respect the schema of 
    latindd4py/latindd4py/prepare/parameters.xsd
    *) For the Latin ddm:
            -Build the mesh with the MED format. The subdomains are identified as 
            groups of elements named "S$(number_of_the_subdomain)". Boundary 
            conditions are also identified as groups of elements. For Dirichlet 
            conditions, the groups are named "Wd$(number_of_the_condition)$, the 
            Neumann conditions are namde "Fd$(number_of_the_condition)$.
            Ex : mesh with 3 subdomains and 2 Dirichlet and 1 Neumann, the groups
                stored in the mesh are : 'S0','S1','S2','Wd0','Wd1','Fd0'
            -Launch the preparation of the mesh with salome:
                
mpirun -n X $(path_to_salome)/salome shell -- python salomedd4aster/bin/subStructuringBySalome.py -data=path_to_the_data_file -subdomains={all or list_of_the_subdomains_to_prepare}
            
            the subdomain argument is optional. It permits in case of failure of 
            some mpi process to launch again the process and prepare only the 
            subdomains which were concerned by the failure.
            The script create a directory with the following architecture:
                nameOfStudy/
                ----Prepare/
                --------MED/
                ------------S$(number_of_subdomains).med                        # MED file for each subdomain
                --------Parameters/
                ------------Parameters_$(number_of_subdomains)/
                ----------------parameters_$(number_of_subdomains).{bak,dat,dir}# Persistent python objects containing information about a subDomain

      For the Schwarz ddm:
            -From a monolitic mesh with only groups related to boundary conditions,
            make the substructuring with overlap with the script develop by EDF "myMEDSplitter.py".
            To launch it, it requires a Salome-dev install and to launch it on the host where it is installed:
                
/utmp/chinon/oumaziz/salome-dev/appli_V8_DEV/salome shell -- salomedd4aster/bin/myMEDSplitter.py --fichier=med_file --maillage=name_mesh_in_meshfile --partitions=number_of_subdomains

            It build a mesh file for each subdomain named "$(name_of_initial_mesh_file)_$(number_of_subdomain).med"
            
            -Make the directory Prepare/MED to stored the previous mesh files and rename them as "prepare_$(number_of_subdomain).med"
            Then launch the preparation script such as for the Latin ddm

mpirun -n X $(path_to_salome)/salome shell -- python salomedd4aster/bin/subStructuringBySalome.py -data=path_to_the_data_file -subdomains={all or list_of_the_subdomains_to_prepare}

2 ) Computation
---------------

If the directory of preparation is correctly specified in the xml data file, it 
is easy to launch the computation with:
    
mpirun -n X latindd4py/bin/launch_job.bash --data=path_to_the_data_file --aster=path_to_asterxx_installation_directory --sources=path_to_latindd4py

It produces a directory of results with the following architecture:
nameOfStudy/
----nameOfStudy_$(number_of_subdomains)/
--------LOG.log																														 # Log file
--------latin_error.txt                                                    # Text file of the error indicator
--------profile.txt                                                        # File of profile
--------MED/                                                               # Directory of MED results files
------------iteration$(number_of_iterations)_$(number_of_timestep)t.med    # Result by iteration and by timestep

3 ) Post-treatment
------------------

Once all the results are gathered in the same directory, it is possible to 
post-treat the results automatically with salome. The script postTreatment.py
makes a group data set for all subdomains by iteration and by timestep and 
export it in vtm format.

It creates in the directory nameOfStudy/ a new directory Iterations
nameOfStudy/
----Iterations/
--------Iterations_$(timestep)/
------------Iteration_$(timestep)t_$(iteration)/
----------------Iterations_$(timestep)t_$(iteration)_$(subdomain)_0.vtu
------------Iterations_$(timestep)t_$(iteration).vtm

Then collections are made to postreat easily in paraview:
    - the first one permits to visualize the evolution of the timesteps for
    a given iteration. It creates new directories with the following 
    architecture:
    
    nameOfStudy/
    ----EvolTimeStep/
    --------Iterations_$(iteration)/
    ------------Iterations_$(timestep).vtm
    
    
    - the second one permits to visualize the evolution of the iterations
    for a given timestep. It creates new directories with the following 
    architecture:
    
    nameOfStudy/
    ----Iterations/
    --------EvolTimeStep_$(timestep)/
    ------------Iterations_$(iteration).vtm
    
Then simply open one of these two previous collection to see the evolution 
in paraview.


In case of any problem do not hesitate to contact oumaziz@lmt.ens-cachan.fr


